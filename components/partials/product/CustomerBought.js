import React, { PureComponent } from "react";
import Product from "../../../components/elements/products/Product";
import Slider from "react-slick";
import NextArrow from "../../../components/elements/carousel/NextArrow";
import PrevArrow from "../../../components/elements/carousel/PrevArrow";
import { BaseURL } from "../../../public/static/data/baseURL.json";
class CustomerBoughtdata extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      productss: [],
    };
  }

  componentDidMount() {
    const { productId } = this.props;
    const req_data = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        product_id: { productId }.productId,
      }),
    };
    fetch(`${BaseURL}ecomapi/similar_products`, req_data)
      // .then((res) => res.json())
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else if (res.status === 401) {
          console.log("SOMETHING WENT WRONG")
          // this.setState({ requestFailed: true })
        }
      })
      .then((result) => {
        this.setState({
          isLoaded: true,
          productss: result.data,
        });
      })
      .catch((error) =>
        this.setState({
          isLoaded: true,
          error: error,
        })
      );
  }

  render() {
    const { productss } = this.state;

    const carouselSetting = {
      dots: false,
      infinite: false,
      speed: 500,
      slidesToShow: 7,
      slidesToScroll: 3,
      nextArrow: <NextArrow />,
      prevArrow: <PrevArrow />,
      responsive: [
        {
          breakpoint: 1366,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 3,
            infinite: false,
            dots: false,
          },
        },
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 4,
            infinite: false,
            dots: true,
            arrows: false,
          },
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 2,
            dots: true,
            arrows: false,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
            dots: true,
            arrows: false,
          },
        },
      ],
    };
    return (
      <div className="ps-product-list ps-garden-kitchen">
        <div className="ps-container">
          <div className="ps-section__header">
            <h3>Customers who bought this item also bought</h3>
            <ul className="ps-section__links"></ul>
          </div>
          <div className="ps-section__content">
            <Slider {...carouselSetting} className="ps-carousel outside">
              {productss.map((product) => (
                <Product product={product} key={product.id} />
              ))}
            </Slider>
          </div>
        </div>
      </div>
    );
  }
}

export default CustomerBoughtdata;
