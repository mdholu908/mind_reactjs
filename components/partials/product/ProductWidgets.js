import React, { PureComponent } from "react";
import Link from "next/link";
import { BaseURL } from "../../../public/static/data/baseURL.json";
class ProductWidgets extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: [],
      adsitems: [],
    };
  }

  render() {
    const { items } = this.state;
    const { adsitems } = this.state;

    return (
      <section>
        <aside className="widget widget_product widget_features">
          <p>
            <i className="icon-cart-add"></i>Buy Now Pay Later For All Orders
          </p>
          <p>
            <i className="icon-percent"></i>No Interest or Processing Fees
          </p>
          <p>
            <i className="icon-calculator2"></i> Easy EMI with Early Payment
            Options
          </p>
          <p>
            <i className="icon-cash-dollar"></i>No Down Payment or Upfront Fees
          </p>
        </aside>

        <aside className="widget widget_ads">
          <Link href="/">
            <a>
              <img src="/static/img/ads/product_ads.png" alt="Creditkart" />
            </a>
          </Link>
        </aside>
      </section>
    );
  }
}

export default ProductWidgets;
