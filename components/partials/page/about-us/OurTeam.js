import React from "react";

const OurTeam = () => (
  <div className="ps-our-team">
    <div className="container">
      <iframe src='https://www.youtube.com/embed/18WLuhu796I'
        allow='autoplay; encrypted-media'
        title='video' height="500"
      />

     
      <div className="ps-section__content">
      </div>
    </div>
  </div>
);

export default OurTeam;
