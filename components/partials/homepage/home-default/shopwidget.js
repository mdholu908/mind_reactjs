import React, { Component } from "react";
import Router from "next/router";
import { connect } from "react-redux";
import {
  getProductsByPrice,
  getProductsByBrands,
} from "../../../../store/product/action";
import { Slider, Checkbox } from "antd";
import Link from "next/link";
import { BaseURL } from "../../../../public/static/data/baseURL.json";
class ShopWidget extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      priceMin: 0,
      priceMax: 2000,
      items: [],
    };
  }
   componentDidMount() {
    fetch(`${BaseURL}ecomapi/category_list`)
      .then((results) => {
        return results.json();
      })
      .then((data) => {
        this.setState({
          items: data.category,
        });
      })
      .catch((error) =>
        this.setState({
          isLoaded: true,
          error: error,
        })
      );
  }

  render() {
    /* You can get categories from your API using redux */
    const { items } = this.state;

    return (
      <div className="ps-layout__left">
        <aside className="widget widget_shop">
          <h4 className="widget-title">Categories</h4>
          <ul className="ps-list--categories">
            {items.map((category) => (
              <li key={category.name}>
                <Link href="/shop/[cat]" as={`/shop/${category.name}`}>
                  <a>
                  {category.name.split("_").join(" ")}
                  </a>
                </Link>
              </li>
            ))}
          </ul>
        </aside>
      </div>
    );
  }
}

export default ShopWidget;
