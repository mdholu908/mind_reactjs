import React, { PureComponent } from "react";
import Product from "../../../elements/products/Product";
import BreadCrumb from "../../../elements/BreadCrumb";
import Loader from "../../../elements/Loader";
import { BaseURL } from "../../../../public/static/data/baseURL.json";
import { Pagination } from "antd";
import fetch from "isomorphic-fetch";


class SpecificCategory extends PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			error: null,
			isLoaded: true,
			items: [],
			itemsall: [],
			currentPage: 1,
			totalPages: 1,
			newtotalPages: 1,
			isfetched: false,
		};
	}

	onPageChange = (page) => {


		let start = 4 * (page - 1);
		let end = 4 * page;
		this.setState(
			{
				currentPage: page,
			},
			() => {
				this.categorydata();
				window.scrollTo(0,0)
			}
		);

	
		
		
	};

	categorydata = () => {
		
		const { cat } = this.props;
		const req_data = {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				category: { cat }.cat,
				page_id: this.state.currentPage,
			}),
		};
		fetch(`${BaseURL}ecomapi/ProductsForCategoryPagination/`, req_data)
			// .then((res) => res.json())
			.then((res) => {
				if (res.ok) {
					return res.json();
				} else if (res.status === 401) {
					console.log("SOMETHING WENT WRONG")
					// this.setState({ requestFailed: true })
				}
			})
			.then((result) => {
				if (result.total_page_count) {
					let temp_total_page_count = result.total_page_count;
					if (temp_total_page_count == 0) {
						temp_total_page_count = 1;
					}

					temp_total_page_count = temp_total_page_count * 10;
					this.setState({
						// isLoaded: true,
						isfetched: true,
						totalPages: temp_total_page_count,
						currentPage: 1,
						items: result.data,
					});
				} else {
					this.setState({
						// isLoaded: true,
						isfetched: true,
						totalPages: result.total_page_count,
						currentPage: 1,
						items: result.data,
					});
				}
			});

			
	};
	render() {

		const { cat } = this.props;
		const { items } = this.state;

		let breadCrumb = [
			{
				text: "Home",
				url: "/",
			},

			{
				text: cat.split("!").join("/"),
			},
		];
		return (
			<div className="ps-layout--shop">
				{this.state.isLoaded ? (
					<div className="ps-layout">
						{this.state.isfetched !== true
							? this.categorydata()
							: null}
						<BreadCrumb breacrumb={breadCrumb} />
						<div className="ps-shopping">
							<div className="ps-shopping__content">
								<div className="ps-shopping-product">
									<div className="row">
										{items.map((item) => (
											<div
												className="col-xl-2 col-lg-4 col-md-4 col-sm-6 col-6 "
												key={item.id}
											>
												<Product
													product={item}
													key={item.id}
												/>
											</div>
										))}
									</div>
								</div>

								<Pagination
									defaultCurrent={1}
									total={this.state.totalPages}
									// total={50}
									onChange={this.onPageChange}
									hideOnSinglePage={true}
									simple={false}
									showQuickJumper={true}
								/>
							</div>
						</div>
					</div>
				) : (
						<Loader />
					)}
			</div>
		);
	}
}

export default SpecificCategory;
