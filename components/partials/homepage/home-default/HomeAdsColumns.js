import React, { PureComponent } from "react";
import Link from "next/link";
import { BaseURL } from "../../../../public/static/data/baseURL.json";
import "lazysizes";

class HomeAds extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: [],
    };
  }

  componentDidMount() {
    fetch(`${BaseURL}loan-app-data/HomepageAds`)
      // .then((res) => res.json())
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else if (res.status === 401) {
          console.log("SOMETHING WENT WRONG")
          // this.setState({ requestFailed: true })
        }
      })
      .then((result) => {
        this.setState({
          isLoaded: true,
          items: result.homeadscol,
        });
      })
      .catch((error) =>
        this.setState({
          isLoaded: true,
          error: error,
        })
      );
  }

  render() {
    const { items } = this.state;

    return (
      <div className="ps-home-ads">
        <div className="ps-container">
          <div className="row">
            {items.map((item, index) => (
              <div
                key={item.ads + index}
                className="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 "
              >
                <Link href="/#">
                  <a className="ps-collection">
                    <img
                      data-src={item.ads}
                      // width={200} height={100}
                      alt="Creditkart"
                      class="lazyload"
                    />
                  </a>
                </Link>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}

export default HomeAds;

// const HomeAds = () => (
//     <div className="ps-home-ads">
//         <div className="ps-container">
//             <div className="row">
//                 <div className="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 ">
//                     <Link href="/shop">
//                         <a className="ps-collection">
//                             <img
//                                 src="/static/img/mudrakwik.png"
//                                 alt="Creditkart"
//                             />
//                         </a>
//                     </Link>
//                 </div>
//                 <div className="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 ">
//                     <Link href="/shop">
//                         <a className="ps-collection">
//                             <img
//                                 src="/static/img/collection/home-1/2.jpg"
//                                 alt="Creditkart"
//                             />
//                         </a>
//                     </Link>
//                 </div>
//                 <div className="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 ">
//                     <Link href="/shop">
//                         <a className="ps-collection">
//                             <img
//                                 src="/static/img/collection/home-1/3.jpg"
//                                 alt="Creditkart"
//                             />
//                         </a>
//                     </Link>
//                 </div>
//             </div>
//         </div>
//     </div>
// );

// export default HomeAds;
