import React, { PureComponent } from "react";

import Slider from "react-slick";
import NextArrow from "../../../elements/carousel/NextArrow";
import PrevArrow from "../../../elements/carousel/PrevArrow";
import Link from "next/link";
import { BaseURL, LocalURL } from "../../../../public/static/data/baseURL.json";
import "lazysizes";

class HomeBanner extends PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			error: null,
			isLoaded: false,
			items: [],
			adsdata: [],
		};
	}

	 componentDidMount() {

		const req_data2 = {
			method: "GET",
			headers: {
				"Content-Type": "application/json",
			},

		};
		 fetch(`${BaseURL}ecomapi/offer_banner_api_web/`, req_data2)
			// .then((res) => res.json())
			.then((res) => {
				if (res.ok) {
				  return res.json();
				} else if (res.status === 401) {
				  console.log("SOMETHING WENT WRONG")
				  // this.setState({ requestFailed: true })
				}
			  })
			.then((result) => {
				console.log("api resp "+JSON.stringify(result));
				this.setState({
					isLoaded: true,
					items: result.data,
				});
			})
			.catch((error) =>
				this.setState({
					isLoaded: true,
					error: error,
				})
			);
	}

	render() {
		const { items } = this.state;
		const carouselSetting = {
			fade: true,
			dots: false,
			infinite: true,
			speed: 1000,
			autoplaySpeed: 3000,
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: true,

			nextArrow: <NextArrow />,
			prevArrow: <PrevArrow />,
		};
		return (
			<div className="ps-home-banner">
				<Slider {...carouselSetting} className="ps-carousel">

					{items.map((item) => (
						<div className="ps-banner">

							<img
								src={item.path}
								alt="Creditkart"
								class="lazyload"
							/>

						</div>
					))}
				</Slider>
			</div>
		);
	}
}

export default HomeBanner;
