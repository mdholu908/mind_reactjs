import React, { PureComponent } from "react";
import Link from "next/link";
import Router from "next/router";
import "lazysizes";
import { connect } from "react-redux";
import { login, saveUser } from "../../../../store/auth/action";
import { BaseURL } from "../../../../public/static/data/baseURL.json";
class HomeDefaultTopCategories extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: [],
    };
  }
  componentDidMount() {
    this.handleParseUrl();
  }
  handleParseUrl() {
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
      params = {},
      match;
    while ((match = regex.exec(window.location.href))) {
      params[match[1]] = match[2];
    }
    if (params.id) {
      this.validateToken(params.id);
    }
  }
  validateToken(token) {
    let Token = "Token" + " " + token;
    const requestOptions = {
      headers: { authorization: Token },
    };
    const url = "http://127.0.0.1:8000/ecomapi/validate_user/";
    const validate_token_url = `${BaseURL}ecomapi/validate_user/`;
    fetch(validate_token_url, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        let status_code = result.status;
        const { username } = result;
        const { is_profile_filled } = result;
        if (typeof status_code !== "undefined" || status_code != null) {
          if (status_code == "1" || status_code == 1) {
            this.props.dispatch(
              saveUser({
                username: username,
                token: token,
              })
            );
            this.props.dispatch(login());
            // user info
            if (is_profile_filled === true) {
              Router.push("/");
            } else {
              Router.push("/account/view-user-information");
            }
          } else {
            // alert(status_code + result.message);
          }
        } else if (result.detail) {
          // alert(result.detail)
          if (typeof result.detail !== "undefined" || result.detail != null) {
          }
        } else {
          // alert(status_code)
        }
      })
      .catch((error) =>
        this.setState({
          isLoaded: true,
          error: error,
        })
      );
  }
  render() {
    const Electronics = "Electronics";
    const Men = "Men";
    const Women = "Women";
    const Home = "Home";
    return (
      <div className="ps-container" style={{backgroundColor: "white"}}>
        <h3>Top Categories </h3>
        <div className="row">
          <div className="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6 ">
            <div className="ps-block--category">
              <Link href="/shop/[cat]" as={`/shop/${Women}`}>
                <img
                  data-src="/static/img/categories/womens.png"
                  alt="CreditKart"
                  class="lazyload"
                />
              </Link>
              <Link href="/shop/[cat]" as={`/shop/${Women}`}>
                <p>Women</p>
              </Link>
            </div>
          </div>
          <div className="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6 ">
            <div className="ps-block--category">
              <Link href="/shop/[cat]" as={`/shop/${Men}`}>
                <img
                  data-src="/static/img/categories/Men.png"
                  alt="CreditKart"
                  class="lazyload"
                />
              </Link>
              <Link href="/shop/[cat]" as={`/shop/${Men}`}>
                <p>Men</p>
              </Link>
            </div>
          </div>
          <div className="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6 ">
            <div className="ps-block--category">
              <Link href="/shop/[cat]" as={`/shop/${Electronics}`}>
                <img
                  data-src="/static/img/categories/electronic.png"
                  alt="CreditKart"
                  class="lazyload"
                />
              </Link>
              <Link href="/shop/[cat]" as={`/shop/${Electronics}`}>
                <p>Electronics</p>
              </Link>
            </div>
          </div>

          <div className="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6 ">
            <div className="ps-block--category">
              <Link href="/shop/[cat]" as={`/shop/${Home}`}>
                <img
                  data-src="/static/img/categories/home.png"
                  alt="CreditKart"
                  class="lazyload"
                />
              </Link>
              <Link href="/shop/[cat]" as={`/shop/${Home}`}>
                <p>Home</p>
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  // Redux Store --> Component
  return state.auth;
};
export default connect(mapStateToProps)(HomeDefaultTopCategories);
