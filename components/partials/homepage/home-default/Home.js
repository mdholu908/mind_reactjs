import React, { PureComponent } from "react";
import Link from "next/link";
import Slider from "react-slick";
import Product from "../../../elements/products/Product";
import NextArrow from "../../../elements/carousel/NextArrow";
import PrevArrow from "../../../elements/carousel/PrevArrow";
import Loader from "../../../elements/Loader";
import { BaseURL } from "../../../../public/static/data/baseURL.json";

class Home extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      productss: [],
      categoryname: [],
    };
  }

   componentDidMount() {
    const req_data2 = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        category_name: "Home",
      }),
    };
     fetch(`${BaseURL}ecomapi/dashboard_products_new_priority/`, req_data2)
      // .then((res) => res.json())
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else if (res.status === 401) {
          console.log("SOMETHING WENT WRONG")
          // this.setState({ requestFailed: true })
        }
      })
      .then((result) => {
        this.setState({
          isLoaded: true,
          productss: result.products.Home,
          categoryname: result.products.Home[0].categories[0].name,
        });
      })
      .catch((error) =>
        this.setState({
          isLoaded: true,
          error: error,
        })
      );
  }

  render() {
    const { productss } = this.state;
    const { categoryname } = this.state;
    const test1 = productss[0];

    // const a  = productss.home_1_electronic;

    const carouselSetting = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 5,
      slidesToScroll: 3,
      nextArrow: <NextArrow />,
      prevArrow: <PrevArrow />,
      responsive: [
        {
          breakpoint: 1366,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 3,
            infinite: true,
            dots: false,
          },
        },
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 3,
            infinite: true,
            dots: true,
            arrows: false,
          },
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 2,
            infinite: true,
            dots: true,
            arrows: false,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: true,
            dots: true,
            arrows: false,
          },
        },
      ],
    };
    return (
      <div className="ps-product-list ps-garden-kitchen">
        {this.state.isLoaded ? (
          <div className="ps-container">
            <div className="ps-section__header">
              <h3>Home</h3>
              <ul className="ps-section__links">
                <li>
                  <Link href="/shop/[cat]" as={`/shop/${categoryname}`}>
                    <a>View All</a>
                  </Link>
                </li>
              </ul>
            </div>
            <div className="ps-section__content__new">
              <Slider {...carouselSetting} className="ps-carousel outside">
                {productss.map((product) => (
                  <Product product={product} key={product.id} />
                ))}
              </Slider>
            </div>
          </div>
        ) : (
          <Loader />
        )}
      </div>
    );
  }
}
export default Home;
