// import React, { PureComponent } from "react";
// import Link from "next/link";
// import { BaseURL } from "../../../../public/static/data/baseURL.json";
// class HomeAds extends PureComponent {
//   constructor(props) {
//     super(props);
//     this.state = {
//       error: null,
//       isLoaded: false,
//       items: [],
//     };
//   }

//   componentDidMount() {
//     fetch(`${BaseURL}loan-app-data/HomepageAds`)
//       .then((res) => res.json())
//       .then((result) => {
//         this.setState({
//           isLoaded: true,
//           items: result.homeads,
//         });
//       })
//       .catch((error) =>
//         this.setState({
//           isLoaded: true,
//           error: error,
//         })
//       );
//   }

//   render() {
//     const { items } = this.state;

//     return (
//       <div className="ps-home-ads">
//         <div className="ps-container">
//           <div className="row">
//             {items.map((item, index) => (
//               <div
//                 key={item.ads + index}
//                 className="col-xl-4 col-lg-4 col-md-8 col-sm-12"
//               >
//                 <Link href="/#">
//                   <a className="ps-collection">
//                     <img
//                       src={item.ads}
//                       // width={200} height={100}
//                       alt="Creditkart"
//                     />
//                   </a>
//                 </Link>
//               </div>
//             ))}
//           </div>
//         </div>
//       </div>
//     );
//   }
// }

// export default HomeAds;
