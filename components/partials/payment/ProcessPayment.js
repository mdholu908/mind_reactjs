import React, { Component } from "react";
import Link from "next/link";
import Router from "next/router";
import { connect } from "react-redux";
import { BaseURL } from "../../../public/static/data/baseURL.json";

import { orderPlacedSuccessByNow } from "../../../store/cart/action";

class ProcessPayment extends Component {
	constructor(props) {
		super(props);
		this.state = {
			error: null,
			isLoaded: false,
			isSuccess: false,
			isfetched: false,
			status: "",
			data: {},
		};
	}
	componentDidMount() {
		this.handleParseUrl();
	}

	handleParseUrl() {
		const url = window.location;
		var regex = /[?&]([^=#]+)=([^&#]*)/g,
			params = {},
			match;
		if (url) {
			const full_url = window.location.href;

			while ((match = regex.exec(full_url))) {
				params[match[1]] = match[2];
			}
			if (params) {
				this.getPaymentStatus({ order_id: params.order_id });
			}
		}
	}

	getPaymentStatus(data) {
		// let Token = "Token" + " " + token;
		let Token = "Token" + " " + "5a18ec93d812124606dfee4b3dcdbdf50403261f";

		const param = { order_id: data.order_id };
		const requestOptions = {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				authorization: Token,
			},
			body: JSON.stringify(data),
		};

		const url = "http://127.0.0.1:8000/ecomapi/payment_status/";
		const getPaymentStatusUrl = `${BaseURL}ecomapi/razorpay_transaction_status/`;

		fetch(getPaymentStatusUrl, requestOptions)
			.then((response) => response.json())
			.then((result) => {
				let status_code = result.status_code;

				if (typeof status_code !== "undefined" || status_code != null) {
					if (status_code == "1" || status_code == 1) {
						this.setState({
							data: result,
							isfetched: true,
							status: result.status,
						});

						this.props.dispatch(orderPlacedSuccessByNow());

						if (result.status == "success") {
							this.setState({ isSuccess: true });
							// empty buynow
							this.props.cart.buynow.length = 0;
						} else {
							this.setState({ isSuccess: false });
							// empty buynow
							this.props.cart.buynow.length = 0;
						}
					}
				}
			})
			.catch((error) =>
				this.setState({
					isLoaded: true,
					error: error,
				})
			);
	}

	render() {
		const {
			status,
			transaction_id,
			status_code,
			paytm_order_id,
			ecom_order_id,
			message,
			date,
			amount,
		} = this.state.data;
		return (
			<div className="ps-container">
				<div>
				</div>
				<div className="row">

					<div className="col-sm-4"></div>
					<div className="col-sm-4">
					<h4>redirecting to homepage</h4>

						<div className="ps-form__content">

							<div className="ps-block--checkout-order">
								<div className="ps-block__content">
									<figure className="ps-block__items">
										<div className="row">
											{this.state.isSuccess ? (
												<div className="col-xl-10 col-lg-8 col-md-6 col-sm-6 col-6 ">
													<img
														src="/static/img/success.gif"
														alt="CreditKart"
													/>
												</div>
											) : (
													<div className="col-xl-12 col-lg-8 col-md-6 col-sm-6 col-6 ">
														<img
															src="/static/img/Paymentfailed.png"
															alt="CreditKart"
														/>
													</div>
												)}
										</div>
									</figure>

									<figure className="ps-block__items">
										<strong>Order id:</strong>
										<p>{paytm_order_id}</p>
									</figure>
									<figure className="ps-block__items">
										<strong>Date:</strong>
										<p>{date}</p>
									</figure>
									<figure className="ps-block__items">
										<strong>Amount:</strong>
										<p>{amount}</p>
									</figure>
									<div>
										<strong>Payment Status:</strong>
										<p>{status}</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="col-sm-4"></div>
				</div>

			</div>
		);
	}
}

//export default ProcessPayment;

const mapStateToProps = (state) => {
	// Redux Store --> Component
	return {
		auth: state.auth,
		cart: state.cart,
	};
};
export default connect(mapStateToProps)(ProcessPayment);
