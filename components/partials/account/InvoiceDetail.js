import React, { PureComponent } from "react";
import AccountMenuSidebar from "./modules/AccountMenuSidebar";
import Link from "next/link";
import ProductCart from "../../elements/products/ProductCart";
import { connect } from "react-redux";
import { BaseURL } from "../../../public/static/data/baseURL.json";
class InvoiceDetail extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isfetched: false,
      items: [],
      cancellable: false,
      shipping_address: "",
      credit: 0.0,
      shipping_charges: 0.0,
    };
  }

  handleGetInvoiceDetail(data) {
    const { record } = this.props;
    if (data !== null) {
      let Token = "Token" + " " + data.token;

      let req_param = { order_details_id: record };
      const requestOptions = {
        method: "POST",
        headers: { "Content-Type": "application/json", authorization: Token },
        body: JSON.stringify(req_param),
      };

      const url = "http://127.0.0.1:8000/ecomapi/invoice_details/";
      const invoice_details_url = `${BaseURL}ecomapi/invoice_details/`;

      fetch(invoice_details_url, requestOptions).then((result) => {
        result.json().then((resp) => {
          this.setState({
            isfetched: true,
            items: resp.data.data,
            shipping_address: resp.data.shipping_address,
            credit: resp.data.credit,
            shipping_charges: resp.data.shipping_charges,
          });
        });
      });
    } else {
    }
  }

  orderCancelApi(order_detail_id) {
    let req_param = {
      order_data: {
        orders: order_detail_id,
      },
    };
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(req_param),
    };
    const url = "http://127.0.0.1:8000/ecomapi/order-cancel";
    const order_cancel_url = `${BaseURL}ecomapi/invoice_details/`;

    fetch(order_cancel_url, requestOptions).then((result) => {
      result.json().then((resp) => {});
    });
  }

  handleOrderCancel = (e, product) => {
    e.preventDefault();
    this.orderCancelApi([product.id]);
  };

  handleAllCancelOrder = (e, orders) => {
    e.preventDefault();
    let orders_list = [];
    var lookup = {};
    for (var i = 0, len = orders.length; i < len; i++) {
      orders_list.push(orders[i].id);
    }
    this.orderCancelApi(orders_list);
  };

  myFunction() {
    let status = "placed";
    return status;
  }
  render() {
    const { record } = this.props;

    if (this.props.auth.auth_data != null) {
      const { token, username } = this.props.auth.auth_data;
    }

    const { items } = this.state;
    const accountLinks = [
      {
        text: "Account Information",
        url: "/account/user-information",
        icon: "icon-user",
      },
      {
        text: "Notifications",
        url: "/account/notifications",
        icon: "icon-alarm-ringing",
      },
      {
        text: "Invoices",
        url: "/account/invoices",
        icon: "icon-papers",
        active: true,
      },
      {
        text: "Address",
        url: "/account/addresses",
        icon: "icon-papers",
      },
    ];

    return (
      <section className="ps-my-account ps-page--account">
        {/* isfetched */}
        {this.state.isfetched === false
          ? [
              <div>
                {this.props.auth.auth_data !== null
                  ? this.handleGetInvoiceDetail({
                      token: this.props.auth.auth_data.token,
                    })
                  : null}
              </div>,
            ]
          : null}
        <div className="container">
          <div className="row">
            <div className="col-lg-4">
              <div className="ps-page__left">
                <AccountMenuSidebar data={accountLinks} />
              </div>
            </div>
            <div className="col-lg-8">
              <div className="ps-page__content">
                <div className="ps-section--account-setting">
                  <div className="ps-section__header">
                    <h3>
                      Invoice #{record} -<strong>Successful delivery</strong>
                    </h3>
                  </div>
                  <div className="ps-section__content">
                    <div className="row">
                      <div className="col-md-4 col-12">
                        <figure className="ps-block--invoice">
                          <figcaption>Address</figcaption>
                          <div className="ps-block__content">
                            {/*<strong>John Walker</strong>*/}
                            <p>{this.state.shipping_address}</p>
                            <p>Phone: {this.props.auth.auth_data.username}</p>
                          </div>
                        </figure>
                      </div>

                      <div className="col-md-4 col-12">
                        <figure className="ps-block--invoice">
                          <figcaption>Payment</figcaption>
                          <div className="ps-block__content">
                            <p>Payment Method: Visa</p>
                          </div>
                        </figure>
                      </div>
                      <div className="col-md-4 col-12">
                        <figure className="ps-block--invoice">
                          <figcaption>Shipping Fee</figcaption>
                          <div className="ps-block__content">
                            <p>Shipping Fee: {this.state.shipping_charges}</p>
                          </div>
                        </figure>
                      </div>
                      <div className="col-md-4 col-12">
                        <figure className="ps-block--invoice">
                          <figcaption>Credit</figcaption>
                          <div className="ps-block__content">
                            <p>Credit: {this.state.credit}</p>
                          </div>
                        </figure>
                      </div>
                    </div>
                    <div className="table-responsive">
                      <table className="table ps-table--shopping-cart">
                        <thead>
                          <tr>
                            <th>Product</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Amount</th>
                            <th>Status</th>
                          </tr>
                        </thead>
                        {items ? (
                          <tbody>
                            {items.map((product) => (
                              <tr key={product.id}>
                                <td>
                                  <ProductCart product={product} />
                                </td>
                                <td className="price">₹{product.price}</td>
                                <td>1</td>
                                <td className="price">₹{product.price}</td>

                                <td className="status">{product.status}</td>
                              </tr>
                            ))}
                          </tbody>
                        ) : (
                          <tbody></tbody>
                        )}
                      </table>
                    </div>
                    <div className="row">
                      <Link href="/account/invoices">
                        <a className="ps-btn ps-btn--sm ">Back to invoices</a>
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

// Map State To Props (Redux Store Passes State To Component)
const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};
export default connect(mapStateToProps)(InvoiceDetail);
