import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { notification } from "antd";
import { Form, Input, Radio, DatePicker } from "antd";
import Router from "next/router";
import { BaseURL } from "../../../public/static/data/baseURL.json"
class UserInformation extends PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			pancard: "",
			data: "",
			msg: null,
			name: null,
			gender: null,
			maritalstatus: null,
			date: null,
			mobile: null,
			personal: {},
			addressdata: {},
			isfetched: false,
		};
	}
	handleChange = (param, event) => {
		this.setState({ [param]: event.target.value.toUpperCase() });
	};
	handleemailChange = (param, event) => {
		this.setState({ [param]: event.target.value });
	};

	onValueChange(event) {
		this.setState({
			gender: event.target.value,
		});
	}

	MaritalstatusChange(event) {
		this.setState({
			maritalstatus: event.target.value,
		});
	}
	onChange = (date) => {
		const valueOfInput = date.format();
		this.setState({ date: valueOfInput });
	};
	panCarddata() {
		const requestOptions = {
			method: "POST",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify({
				pan_card: this.state.pancard,
			}),
		};
		// try {
		fetch(`${BaseURL}api/pancard-validate/`, requestOptions)
			// let result = await response.json();
			// this.setState({
			//             msg:result.message,
			//             name:result.name,
			//             mobile:this.props.auth.auth_data.username,
			//         });
			//     }catch (error) {
			//       }

			// .then((res) => res.json())
			.then((res) => {
				if (res.ok) {
					return res.json();
				} else if (res.status === 401) {
					console.log("SOMETHING WENT WRONG")
					// this.setState({ requestFailed: true })
				}
			})
			.then((result) => {
				this.setState({
					msg: result.message,
					name: result.name,
					mobile: this.props.auth.auth_data.username,
					isfetched: true,
				});
			});
	}

	handlenumberChange = (e) => {
		const re = /^[0-9\b]+$/;
		if (e.target.value === "" || re.test(e.target.value)) {
			this.setState({ salary: e.target.value });
		} else {
			this.setState({ salary: 0 });
		}
	};

	handleSubmit = (e) => {
		e.preventDefault();
		if (
			this.state.name !== null &&
			this.state.salary !== 0 &&
			this.state.firstName !== null &&
			this.state.lastName !== null &&
			this.state.gender !== null &&
			this.state.maritalstatus !== null &&
			this.state.address !== null &&
			this.state.apartment !== null &&
			this.state.city !== null &&
			this.state.state_1 !== null &&
			this.state.postalcode &&
			this.state.date !== null
		) {
			this.state.personal = {
				pancard: this.state.pancard,
				name: this.state.name,
				mobile: this.state.mobile,
				firstName: this.state.firstName,
				lastName: this.state.lastName,
				gender: this.state.gender,
				marital_status: this.state.maritalstatus,
				email: this.state.email,
				income: this.state.salary,
				birth_date: this.state.date,
			};
			this.state.addressdata = {
				address_1: this.state.address,
				address_2: this.state.apartment,
				city: this.state.city,
				state: this.state.state_1,
				postalcode: this.state.postalcode,
			};

			let Token = "Token" + " " + this.props.auth.auth_data.token;

			const requestOptions = {
				method: "POST",
				headers: { "Content-Type": "application/json", authorization: Token },
				body: JSON.stringify({
					personal: this.state.personal,
					addr: this.state.addressdata,
				}),
			};
			fetch(
				`${BaseURL}ecomapi/user_personal_details_add/`,
				requestOptions
			);

			notification["success"]({
				message: "success",
				description: "Your Details has been added Succcessfully !",
				duration: 4,
			});
			Router.push(`/`);
		} else {
			notification["warning"]({
				message: "warning",
				description:
					"Please make sure that all the fields are filled in/ marked correctly.!",
				duration: 3,
			});
		}
	};

	render() {
		const { getFieldDecorator } = this.props.form;
		return (
			<div>
				{this.props.auth.auth_data !== null ? (
					<div>
						<Form
							className="ps-form--checkout"
							onSubmit={this.handleSubmit.bind(this)}
						>
							<div className="ps-form__content">
								<div className="row">
									<div className="col-xl-8 col-lg-8 col-md-12 col-sm-12">
										<div className="ps-form__billing-info">
											<h3 className="ps-form__heading">User Information</h3>
											<div className="row">
												<div className="col-sm-6">
													<div className="form-group">
														<Form.Item>
															{getFieldDecorator("pancard", {
																rules: [
																	{
																		required: true,
																		message: "Enter PANCARD number",
																	},
																],
															})(
																<Input
																	className="form-control"
																	type="text"
																	placeholder="Enter PANCARD"
																	maxLength="10"
																	onChange={this.handleChange.bind(
																		this,
																		"pancard"
																	)}
																/>
															)}
														</Form.Item>
													</div>
													{this.state.pancard.length === 10 ? (
														<div>
															{this.state.isfetched !== true ? (
																<div>{this.panCarddata()}</div>
															) : null}
															{this.state.name !== null
																? this.state.name
																: null}
															<br />
															{this.state.msg !== null ? this.state.msg : null}
														</div>
													) : null}
												</div>
												<div className="col-sm-6">
													<div className="form-group">
														<Form.Item>
															{getFieldDecorator("mobile", {
																rules: [
																	{
																		required: false,
																		message: "Enter Mobile Number",
																	},
																],
															})(
																<Input
																	className="form-control"
																	disabled
																	type="text"
																	placeholder={
																		this.props.auth.auth_data.username
																	}
																/>
															)}
														</Form.Item>
													</div>
												</div>
											</div>
											<h3 className="ps-form__heading">Address</h3>

											<div className="form-group">
												<Form.Item>
													{getFieldDecorator("address", {
														rules: [
															{
																required: true,
																message: "Enter an address!",
															},
														],
													})(
														<Input
															className="form-control"
															type="text"
															placeholder="Address"
															onChange={this.handleChange.bind(this, "address")}
														/>
													)}
												</Form.Item>
											</div>
											<div className="form-group">
												<Form.Item>
													{getFieldDecorator("apartment", {
														rules: [
															{
																required: true,
																message: "Enter an Apartment!",
															},
														],
													})(
														<Input
															className="form-control"
															type="text"
															placeholder="Apartment, suite, etc."
															onChange={this.handleChange.bind(
																this,
																"apartment"
															)}
														/>
													)}
												</Form.Item>
											</div>
											<div className="row">
												<div className="col-sm-6">
													<div className="form-group">
														<Form.Item>
															{getFieldDecorator("city", {
																rules: [
																	{
																		required: true,
																		message: "Enter a city!",
																	},
																],
															})(
																<Input
																	className="form-control"
																	type="city"
																	placeholder="City"
																	onChange={this.handleChange.bind(
																		this,
																		"city"
																	)}
																/>
															)}
														</Form.Item>
													</div>
												</div>
												<div className="col-sm-6">
													<div className="form-group">
														<Form.Item>
															{getFieldDecorator("state_1", {
																rules: [
																	{
																		required: true,
																		message: "Enter a State!",
																	},
																],
															})(
																<Input
																	className="form-control"
																	type="city"
																	placeholder="State"
																	onChange={this.handleChange.bind(
																		this,
																		"state_1"
																	)}
																/>
															)}
														</Form.Item>
													</div>
												</div>
												<div className="col-sm-6">
													<div className="form-group">
														<Form.Item>
															{getFieldDecorator("postalCode", {
																rules: [
																	{
																		required: true,
																		message: "Enter a postal code!",
																	},
																],
															})(
																<Input
																	className="form-control"
																	type="postalCode"
																	placeholder="Postal Code"
																	onChange={this.handleChange.bind(
																		this,
																		"postalcode"
																	)}
																/>
															)}
														</Form.Item>
													</div>
												</div>
											</div>
											<h3 className="ps-form__heading">Personal Information</h3>

											<div className="row">
												<div className="col-sm-6">
													<div className="form-group">
														<Form.Item>
															{getFieldDecorator("firstName", {
																rules: [
																	{
																		required: true,
																		message: "Enter your first name!",
																	},
																],
															})(
																<Input
																	className="form-control"
																	type="text"
																	placeholder="First Name"
																	onChange={this.handleChange.bind(
																		this,
																		"firstName"
																	)}
																/>
															)}
														</Form.Item>
													</div>
												</div>
												<div className="col-sm-6">
													<div className="form-group">
														<Form.Item>
															{getFieldDecorator("lastName", {
																rules: [
																	{
																		required: true,
																		message: "Enter your last name!",
																	},
																],
															})(
																<Input
																	className="form-control"
																	type="text"
																	placeholder="Last Name"
																	onChange={this.handleChange.bind(
																		this,
																		"lastName"
																	)}
																/>
															)}
														</Form.Item>
													</div>
												</div>
											</div>
											<div className="row">
												<div className="col-sm-6">
													<div className="form-group">
														<Form.Item>
															{getFieldDecorator("email", {
																rules: [
																	{
																		required: true,
																		message: "Enter your email!",
																	},
																],
															})(
																<Input
																	className="form-control"
																	type="text"
																	placeholder="example@gmail.com"
																	onChange={this.handleemailChange.bind(
																		this,
																		"email"
																	)}
																/>
															)}
														</Form.Item>
													</div>
												</div>
												<div className="col-sm-6">
													<div className="form-group">
														<Form.Item>
															{getFieldDecorator("salary", {
																rules: [
																	{
																		required: true,
																		message: "Enter your Montly Salary!",
																	},
																],
															})(
																<Input
																	className="form-control"
																	type="number"
																	placeholder="Monthly Salary"
																	onChange={this.handlenumberChange.bind(this)}
																/>
															)}
														</Form.Item>
													</div>
												</div>
											</div>
											<div className="col-sm-6">
												<div className="form-group">
													<Form.Item label="Gender">
														{getFieldDecorator("gender", {
															rules: [
																{
																	required: true,
																	message: "Please Select Gender!",
																},
															],
														})(
															<Radio.Group>
																<Radio
																	value="Male"
																	checked={this.state.selectedOption === "Male"}
																	onChange={this.onValueChange.bind(this)}
																>
																	Male
																</Radio>

																<Radio
																	value="Female"
																	checked={
																		this.state.selectedOption === "Female"
																	}
																	onChange={this.onValueChange.bind(this)}
																>
																	Female
																</Radio>
															</Radio.Group>
														)}
													</Form.Item>
												</div>
											</div>
											<div className="col-sm-6">
												<div className="form-group">
													<Form.Item label="Marital status">
														{getFieldDecorator("maritalstatus", {
															rules: [
																{
																	required: true,
																	message: "Please Select status!",
																},
															],
														})(
															<Radio.Group>
																<Radio
																	value="Married"
																	checked={
																		this.state.selectedOption === "Married"
																	}
																	onChange={this.MaritalstatusChange.bind(this)}
																>
																	Married
																</Radio>

																<Radio
																	value="Unmarried"
																	checked={
																		this.state.selectedOption === "Unmarried"
																	}
																	onChange={this.MaritalstatusChange.bind(this)}
																>
																	Unmarried
																</Radio>

																<Radio
																	value="Divorced"
																	checked={
																		this.state.selectedOption === "Divorced"
																	}
																	onChange={this.MaritalstatusChange.bind(this)}
																>
																	Divorced
																</Radio>

																<Radio
																	value="Widow"
																	checked={
																		this.state.selectedOption === "Widow"
																	}
																	onChange={this.MaritalstatusChange.bind(this)}
																>
																	Widow
																</Radio>
															</Radio.Group>
														)}
													</Form.Item>
												</div>
											</div>

											<div className="col-sm-6">
												<div className="form-group">
													<label>Date of Birth</label>

													<DatePicker
														onChange={this.onChange}
													// value={this.state.date}
													// format={'YYYY-MMMM-DD'}
													/>
												</div>
											</div>
											<div className="ps-form__submit">
												<div className="ps-block__footer">
													<button className="ps-btn" type="submit">
														Submit
													</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</Form>
					</div>
				) : (
						<div className="ps-page--404">
							<div className="container">
								<div className="ps-section__content">
									<figure>
										<img src="/static/img/404.jpg" alt="CreditKart Error" />
									</figure>
								</div>
							</div>
						</div>
					)}
			</div>
		);
	}
}
const WrapFormUserInformation = Form.create()(UserInformation);
const mapStateToProps = (state) => {
	return {
		auth: state.auth,
	};
};
export default connect(mapStateToProps)(WrapFormUserInformation);
