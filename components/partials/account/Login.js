/* Component Name: Login.js
  This page gets called when user registers on thecreditkart.com.
  it also redirects the user from InformationDefault.js page 
  when user clicks on Add to cart/Buy Now Button and when user is not logged in.
 */

import React, { PureComponent } from "react";
import Link from "next/link";
import Router from "next/router";
import { login, saveUser } from "../../../store/auth/action";
import { Form, Input, notification } from "antd";
import { connect } from "react-redux";
import { BaseURL } from "../../../public/static/data/baseURL.json";
class Login extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      is_profile_filled: false,
      data: [],
      colour: "Colour",
      size: "Size",
      productdata: [],
    };
  }

  //------------------------------------------------------------

  // this method gets called after render method gets called.
  // in this method handleParseUrl methosd is called

  componentDidMount() {
    this.handleParseUrl();
  }

  //------------------------------------------------------------

  handleParseUrl() {
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
      params = {},
      match;
    while ((match = regex.exec(window.location.href))) {
      params[match[1]] = match[2];
    }
    if (params.id) {
      this.validateToken(params.id);
    }
  }

  //------------------------------------------------------------

  // validateToken method gets called from handleParseUrl method when params id is present.
  // in this method '/validate_user/' api is called

  validateToken(token) {
    let Token = "Token" + " " + token;

    const requestOptions = {
      headers: { authorization: Token },
    };

    // this is get api 
    const validate_token_url = `${BaseURL}ecomapi/validate_user/`;

    fetch(validate_token_url, requestOptions)
      // .then((response) => response.json())
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else if (res.status === 401) {
          console.log("SOMETHING WENT WRONG")
          // this.setState({ requestFailed: true })
        }
      })
      .then((result) => {
        let status_code = result.status;
        const { username } = result;
        const { is_profile_filled } = result;

        if (typeof status_code !== "undefined" || status_code != null) {
          if (status_code == "1" || status_code == 1) {
            this.props.saveUser({
              username: username,
              token: token,
            });
            this.props.login();
            // user info
            if (is_profile_filled === true) {
              Router.push("/account/demopagefortest");
            } else {
              Router.push("/account/user-information");
            }
          } else {
          }
        } else if (result.detail) {

          if (typeof result.detail !== "undefined" || result.detail != null) {
          }
        } else {
        }
      })
      .catch((error) =>
        this.setState({
          isLoaded: true,
          error: error,
        })
      );
  }

  //---------------------------------------------------

  handleFeatureWillUpdate(e) {
    e.preventDefault();
    notification.open({
      message: "Opp! Something went wrong.",
      description: "This feature has been updated later!",
      duration: 500,
    });
  }

  //-----------------------------------------------------

  // this method gets called from handleLoginSubmit() method

  //-----------------------------------------------------


  validateUser(data) {

    // POST API call
    const login_url = `${BaseURL}ecomapi/user_login`;


    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    };

    fetch(login_url, requestOptions)
      // .then((response) => response.json())
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else if (res.status === 401) {
          console.log("SOMETHING WENT WRONG")
          // this.setState({ requestFailed: true })
        }
      })
      .then((result) => {
        this.props.form.validateFields((err, values) => {
          if (!err) {
            let status_code = result.status;

            if (status_code == "5" || status_code == 5) {
              this.props.login();

              this.props.saveUser({
                username: data.username,
                token: result.token,
              });




              // if user is not loggedin and user selects some product and click on buynow he/she redirects 
              // to the login page after login user redirects to the checkout.js page


              if (this.props.cart.buynow.length >= 1) {
                this.checkBuynow();
              }

              // if user is not loggedin and user selects some product and click on Add To Cart he/she redirects 
              // to the login page after login user redirects to the checkout.js page

              else if (this.props.cart.cartItems.length >= 1) {
                this.checkCartItems();
              }

              // after login user redirects to the home page

              else if (this.props.cart.buynow.length === 0 || this.props.cart.cartItems.length === 0) {
                Router.push("/account/demopagefortest");
              }

            }
          }
        });
        this.setState({
          data: result,
          is_profile_filled: result.is_profile_filled,
        });
      })
      .catch((error) =>
        this.setState({
          isLoaded: true,
          error: error,
        })
      );


  }

  //------------------------------------------------------

  checkBuynow() {

    const req_data1 = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        product_id: this.props.cart.buynow[0].id,
        colour: this.props.cart.buynow[0].size,
        size: this.props.cart.buynow[0].color,
      }),
    };

    fetch(
      `${BaseURL}ecomapi/product_details_info`,
      req_data1
    )
      // .then((res) => res.json())
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else if (res.status === 401) {
          console.log("SOMETHING WENT WRONG")
          // this.setState({ requestFailed: true })
        }
      })
      .then((result) => {


        this.setState(
          {
            productdata: result.output,
          },
          () => {
            let tempProduct = this.state
              .productdata;

          }
        );

        Router.push(`/account/checkout`);
      });


  }

  //----------------------------------------------------

  checkCartItems() {

    const req_data1 = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        product_id: this.props.cart.cartItems[0].id,
        colour: this.props.cart.cartItems[0].size,
        size: this.props.cart.cartItems[0].color,
      }),
    };

    fetch(
      `${BaseURL}ecomapi/product_details_info`,
      req_data1
    )
      // .then((res) => res.json())
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else if (res.status === 401) {
          console.log("SOMETHING WENT WRONG")
          // this.setState({ requestFailed: true })
        }
      })
      .then((result) => {

        this.setState(
          {
            productdata: result.output,
          },
          () => {
            let tempProduct = this.state
              .productdata;

          }
        );

        Router.push(`/account/checkout`);
      });


  }

  //-----------------------------------------------------

  // this method gets called when user clicks on Log In Button

  handleLoginSubmit = () => {
    let fData = this.props.form.getFieldsValue();
    this.validateUser(fData);
  };

  //--------------------------------------------------------

  render() {

    const { getFieldDecorator } = this.props.form;

    return (
      <div className="ps-my-account">
        <div className="container">
          {
            <Form className="ps-form--account">
              <ul className="ps-tab-list">
                <li className="active">
                  <Link href="/account/login">
                    <a>Login</a>
                  </Link>
                </li>
                <li>
                  <Link href="/account/register">
                    <a>Register</a>
                  </Link>
                </li>
              </ul>
              <div className="ps-tab active" id="sign-in">
                <div className="ps-form__content">
                  <h5>Log In Your Account</h5>
                  <div className="form-group">
                    <Form.Item>
                      {getFieldDecorator("username", {
                        rules: [
                          {
                            required: true,
                            message:
                              "Please input your registered mobile number!",
                          },
                        ],
                      })(
                        <Input
                          className="form-control"
                          type="text"
                          placeholder="Mobile Number"
                        />
                      )}
                    </Form.Item>
                  </div>
                  <div className="form-group form-forgot">
                    <Form.Item>
                      {getFieldDecorator("pin", {
                        rules: [
                          {
                            required: true,
                            message: "Please input your 4 Digit Pin!",
                          },
                        ],
                      })(
                        <Input
                          className="form-control"
                          type="password"
                          placeholder="4 Digit Pin..."
                        />
                      )}
                    </Form.Item>
                  </div>
                  <div className="form-group">
                    <div className="ps-checkbox">
                      <input
                        className="form-control"
                        type="checkbox"
                        id="remember-me"
                        name="remember-me"
                      />
                      <label htmlFor="remember-me">Rememeber me</label>
                    </div>
                  </div>
                  <div className="form-group submit">
                    <button
                      type="submit"
                      onClick={this.handleLoginSubmit}
                      className="ps-btn ps-btn--fullwidth"
                    >
                      Login
                    </button>
                  </div>
                </div>
                <br></br>

              </div>
            </Form>
          }
        </div>
      </div>
    );
  }
}

const WrapFormLogin = Form.create()(Login);

// connects Store to the Component

const mapStateToProps = (state) => {
  return {
    cart: state.cart,
    auth: state.auth,
    product: state.product,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    login: () => dispatch(login()),
    saveUser: (saveData) => dispatch(saveUser(saveData)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(WrapFormLogin);
