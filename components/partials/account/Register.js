/*component Name: Register.js
  this component gets called when user Clicks on Register Button from home page
  After Registeration User Redirects to the OTP Page(Otp.js)
*/

import React, { PureComponent } from "react";
import Link from "next/link";
import Router from "next/router";
import { saveMobNo } from "../../../store/auth/action";
import { BaseURL } from "../../../public/static/data/baseURL.json";
import { Form, Input } from "antd";
import { connect } from "react-redux";

class Register extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      is_profile_filled: false,
      data: [],
      entered_mob_no: "",
      new_result: ""
    };
  }

  //--------------------------------------------------------------

  //this method gets called after page is rendered.

  componentDidMount() {
    this.handleParseUrl();
  }

  //---------------------------------------------------------------

  // this method gets called from ComponentDidMount method.

  handleParseUrl() {
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
      params = {},
      match;
    while ((match = regex.exec(window.location.href))) {
      params[match[1]] = match[2];
    }
    if (params.id) {
      this.validateToken(params.id);
    }
  }

  //-------------------------------------------------------------

  validateUser(data) {

    //POST API call
    const login_url = `${BaseURL}api/login_web/`;

    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    };


    fetch(login_url, requestOptions)
      // .then((response) => response.json())
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else if (res.status === 401) {
          console.log("SOMETHING WENT WRONG")
          // this.setState({ requestFailed: true })
        }
      })
      .then((result) => {
        this.setState({ new_result: result }, () => {
          this.props.dispatch(saveMobNo(result));
        })
        this.props.form.validateFields((err, values) => {
          if (!err) {
            let status_code = result.status;
            if (status_code == "1" || status_code == 1) {
              Router.push("/account/otp");
            }
          }
        });
      })
      .catch((error) =>
        this.setState({
          isLoaded: true,
          error: error,
        })
      );
  }

  //---------------------------------------------------------------

  changeValueHandler(e) {
    let tstate = this.state
    tstate.entered_mob_no = e.target.value;
    this.setState({ tstate });

  }

  //---------------------------------------------------------------

  //this method gets called when user clicks on Register Button

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        let fData = this.props.form.getFieldsValue();
        this.validateUser(fData);
      } else {

      }
    });
  };

  //-------------------------------------------------------------------


  render() {

    const { getFieldDecorator } = this.props.form;
    return (
      <div className="ps-my-account">
        <div className="container">
          <Form className="ps-form--account" onSubmit={this.handleSubmit}>
            <ul className="ps-tab-list">
              <li>
                <Link href="/account/login">
                  <a>Login</a>
                </Link>
              </li>
              <li className="active">
                <Link href="/account/register">
                  <a>Register</a>
                </Link>
              </li>
            </ul>
            <div className="ps-tab active" id="register">
              <div className="ps-form__content">
                <h5>Register An Account</h5>
                <div className="form-group">
                  <Form.Item>
                    {/* {getFieldDecorator("mobile_number", {
                      rules: [
                        {
                          required: true,
                          message: "Please input your Mobile Number!",
                        },
                        {
                          required: true,
                          message: "PSSSSSSS",
                        },
                      ],
                    })( */}
                      <Input
                        className="form-control"
                        type="text"
                        placeholder="Mobile No."
                        onChange={this.changeValueHandler.bind(this)}
                      /><br/><br/>
                      <Input
                        className="form-control"
                        type="text"
                        placeholder="Enter Pin"
                        onChange={this.changeValueHandler.bind(this)}
                      />
                    {/* )} */}
                  </Form.Item>
                </div>
                <div className="form-group submit">
                  <button type="submit" className="ps-btn ps-btn--fullwidth">
                    Register
                  </button>
                </div>
              </div>
              <div className="ps-form__footer">
                <p>Connect with:</p>
                <ul className="ps-list--social">
                  <li>
                    <a className="facebook"
                      href="https://www.facebook.com/Creditkart-101820038218707/"
                    >
                      <i className="fa fa-facebook"></i>
                    </a>
                  </li>
                  <li>
                    <a className="linkedin"
                      href="https://www.linkedin.com/company/67294237/"
                    >
                      <i className="fa fa-linkedin"></i>
                    </a>
                  </li>
                  <li>
                    <a className="instagram"
                      href="https://www.instagram.com/creditkartfincom/"
                    >
                      <i className="fa fa-instagram"></i>
                    </a>
                  </li>
                  <li>
                    <a className="twitter"
                      href="https://twitter.com/CreditKartFincm"
                    >
                      <i className="fa fa-twitter"></i>
                    </a>
                  </li>

                </ul>
              </div>
            </div>
          </Form>
        </div>
      </div>
    );
  }
}
const WrapFormRegister = Form.create()(Register);

//connects Store the Component.
const mapStateToProps = (state) => {
  return state.auth;
};
export default connect(mapStateToProps)(WrapFormRegister);