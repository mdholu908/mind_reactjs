import React, { PureComponent } from "react";
import Link from "next/link";
import { connect } from "react-redux";
import { BaseURL } from "../../../public/static/data/baseURL.json";
class Addresses extends PureComponent {
  constructor(props) {
    super(props);
    let test = props.auth.auth_data.token;

    this.state = { token: props.auth.auth_data.token };
    if (this.state.token !== null) {
      this.handleGetAddress({ token: this.state.token });
    }

    this.state = {
      error: null,
      isLoaded: false,
      billing_address: [],
      shipping_address: [],
    };
  }
  handleGetAddress(data) {
    const url = `${BaseURL}ecomapi/user-ecom-address`;
    const address_url = `${BaseURL}ecomapi/user-ecom-address`;
    let Token = "Token" + " " + data.token;

    const requestOptions = {
      headers: { "Content-Type": "application/json", authorization: Token },
    };

    fetch(address_url, requestOptions)
      // .then((response) => response.json())
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else if (res.status === 401) {
          console.log("SOMETHING WENT WRONG")
          // this.setState({ requestFailed: true })
        }
      })
      .then((result) => {
        this.setState({
          isLoaded: true,
          billing_address: result.data.billing_addr,
          shipping_address: result.data.shipping_addr,
        });
      })
      .catch((error) =>
        this.setState({
          isLoaded: true,
          error: error,
        })
      );
  }

  render() {
    if (this.props.auth.auth_data != null) {
      const { token, username } = this.props.auth.auth_data;
    }

    const shipp_addr = this.state.shipping_address;
    const billing_addr = this.state.billing_address;

    const ba = billing_addr.map((addr) => (
      <div key={addr.id}>
        <h3>{addr.address}</h3>
        <p>{addr.city}</p>
        <p>{addr.state}</p>
        <p>{addr.pin_code}</p>
      </div>
    ));

    const sa = shipp_addr.map((addr) => (
      <div key={addr.id}>
        <h3>{addr.address}</h3>
        <p>{addr.city}</p>
        <p>{addr.state}</p>
        <p>{addr.pin_code}</p>
      </div>
    ));

    const accountLinks = [
      {
        text: "Account Information",
        url: "/account/user-information",
        icon: "icon-user",
      },
      {
        text: "Notifications",
        url: "/account/notifications",
        icon: "icon-alarm-ringing",
      },
      {
        text: "Invoices",
        url: "/account/invoices",
        icon: "icon-papers",
      },
      {
        text: "Address",
        url: "/account/addresses",
        icon: "icon-map-marker",
        active: true,
      },
    ];
    return (
      <section className="ps-my-account ps-page--account">
        <div className="container">
          <div className="row">
            <div className="col-lg-8">
              <div className="ps-section--account-setting">
                <div className="ps-section__content">
                  <div className="row">
                    {/*
                                        <div className="col-md-6 col-12">
                                            <figure className="ps-block--address">
                                                <figcaption>
                                                    Billing address
                                                </figcaption>
                                                <div className="ps-block__content">
                                                    {ba}
                                                    <Link href="/account/edit-address">
                                                        <a>Edit</a>
                                                    </Link>
                                                </div>
                                            </figure>
                                        </div>
                                        */}
                    <div className="col-md-6 col-12">
                      <figure className="ps-block--address">
                        <figcaption>Shipping address</figcaption>
                        <div className="ps-block__content">
                          {sa}
                          {/*
                                                    <Link href="/account/edit-address">
                                                        <a>Edit</a>
                                                    </Link>
                                                    */}
                        </div>
                      </figure>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = (state) => {
  // Redux Store --> Component
  return {
    auth: state.auth,
  };
};
export default connect(mapStateToProps)(Addresses);
