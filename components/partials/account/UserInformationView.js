import React, { PureComponent } from "react";
import Link from "next/link";
import { connect } from "react-redux";
import { Form, Input, Divider } from "antd";
import { BaseURL } from "../../../public/static/data/baseURL.json";
class UserInformationView extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: "",
      personal: {
        pancard: "",
        name: "",
        mobile_no: "",
        gender: "",
        marital_status: "",
        email_id: "",
        monthly_income: "",
        birth_date: "",
      },
      address: {
        address: "",
        city: "",
        state: "",
        pin_code: "",
      },
      isfetched: false,
    };
  }
  handleGetAccountInfo(data) {
    const url = "http://127.0.0.1:8000/ecomapi/user-ecom-address";
    const address_url = `${BaseURL}ecomapi/user_personal_address_info/`;
    let Token = "Token" + " " + data.token;

    const requestOptions = {
      headers: {
        "Content-Type": "application/json",
        authorization: Token,
      },
    };

    fetch(address_url, requestOptions)
      // .then((response) => response.json())
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else if (res.status === 401) {
          console.log("SOMETHING WENT WRONG")
          // this.setState({ requestFailed: true })
        }
      })
      .then((result) => {
        this.setState({
          isLoaded: true,
          isfetched: true,
          personal: result.data.personal,
          address: result.data.address,
        });
      })
      .catch((error) =>
        this.setState({
          isLoaded: true,
          error: error,
        })
      );
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const { address, city, state, pin_code } = this.state.address;
    const {
      pancard,
      name,
      mobile_no,
      gender,
      marital_status,
      email_id,
      monthly_income,
      birth_date,
    } = this.state.personal;
    return (
      <section className="ps-my-account ps-page--account">
        <div className="container">
          {this.props.auth.auth_data.token !== null ? (
            <div className="row">
              {this.state.isfetched === false
                ? [
                  <div>
                    {this.props.auth.auth_data.token !== null
                      ? this.handleGetAccountInfo({
                        token: this.props.auth.auth_data.token,
                      })
                      : null}
                  </div>,
                ]
                : null}
              <div className="col-lg-8" style={{ marginBottom: "1%" }}>
                <div className="ps-page__content">
                  <Form className="ps-form--account-setting">
                    <div className="ps-form__header">
                      <h2>Account Information</h2>
                    </div>
                    <div className="ps-form__content">
                      <h3 className="ps-form__heading">Personal Information</h3>
                      <Divider />
                      <div className="row">
                        <div className="col-sm-12">
                          <div className="form-group">
                            <Form.Item label="Name">
                              <Input
                                className="form-control"
                                disabled
                                type="text"
                                value={name}
                              />
                            </Form.Item>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-sm-6">
                          <div className="form-group">
                            <Form.Item label="Pancard">
                              <Input
                                className="form-control"
                                disabled
                                type="text"
                                maxLength="10"
                                value={pancard}
                              />
                            </Form.Item>
                          </div>
                        </div>
                        <div className="col-sm-6">
                          <div className="form-group">
                            <Form.Item label="Mobile Number">
                              <Input
                                className="form-control"
                                disabled
                                type="text"
                                value={mobile_no}
                              />
                            </Form.Item>
                          </div>
                        </div>
                      </div>

                      <div className="row">
                        <div className="col-sm-6">
                          <div className="form-group">
                            <Form.Item label="Email">
                              <Input
                                className="form-control"
                                disabled
                                type="text"
                                value={email_id}
                              />
                            </Form.Item>
                          </div>
                        </div>
                        <div className="col-sm-6">
                          <div className="form-group">
                            <Form.Item label="Monthly Salary">
                              <Input
                                className="form-control"
                                disabled
                                type="text"
                                placeholder="Monthly Salary"
                                value={monthly_income}
                              />
                            </Form.Item>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-sm-6">
                          <div className="form-group">
                            <Form.Item label="Gender">
                              <Input
                                className="form-control"
                                disabled
                                type="text"
                                value={gender}
                              />
                            </Form.Item>
                          </div>
                        </div>
                        <div className="col-sm-6">
                          <div className="form-group">
                            <Form.Item label="Marital status">
                              <Input
                                className="form-control"
                                disabled
                                type="text"
                                value={marital_status}
                              />
                            </Form.Item>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-sm-6">
                          <div className="form-group">
                            <label>Date of Birth</label>

                            <Input
                              className="form-control"
                              disabled
                              type="text"
                              value={birth_date}
                            />
                          </div>
                        </div>
                      </div>
                      <h3 className="ps-form__heading">Address Information</h3>
                      <Divider />

                      <div className="form-group">
                        <Form.Item label="Address">
                          <Input
                            className="form-control"
                            disabled
                            type="text"
                            placeholder="Address"
                            value={address}
                          />
                        </Form.Item>
                      </div>

                      <div className="row">
                        <div className="col-sm-6">
                          <div className="form-group">
                            <Form.Item label="State">
                              <Input
                                className="form-control"
                                disabled
                                type="city"
                                placeholder="State"
                                value={state}
                              />
                            </Form.Item>
                          </div>
                        </div>
                        <div className="col-sm-6">
                          <div className="form-group">
                            <Form.Item label="Ciy">
                              <Input
                                className="form-control"
                                disabled
                                type="city"
                                placeholder="City"
                                value={city}
                              />
                            </Form.Item>
                          </div>
                        </div>

                        <div className="col-sm-6">
                          <div className="form-group">
                            <Form.Item label="Pin Code">
                              <Input
                                className="form-control"
                                disabled
                                type="postalCode"
                                placeholder="Postal Code"
                                value={pin_code}
                              />
                            </Form.Item>
                          </div>
                        </div>
                      </div>

                      <div className="row">
                        <div className="col-sm-6">
                          <div className="form-group submit">
                            <Link href="/">
                              <a className="ps-btn">Back to Home</a>
                            </Link>
                          </div>
                        </div>
                        <div className="col-sm-6">
                          <div className="form-group submit">
                            <Link href="/account/edit-user-information">
                              <a className="ps-btn">Edit</a>
                            </Link>
                          </div>
                        </div>
                      </div>
                    </div>
                  </Form>
                </div>
              </div>
            </div>
          ) : (
              <div className="ps-page--404">
                <div className="container">
                  <div className="ps-section__content">
                    <figure>
                      <img src="/static/img/404.jpg" alt="CreditKart Error" />
                    </figure>
                  </div>
                </div>
              </div>
            )}
        </div>
      </section>
    );
  }
}

const WrapFormUserInformation = Form.create()(UserInformationView);

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};
export default connect(mapStateToProps)(WrapFormUserInformation);
