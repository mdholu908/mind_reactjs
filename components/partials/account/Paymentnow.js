import React, { PureComponent } from "react";
import { connect } from "react-redux";
import Link from "next/link";
import { getPaytmData } from "../../../store/product/action";
import Router from "next/router";
import { BaseURL } from "../../../public/static/data/baseURL.json";
// const { Option } = Select;
import Loader from "../../elements/Loader";

class Paymentnow extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      resp: [],
      data: [],
      sub_total: 0.0,
      credit_amount: "",
      address: [],
      shipping_addr: [],
      billing_addr: [],
      shipping_charges: 0.0,
      method: 1,
      disabled: false,
      bgColor: "#fcb800",
      did_get_credit: false,
      token_check: null,
    };
  }

  handleChangePaymentMethod = (e) => {
    this.setState({ method: e.target.value });
  };

  handleGetAddress(data) {
    const credit = "http://127.0.0.1:8000/ecomapi/get_credit/";
    const credit_url = `${BaseURL}ecomapi/checkout-calculations/`;
    let { amount } = this.props;
    const { buynow } = this.props.cart;
    const user_id = "29";
    let Token = "Token" + " " + data.token;
    const req_data = {
      orders: buynow,
      user_id: user_id,
    };
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json", authorization: Token },
      body: JSON.stringify(req_data),
    };
    if (data.token != null && this.state.did_get_credit === false) {
      fetch(credit_url, requestOptions)
        // .then((res) => res.json())
        .then((res) => {
          if (res.ok) {
            return res.json();
          } else if (res.status === 401) {
            console.log("SOMETHING WENT WRONG")
            // this.setState({ requestFailed: true })
          }
        })
        .then((result) => {
          this.setState({
            isLoaded: true,
            did_get_credit: true,
            data: result.data,
            // billing_addr: result.address.billing_addr,
            // shipping_addr: result.address.shipping_addr,
            shipping_charges: result.data.delivery_charges,
            token_check: data.token,
          });
        })

        .catch((error) =>
          this.setState({
            isLoaded: true,
            error: error,
          })
        );
    }
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { new_orderid } = this.props.auth;
    let Token = "Token" + " " + this.state.token_check;
    const req_data1 = {
      method: "POST",
      headers: {
        // 'Accept':'application/json',
        "Content-Type": "application/json",
        authorization: Token,
      },

      body: JSON.stringify({
        shopping_order_id: new_orderid.order_id,
        CUST_ID: "9082987204",
        TXN_AMOUNT: new_orderid.amount,
      }),
    };
    fetch(`${BaseURL}ecomapi/checksum_generate_ecom_paytm_order`, req_data1)
      // .then((res) => res.json())
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else if (res.status === 401) {
          console.log("SOMETHING WENT WRONG")
          // this.setState({ requestFailed: true })
        }
      })
      .then((result) => {
        this.setState({ paytmdata: result }, () => {
          const paytmdata = this.state;
          this.props.dispatch(getPaytmData(this.state.paytmdata));
          Router.push(`/account/paytm`);
        });
      });
  };
  render() {
    const { amount, buynow } = this.props;
    const { billing_addr } = this.state;
    const { shipping_addr } = this.state;
    const { shipping_charges } = this.state;
    const { new_orderid } = this.props.auth;
    const shipping_details = {
      email: "testMail@gmail.com",
      address: "2015 South Street, Midland, Texas",
    };
    let month = [],
      year = [];
    for (let i = 1; i <= 12; i++) {
      month.push(i);
    }
    for (let i = 2019; i <= 2050; i++) {
      year.push(i);
    }
    return (
      <div>
        {this.props.auth.auth_data.token !== null ? (
          <div className="ps-checkout ps-section--shopping">
            {this.props.auth.auth_data !== null
              ? this.handleGetAddress({
                  token: this.props.auth.auth_data.token,
                })
              : null}
            {this.state.isLoaded ? (
              <div className="container">
                <div className="ps-section__header">
                  <h1>Payment</h1>
                </div>
                <div className="ps-section__content">
                  <div className="row">
                    <div className="col-xl-4 col-lg-4 col-md-12 col-sm-12 ">
                      <div className="ps-form__orders">
                        <div className="ps-block--checkout-order">
                          <div className="ps-block__content">
                            <figure>
                              <figcaption>
                                <strong>Product</strong>
                                <strong>total</strong>
                              </figcaption>
                            </figure>
                            <figure className="ps-block__items">
                              {buynow &&
                                buynow.map((product) => (
                                  <Link href="/" key={product.id}>
                                    <a>
                                      <strong>
                                        {product.title}
                                        <span>x{product.quantity}</span>
                                      </strong>
                                      <small>
                                        {product.quantity * product.price}
                                      </small>
                                    </a>
                                  </Link>
                                ))}
                            </figure>
                            <figure>
                              <figcaption>
                                <strong>Subtotal</strong>
                                <small>₹{this.state.data.total_amount}</small>
                              </figcaption>
                            </figure>
                            <figure>
                              <figcaption>
                                <strong>Credit</strong>
                                <small>- ₹{this.state.data.used_credit}</small>
                              </figcaption>
                            </figure>
                            <figure>
                              <figcaption>
                                <strong>Shipping</strong>
                                <small>
                                  + ₹ {this.state.data.delivery_charges}
                                </small>
                              </figcaption>
                            </figure>
                            <figure className="ps-block__total">
                              <h3>
                                Total
                                <strong>
                                  ₹
                                  {parseFloat(this.state.data.payable_amount) +
                                    parseFloat(
                                      this.state.data.delivery_charges
                                    )}
                                  .00
                                </strong>
                              </h3>
                            </figure>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-xl-8 col-lg-8 col-md-12 col-sm-12">
                      <div className="ps-block--shipping">
                        <div className="ps-block__panel"></div>
                        <h4>Shipping Method</h4>
                        <div className="ps-block__panel">
                          <figure>
                            <small>Shipping</small>
                            <strong>₹{shipping_charges}</strong>
                          </figure>
                        </div>
                        <br />
                        <br />
                        <br />
                        <h4>Proceed to Pay</h4>

                        <button
                          className="ps-btn ps-btn--fullwidth"
                          onClick={(e) => this.handleSubmit(e)}
                          style={{ backgroundColor: this.state.bgColor }}
                        >
                          Make a Payment
                        </button>

                        <div className="ps-block__footer">
                          <Link href="/">
                            <a>
                              <i className="icon-arrow-left mr-2"></i>
                              Return to Shopping
                            </a>
                          </Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ) : (
              <Loader />
            )}
          </div>
        ) : (
          <div className="ps-page--404">
            <div className="container">
              <div className="ps-section__content">
                <figure>
                  <img src="/static/img/404.jpg" alt="CreditKart Error" />
                </figure>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  // Redux Store --> Component
  return {
    cart: state.cart,
    auth: state.auth,
    product: state.product,
  };
};

export default connect(mapStateToProps)(Paymentnow);
