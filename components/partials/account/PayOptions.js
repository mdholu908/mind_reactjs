/* component Name: PayOptions.js
   This component gets called when user select Shipping button from CheckOut.js Page
   In payoptions.js three buttons are displayed 1. Pay now 2. Pay Later 3. Cash On delivery
   All these three buttons have associated handlers which stores payment mode in store in cart (Store/Cart)
   these payment modes are used in Shipping.js and payment.js components to pass payment modes to the api's as per button click.
 */

import React, { PureComponent } from "react";
import { connect } from "react-redux";
// import { codAmount } from "../../../store/cart/action";
import { payMode } from "../../../store/cart/action";
import FormCheckoutInformation from "./modules/FormCheckoutInformation";
import { BaseURL } from "../../../public/static/data/baseURL.json";
import Loader from "../../elements/Loader";
import Link from "next/link";
import { Col, Card, Row, Divider } from "antd";

class PayOptions extends PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			status: 0,
			credit_status: 0,
			total_amt: 0.0,
			error: null,
			isLoaded: false,
			data: [],
			cod_amt: "",
			pay_mode: "",  // payment mode is saved from api call and dispatched to the store 
			cod_mode: ""
		};
	}

	//--------------------------------------------------------------

	/* this method gets called when component is loaded
	in this method payment_mode_selection_api is called 
	api response is stored in state 
	 */

	//--------------------------------------------------------------


	componentDidMount() {
		if (this.props.auth) {
			if (this.props.auth.auth_data) {
			} else {
				alert("Kindly login");
				Router.push("/account/login");
			}
		}
		else {
			alert("Kindly login");
			Router.push("/account/login");
		}
		const credit_url = `${BaseURL}ecomapi/payment_mode_selection_api/`;

		const { cartItems } = this.props.cart;
		let { amount } = this.props;
		const user_id = "29";
		let Token = "Token" + " " + this.props.auth.auth_data.token;


		if (this.props.cart.cartItems.length >= 1) {
			if (this.props.cart.buynow.length >= 1) {
				const req_data = {
					amount: parseInt(this.props.cart.buynow[0].price * this.props.cart.buynow[0].quantity),
				};
				// alert("req_data "+JSON.stringify(req_data.orders));

				const requestOptions = {
					method: "POST",
					headers: {
						"Content-Type": "application/json",
						authorization: Token,
					},
					body: JSON.stringify(req_data),
				};

				fetch(credit_url, requestOptions)
					// .then((res) => res.json())
					.then((res) => {
						if (res.ok) {
						  return res.json();
						} else if (res.status === 401) {
						  console.log("SOMETHING WENT WRONG")
						  // this.setState({ requestFailed: true })
						}
					  })
					.then((result) => {

						this.setState({
							isLoaded: true,
							data: result,
							cod_amt: result.cod_amount.cod_amount,
							pay_mode: result.pay_now.payment_mode,
							cod_mode: result.cod_amount.payment_mode

						},

						);
					})
					.catch((error) =>
						this.setState({
							isLoaded: true,
							error: error,
						})
					);
			}

			else {
				const req_data = {
					amount: parseInt(this.props.cart.amount),
				};
				// alert("req_data "+JSON.stringify(req_data.orders));

				const requestOptions = {
					method: "POST",
					headers: {
						"Content-Type": "application/json",
						authorization: Token,
					},
					body: JSON.stringify(req_data),
				};

				// if (data.token != null && this.state.did_get_credit === false) {
				fetch(credit_url, requestOptions)
					// .then((res) => res.json())
					.then((res) => {
						if (res.ok) {
						  return res.json();
						} else if (res.status === 401) {
						  console.log("SOMETHING WENT WRONG")
						  // this.setState({ requestFailed: true })
						}
					  })
					.then((result) => {

						this.setState({
							isLoaded: true,
							data: result,
							cod_amt: result.cod_amount.cod_amount,
							pay_mode: result.pay_now.payment_mode,
							cod_mode: result.cod_amount.payment_mode
						}
						,
							
						);
					})
					.catch((error) =>
						this.setState({
							isLoaded: true,
							error: error,
						})
					);
			}
		}

		else if (this.props.cart.buynow.length >= 1) {
			const req_data = {
				amount: parseInt(this.props.cart.buynow[0].price * this.props.cart.buynow[0].quantity),
			};

			const requestOptions = {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					authorization: Token,
				},
				body: JSON.stringify(req_data),
			};

			fetch(credit_url, requestOptions)
				// .then((res) => res.json())
				.then((res) => {
					if (res.ok) {
					  return res.json();
					} else if (res.status === 401) {
					  console.log("SOMETHING WENT WRONG")
					  // this.setState({ requestFailed: true })
					}
				  })
				.then((result) => {

					this.setState({
						isLoaded: true,
						data: result,
						cod_amt: result.cod_amount.cod_amount,
						pay_mode: result.pay_now.payment_mode,
						cod_mode: result.cod_amount.payment_mode
					},


					);
				})
				.catch((error) =>
					this.setState({
						isLoaded: true,
						error: error,
					})
				);

		}
		// }
	}

	//--------------------------------------------------------------

	/* this method gets called when user clicks on Pay now Button
	   this method stores(dispatches) payment mode(pay_mode) of Pay now(cash) in store
	 */

	//--------------------------------------------------------------


	payNowhandleClickHandler = () => {

		this.props.dispatch(payMode(this.state.pay_mode))

	}

	//--------------------------------------------------------------

	/* this method gets called when user clicks on Cash On Delivery Button
	   this method stores(dispatches) payment mode(pay_mode) of cash on delivery(cod) in store
	 */

	//--------------------------------------------------------------


	cashOnDeliveryclickHandler = () => {
		this.props.dispatch(payMode(this.state.cod_mode))

	}


	//--------------------------------------------------------------


	render() {
		const cardStyle = {
			width: "100%",
			background: `linear-gradient(to right,  ${"#00FECA"} 0%,${"#2291C9"} 100%)`,
			color: "white",
			borderRadius: 20,
		};

		const white = {
			color: "white",
		};
		const { cod_amount } = this.state.data;


		return (
			<div className="container">
				<br></br>
				<center>
					<div className="col-xl-4 col-lg-4 col-md-12 col-sm-12">
						<div>
							<a onClick={this.payNowhandleClickHandler}>
								<Link href="/account/shipping">
									<Card style={cardStyle}>
										<p style={white}>Pay Now</p>
										<p style={white}>
											To avail exciting Cashback
											redeemable instantly
										</p>
										<p style={white}>
											Total Amount :{" "}
											{this.state.data.total_amount}
										</p>

										<p style={white}>
											Cashback Amount :{" "}
											{this.state.data.pay_now
												&&
												this.state.data.pay_now
													.cashback
											}
										</p>
									</Card>
								</Link>
							</a>
						</div>
						<br />
						<br />
						<div>
							<a href="https://play.google.com/store/apps/details?id=com.mudrakwik.creditkartfincom&hl=en_GB">
								<Card style={cardStyle}>
									<p
										style={{
											fontWeight: "bold",
											color: "white",
										}}
									>
										Pay Later
									</p>
									<p style={white}>
										To repay the amount in 3 subsequent EMIs
									</p>
									<p style={white}>Repayment Details</p>
									<Row>
										<Col offset={4}>
											{this.state.data.credit &&
												this.state.data.credit.map(
													(repay) => (
														<div>
															<p style={white}>
																<span>
																	Date :
																	{repay.date}
																</span>
																<br />
																<span>
																	Amount :{" "}
																	{
																		repay.amount
																	}
																</span>
															</p>
														</div>
													)
												)}
										</Col>
									</Row>
								</Card>
							</a>
						</div>

						<br />
						<br />
						{this.state.cod_amt != null ? (<div>
							<a onClick={this.cashOnDeliveryclickHandler}>
								<Link href="/account/shipping">
									<Card style={cardStyle}>
										<p style={white}>Cash On Delivery</p>
										<p style={white}>
											To avail exciting Cashback
											redeemable instantly
										</p>
										<p style={white}>
											Total Amount :{" "}
											{this.state.cod_amt}
										</p>
										
									</Card>
								</Link>
							</a>
						</div>) : ""}

					</div>
					<br></br>
				</center>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		cart: state.cart,
		auth: state.auth,
		product: state.product,

	};
};

export default connect(mapStateToProps)(PayOptions);
