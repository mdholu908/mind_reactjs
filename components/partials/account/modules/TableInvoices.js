import React, { Component } from "react";
import { Table, Divider, Tag } from "antd";
import Link from "next/link";
import { connect } from "react-redux";
import { BaseURL } from "../../../../public/static/data/baseURL.json";
class TableInvoices extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isfetched: false,
      isLoaded: false,
      data: [],
    };
  }
  handleGetInvoice(data) {
    const invoice_url = `${BaseURL}ecomapi/Invoices`;
    const url = "http://127.0.0.1:8000/ecomapi/Invoices";

    let Token = "Token" + " " + data.token;

    const requestParam = {
      headers: {
        authorization: Token,
      },
    };

    fetch(invoice_url, requestParam)
      // .then((res) => res.json())
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else if (res.status === 401) {
          console.log("SOMETHING WENT WRONG")
          // this.setState({ requestFailed: true })
        }
      })
      .then((result) => {
        this.setState({
          isLoaded: true,
          isfetched: true,
          data: result.data,
        });
      })
      .catch((error) =>
        this.setState({
          isLoaded: true,
          error: error,
        })
      );
  }

  render() {
    /*
            You can change data by API
            example: https://ant.design/components/table/
        */

    const tableData = [
      {
        id: "1",
        invoiceId: "500884010",
        title: "Marshall Kilburn Portable Wireless Speaker",
        dateCreate: "20-1-2020",
        amount: "42.99",
        status: "Successful delivery",
      },
      {
        id: "2",
        invoiceId: "593347935",
        title: "Herschel Leather Duffle Bag In Brown Color",
        dateCreate: "20-1-2020",
        amount: "199.99",
        status: "Cancel",
      },
      {
        id: "3",
        invoiceId: "593347935",
        title: "Xbox One Wireless Controller Black Color",
        dateCreate: "20-1-2020",
        amount: "199.99",
        status: "Cancel",
      },
      {
        id: "4",
        invoiceId: "615397400",
        title: "Grand Slam Indoor Of Show Jumping Novel",
        dateCreate: "20-1-2020",
        amount: "41.00",
        status: "Cancel",
      },
    ];
    const tableColumn = [
      {
        title: "Id",
        dataIndex: "invoiceId",
        rowKey: "invoiceId",
        key: "invoiceId",
        width: "120px",
        render: (text, record) => (
          // <Link href="/account/[invoicedetail]" as={`/account/${record.invoiceId}`}>
          //     {record.invoiceId}
          // </Link>
          <Link href="/account/[invoicedetail]" as={`/account/${record.id}`}>
            {record.invoiceId}
          </Link>
        ),
      },
      {
        title: "Title",
        dataIndex: "title",
        rowKey: "title",
        key: "title",
      },
      {
        title: "Date",
        rowKey: "dateCreate",
        dataIndex: "dateCreate",
        key: "dateCreate",
        // width: '120px',
        render: (text, record) => (
          <span className="text-right">{record.dateCreate}</span>
        ),
      },
      {
        title: "Amount",
        rowKey: "amount",
        dataIndex: "amount",
        key: "amount",
        render: (text, record) => (
          <span className="text-right">₹{record.amount}</span>
        ),
      },
      {
        title: "Status",
        key: "status",
        dataIndex: "status",
        rowKey: "status",
        width: "150px",
        render: (text, record) => (
          <span className="text-right">{record.status}</span>
        ),
      },
    ];
    return (
      <div>
        {/* isfetched */}
        {this.state.isfetched === false
          ? [
              <div>
                {this.props.auth.auth_data !== null
                  ? this.handleGetInvoice({
                      token: this.props.auth.auth_data.token,
                    })
                  : null}
              </div>,
            ]
          : null}
        <Table
          columns={tableColumn}
          dataSource={this.state.data}
          rowKey={(record) => record.id}
          pagination={{
            defaultPageSize: 5,
            showSizeChanger: true,
            showQuickJumper: true,
            pageSizeOptions: ["5", "10", "20", "30"],
          }}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  // Redux Store --> Component
  return {
    auth: state.auth,
  };
};
export default connect(mapStateToProps)(TableInvoices);
