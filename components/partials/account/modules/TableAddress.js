import React, { Component } from "react";
import { Table, Divider, Radio, Tag } from "antd";
import Link from "next/link";
import { connect } from "react-redux";
import {
	saveShippingAddress,
	getShippingAddress,
	serviceAvailable,
} from "../../../../store/auth/action";
import { notification } from "antd";
import { BaseURL } from "../../../../public/static/data/baseURL.json";
class TableAddress extends Component {
	constructor(props) {
		super(props);
		this.state = {
			error: null,
			isLoaded: false,
			isfetched: false,
			data: [],
			address: [],
			not_available_list: [],
			disabled: true,
			is_all_available: true,
		};
	}

	handleGetAddress(data) {
		let Token = "Token" + " " + data.token;

		const requestParam = {
			headers: {
				authorization: Token,
			},
		};
		const get_address_url = `${BaseURL}ecomapi/user_ecom_shipping_address/`;
		const url = "http://127.0.0.1:8001/ecomapi/user_ecom_shipping_address/";

		fetch(get_address_url, requestParam)
			// .then((res) => res.json())
			.then((res) => {
        if (res.ok) {
          return res.json();
        } else if (res.status === 401) {
          console.log("SOMETHING WENT WRONG")
          // this.setState({ requestFailed: true })
        }
      })
			.then((result) => {
				this.setState({
					isLoaded: true,
					data: result.address,
					isfetched: true,
				});
			})
			.catch((error) =>
				this.setState({
					isLoaded: true,
					error: error,
				})
			);
	}

	checkAddressAvaliable(data) {
		const address_url = `${BaseURL}ecomapi/sr_is_delivery_available/`;
		const { token, username } = this.props.auth.auth_data;

		let Token = "Token" + " " + token;

		const requestOptions = {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				authorization: Token,
			},
			body: JSON.stringify(data),
		};

		fetch(address_url, requestOptions)
			.then((response) => response.json())
			.then((result) => {

				let is_all_available = result.is_all_available;
				if (is_all_available == true) {
					
					this.setState({
						isLoaded: false,
						disabled: false,
						is_all_available: true,
					});
					
					// Saving service available to true
					this.props.dispatch(
						serviceAvailable(true)
					);
					

				} else {

					notification.open({
						message: "Opp! Something went wrong.",
						description: "Address not Serviceable, Please select/ add another address",
						duration: 4,
					});

					this.setState({
						not_available_list: result.not_available_list,
						disabled: true,
						is_all_available: false,
					});
				}
			})
			.catch((error) =>
				this.setState({
					isLoaded: true,
					error: error,
				})
			);
	}

	onSelectChange = (selectedRowKeys) => {
		// this.setState({ selectedRowKeys });
		if (selectedRowKeys.length > 1) {
			const lastSelectedRowIndex = [...selectedRowKeys].pop();
			//   this.setState({ selectedRowKeys: lastSelectedRowIndex });
		}
		// this.setState({ selectedRowKeys });
	};

	render() {
		/*
            You can change data by API
            example: https://ant.design/components/table/
        */
		const title = () => "Select Shipping Address";
		let rowSelection = [];
		const selectedRowKeys = [];
		const columns = [
			{
				title: "Name",
				dataIndex: "name",
				render: (text) => <a>{text}</a>,
			},
			{
				title: "Mobile",
				dataIndex: "mobile",
				render: (text) => <a>{text}</a>,
			},
			{
				title: "Address",
				dataIndex: "address",
			},
			{
				title: "State",
				dataIndex: "state",
			},
			{
				title: "City",
				dataIndex: "city",
			},

			{
				title: "Pincode",
				dataIndex: "pin_code",
			},
		];

		return (
			<div>
				{/* isfetched */}
				{this.state.isfetched === false
					? [
							<div>
								{this.props.auth.auth_data.token !== null
									? this.handleGetAddress({
											token: this.props.auth.auth_data
												.token,
									  })
									: null}
							</div>,
					  ]
					: null}

				<Table
					rowSelection={{
						type: "radio",
						...rowSelection,
						onChange: (selectedRowKeys, selectedRows) => {
							this.setState({
								address: selectedRows[0].address_id,
							});
							this.props.dispatch(
								saveShippingAddress(selectedRows[0].address_id)
							);

							const selected_address_data = selectedRows[0];
							this.props.dispatch(
								getShippingAddress(selected_address_data)
							);

							// check service avaliability
							const { cartItems } = this.props.cart;
							const prod_id_list = [];

							var tifOptions = Object.keys(cartItems).map(
								function (key) {
									prod_id_list.push(cartItems[key].id);
								}
							);
							// const pincode_data = {
							// 	product_details_id_list: [
							// 		113,
							// 		25909,
							// 		1448,
							// 		5321,
							// 	],
							// 	pincode: selectedRows[0].pin_code,
							// };

							const pincode_data = {
								product_details_id_list: prod_id_list,
								pincode: selectedRows[0].pin_code,
							};
							console.log(
								"--checkAddressAvaliable",
								pincode_data
							);

							this.checkAddressAvaliable(pincode_data);
						},
					}}
					columns={columns}
					dataSource={this.state.data}
					title={title}
					// rowKey={record => record.id}
					rowKey={(record) => record.uid}
					pagination={{
						defaultPageSize: 5,
						showSizeChanger: true,
						showQuickJumper: true,
						pageSizeOptions: ["5", "10", "20", "30"],
					}}
				/>
			</div>
		);
	}
}
// export default TableAddress;

const mapStateToProps = (state) => {
	return {
		cart: state.cart,
		auth: state.auth,
	};
};

export default connect(mapStateToProps)(TableAddress);
