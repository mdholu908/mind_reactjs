import React, { Component } from "react";
import { BaseURL } from "../../../../public/static/data/baseURL.json";
class FormEditAddress extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      data: [],
    };
  }
  componentDidMount() {
    fetch(`${BaseURL}loan-app-data/user-ecom-address/`)
      // .then((res) => res.json())
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else if (res.status === 401) {
          console.log("SOMETHING WENT WRONG")
          // this.setState({ requestFailed: true })
        }
      })
      .then((result) => {
        this.setState({
          isLoaded: true,
          data: result.data,
        });
      })
      .catch((error) =>
        this.setState({
          isLoaded: true,
          error: error,
        })
      );
  }
  
}

export default FormEditAddress;
