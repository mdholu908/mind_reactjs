import React, { Component } from "react";
import { Table, Divider, Tag } from "antd";
import { connect } from "react-redux";
import { BaseURL } from "../../../../public/static/data/baseURL.json";
class TableUserCredit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      isfetched: false,
      data: [],
      credit: [],
    };
  }

  handleGetCredit(data) {
    let Token = "Token" + " " + data.token;

    const requestParam = {
      headers: {
        authorization: Token,
      },
    };
    const get_address_url = `${BaseURL}ecomapi/user_credit_details/`;
    const url = "http://127.0.0.1:8000/ecomapiuser_ecom_shipping_address/";

    fetch(get_address_url, requestParam)
      // .then((res) => res.json())
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else if (res.status === 401) {
          console.log("SOMETHING WENT WRONG")
          // this.setState({ requestFailed: true })
        }
      })
      .then((result) => {
        this.setState({
          isLoaded: true,
          data: result.Data,
          isfetched: true,
        });
      })
      .catch((error) =>
        this.setState({
          isLoaded: true,
          error: error,
        })
      );
  }
  render() {
    const tableData = [
      {
        key: "1",
        title: "---------------commodo ligula eget dolor",
        dateCreate: "20-1-2020",
        tags: ["sale"],
        responsive: ["xs"],
      },
      {
        key: "2",
        title: "voluptate velit esse quam nihil molestiae consequatur",
        dateCreate: "21-1-2020",
        tags: ["new"],
      },
      {
        key: "3",
        title: " Et harum quidem rerum",
        dateCreate: "21-1-2020",
        tags: ["new"],
        responsive: ["xs"],
      },
      {
        key: "4",
        title: "Neque porro quisquam est",
        dateCreate: "21-1-2020",
        tags: ["sale"],
        responsive: ["xs"],
        //                width: '100px',
      },
    ];
    const tableColumn = [
      {
        title: "Date",
        dataIndex: "Created_Date",
        key: "Created_Date",
        render: (text) => <a>{text}</a>,
        // width: "150px",
      },
      {
        title: "Credit",
        dataIndex: "Credit",
        key: "Credit",
      },
      {
        title: "Balance Credit",
        dataIndex: "Balance_Credit",
        key: "Balance_Credit",
      },
      {
        title: "User Credit",
        key: "Used_Credit",
        dataIndex: "Used_Credit",
      },
      {
        title: "Actual used",
        key: "Actual_Used",
        dataIndex: "Actual_Used",
      },
      {
        title: "Repayment",
        key: "Repayment_Add",
        dataIndex: "Repayment_Add",
      },

      {
        title: "Status",
        key: "Status",
        dataIndex: "Status",
      },
    ];
    return (
      <div className="col-xl-8 col-lg-8 col-md-12 col-sm-12">
        {this.state.isfetched === false
          ? [
            <div>
              {this.props.auth.auth_data.token !== null
                ? this.handleGetCredit({
                  token: this.props.auth.auth_data.token,
                })
                : null}
            </div>,
          ]
          : null}
        <Table
          columns={tableColumn}
          dataSource={this.state.data}
          pagination={{
            defaultPageSize: 5,
            showSizeChanger: true,
            showQuickJumper: true,
            pageSizeOptions: ["5", "10", "20", "30"],
          }}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

export default connect(mapStateToProps)(TableUserCredit);
