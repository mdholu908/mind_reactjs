import React, { Component } from "react";
import Link from "next/link";
import Router from "next/router";
import { Form, Input } from "antd";
import { connect } from "react-redux";
import { login } from "../../../../store/auth/action";
import {
	saveShippingAddress,
	serviceAvailable,
} from "../../../../store/auth/action";
import { clearCart } from "../../../../store/cart/action";
import TableAddress from "./TableAddress";
import { BaseURL } from "../../../../public/static/data/baseURL.json";

class FormCheckoutInformation extends Component {
	constructor(props) {
		super(props);
		this.state = {
			error: null,
			isLoaded: false,
			isfetched: false,
			address_present: false,
			address_data: [],
			data: [],
			address_id: null,
			fields_required: true,
			is_delivery_available: true,
			address: {
				pincode: "",
				city: "",
				state: "",
			},
		};
	}

	handleIsLogin() {
		if (this.props.auth.isLoggedIn === true) {
			return true;
		} else {
			return false;
		}
	}

	handleIsAddressTypeChange() {
		this.setState({
			address_present: !this.state.address_present,
		});
		// TODO set shipping addr to null
	}

	handleAddressSubmit(data) {
		const url = "http://127.0.0.1:8001/ecomapi/user-ecom-address";
		const address_url = `${BaseURL}ecomapi/user-ecom-address`;
		const { token, username } = this.props.auth.auth_data;
		let Token = "Token" + " " + token;

		const requestOptions = {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				authorization: Token,
			},
			body: JSON.stringify(data),
		};

		fetch(address_url, requestOptions)
			// .then((response) => response.json())
			.then((res) => {
				if (res.ok) {
				  return res.json();
				} else if (res.status === 401) {
				  console.log("SOMETHING WENT WRONG")
				  // this.setState({ requestFailed: true })
				}
			  })
			.then((result) => {

				let status_code = result.status;
				if (
					status_code == "1" ||
					status_code == 1 ||
					status_code == "2" ||
					status_code == 2
				) {
					this.setState({
						isLoaded: false,
						address_id: result.address_id,
					});
					if (result.address_id != null) {
						this.props.dispatch(
							saveShippingAddress(result.address_id)
						);
					}
				}
			})
			.catch((error) =>
				this.setState({
					isLoaded: true,
					error: error,
				})
			);
	}

	handleGetAddress(data) {
		let Token = "Token" + " " + data.token;

		const requestParam = {
			headers: {
				authorization: Token,
			},
		};
		const get_address_url = `${BaseURL}ecomapi/user_ecom_shipping_address/`;
		const url = "http://127.0.0.1:8001/ecomapi/user_ecom_shipping_address/";

		fetch(get_address_url, requestParam)
			// .then((res) => res.json())
			.then((res) => {
				if (res.ok) {
				  return res.json();
				} else if (res.status === 401) {
				  console.log("SOMETHING WENT WRONG")
				  // this.setState({ requestFailed: true })
				}
			  })
			.then((result) => {
				this.setState({
					isLoaded: true,
					data: result.address,
					isfetched: true,
				});
				if (result.status_code === "1") {
					this.setState({
						address_present: true,
						// address_present: false,
						address_data: result.address,
					});
				}
			})
			.catch((error) =>
				this.setState({
					isLoaded: true,
					error: error,
				})
			);
	}

	checkAddressAvaliable(data) {
		const url = "http://127.0.0.1:8001/ecomapi/user-ecom-address";
		const address_url = `${BaseURL}ecomapi/sr_is_delivery_available/`;
		const { token, username } = this.props.auth.auth_data;

		let Token = "Token" + " " + token;

		const requestOptions = {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				authorization: Token,
			},
			body: JSON.stringify(data),
		};

		fetch(address_url, requestOptions)
			// .then((response) => response.json())
			.then((res) => {
				if (res.ok) {
				  return res.json();
				} else if (res.status === 401) {
				  console.log("SOMETHING WENT WRONG")
				  // this.setState({ requestFailed: true })
				}
			  })
			.then((result) => {
				// alert("not servic");
				let is_all_available = result.is_all_available;
				if (is_all_available == true) {
					this.setState({
						isLoaded: false,
						disabled: false,
						is_all_available: true,
						is_delivery_available: true,
					});

					// Saving service available to true
					this.props.dispatch(serviceAvailable(true));
				} else {
					this.setState({
						not_available_list: result.not_available_list,
						disabled: true,
						is_all_available: false,
						is_delivery_available: false,
					});
				}
			})
			.catch((error) =>
				this.setState({
					isLoaded: true,
					error: error,
				})
			);
	}

	pinCodeApi = (param, event) => {
		let pincode = event.target.value;
		if (pincode.length === 6) {
			const requestOptions = {
				method: "POST",
				headers: { "Content-Type": "application/json" },
				body: JSON.stringify({
					pincode: pincode,
				}),
			};
			const pincode_api_url = `${BaseURL}ecomapi/ecom_validate_pin_code/`;
			fetch(pincode_api_url, requestOptions)
				// .then((res) => res.json())
				.then((res) => {
					if (res.ok) {
					  return res.json();
					} else if (res.status === 401) {
					  console.log("SOMETHING WENT WRONG")
					  // this.setState({ requestFailed: true })
					}
				  })
				.then((result) => {
					let status_code = result.status;
					if (status_code == "1" || status_code == 1) {
						this.setState({
							address: {
								pincode: pincode,
								city: result.city,
								state: result.state,
							},
							is_delivery_available: result.is_delivery_available,
						});
						this.props.form.setFieldsValue({
							city: this.capitalizeFirstLetter(result.city),
							state: this.capitalizeFirstLetter(result.state),
						});

						// check service avaliability
						const { cartItems } = this.props.cart;
						const prod_id_list = [];

						var tifOptions = Object.keys(cartItems).map(function (
							key
						) {
							console.log(
								"In getProductIdList",
								key,
								cartItems[key],
								cartItems[key].id
							);
							prod_id_list.push(cartItems[key].id);
						});

						const pincode_data = {
							product_details_id_list: prod_id_list,
							pincode: pincode,
						};
						this.checkAddressAvaliable(pincode_data);
					}
				});
		}
	};
	capitalizeFirstLetter(string) {
		if (string != null || string != "") {
			return string.charAt(0).toUpperCase() + string.slice(1);
		} else {
			return "";
		}
	}

	handleEmptyCart = (e) => {
		e.preventDefault();
		const { product } = this.props;
		this.props.dispatch(addItem(product));
	};

	handleLoginSubmit = (e) => {
		e.preventDefault();

		let islogin = this.handleIsLogin();
		if (islogin === true) {
			this.props.form.validateFields((err, values) => {
				if (!err) {
					let data = this.props.form.getFieldsValue();

					let name = data.firstName + " " + data.lastName;

					Object.assign(data, { name: name });

					Object.assign(data, { type: "Shipping" });
					if (this.state.address_present === false) {
						this.handleAddressSubmit(data);
						// address_present: false,
					}
					const addr_data = this.props.auth.shipping_address_selected;
					if (this.props.auth.is_service_available) {
						if (addr_data) {
							Router.push("/account/payoptions");
							Router.onRouteChangeComplete = () => {
								window.scroll({
								  top: 0,
								  left: 0,
								});
							  };
						} else {
							// Saving service available to false
							this.props.dispatch(serviceAvailable(false));
							alert("Kindly select address");
						}
					} else {
						alert("Kindly select/add new address");
					}
				}
			});
		}
	};

	render() {
		//alert("In formcheckoutinformation render "+JSON.stringify(this.props));

		const { getFieldDecorator } = this.props.form;
		const { amount, cartItems, cartTotal } = this.props;
		const a = {
			backgroundColor: "#cccccc",
			border: "1px solid #999999",
			color: "#666666",
		};

		return (
			<div>
				{this.props.auth.auth_data !== null
					? [
						<div>
							{this.state.isfetched === false
								? [
									<div>
										{this.props.auth.auth_data !==
											null
											? this.handleGetAddress({
												token: this.props
													.auth.auth_data
													.token,
											})
											: null}
									</div>,
								]
								: null}
						</div>,
					]
					: null}
				<Form
					className="ps-form--checkout"
					onSubmit={this.handleLoginSubmit}
				>
					<div className="ps-form__content">
						<div className="row">

							{/* {this.state.address_present === true ? (
								<div
									className="ps-form--checkout"
									style={{ width: "60%", paddingRight: "50px" }}
								>
									<div>
										<div className="col-lg-12">
											<div className="ps-section__content">
												<div className="ps-page__left">
													<div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 ps-section__content">
														<div className="">
															<h3>Address</h3>
														</div>
														<TableAddress
															address_data={
																this.state
																	.address_data
															}
														/>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							) : null} */}

							<div className="col-xl-4 col-lg-4 col-md-12 col-sm-12  ps-block--checkout-order">
								<div className="ps-form__orders">
									<h3>Your order</h3>
									<div className="ps-block--checkout-order">
										<div className="ps-block__content">
											<figure>
												<figcaption>
													<strong>Product</strong>
													<strong>total</strong>
												</figcaption>
											</figure>
											<figure className="ps-block__items">
												{cartItems &&
													cartItems.map((product) => (
														<Link
															href="/"
															key={product.id}
														>
															{/* <img src={product.thumbnail} alt="CreditKart"></img> */}
															<a>
																<img style={{ width: "70px", height: "70px" }} src={product.thumbnail} alt="CreditKart"></img> &nbsp;&nbsp;
																<strong>
																	{
																		product.title
																	}
																	<br />
																	<span>
																		x
																		{
																			product.quantity
																		}
																	</span>
																</strong>

																<small>
																	₹
																	{product.quantity *
																		product.price}
																</small>
															</a>
														</Link>
													))}
											</figure>
											<figure>
												<figcaption>
													<strong>Subtotal</strong>
													<small>₹{amount}</small>
												</figcaption>
											</figure>
											<figure className="ps-block__shipping">
												<h3>Shipping</h3>
												<p>Calculated at next step</p>
											</figure>
										</div>
									</div>
								</div>
								{this.state.is_delivery_available ? (
									<div className="ps-block__footer">
										<br />
										<button
											className="ps-btn1"
											type="submit"
										>
											Continue to shipping
											</button>
									</div>
								) : (
										<div className="ps-block__footer">
											<button
												className="ps-btn1"
												type="submit"
												style={a}
												disabled="true"
											>
												Continue to shipping
											</button>
										</div>
									)}

							</div>

							{this.state.address_present === false ? (
								<div className="col-xl-8 col-lg-8 col-md-12 col-sm-12">
									<div className="ps-form__billing-info">
										<h3 className="ps-form__heading">
											Contact information
										</h3>
										<div className="form-group">
											<Form.Item>
												{getFieldDecorator("email", {
													rules: [
														{
															required: false,
															message:
																"Enter an email!",
														},
													],
												})(
													<Input
														className="form-control"
														type="text"
														placeholder="Email"
													/>
												)}
											</Form.Item>
										</div>
										<div className="row">
											<div className="col-sm-6">
												<div className="form-group">
													<Form.Item>
														{getFieldDecorator(
															"mobile",
															{
																rules: [
																	{
																		required: this
																			.state
																			.fields_required,
																		message:
																			"Enter a phone number!",
																	},
																],
															}
														)(
															<Input
																className="form-control"
																type="number"
																placeholder="Phone Number"
																maxLength="10"
															/>
														)}
													</Form.Item>
												</div>
											</div>
										</div>

										<h3 className="ps-form__heading">
											Shipping address
										</h3>
										<div className="row">
											<div className="col-sm-6">
												<div className="form-group">
													<Form.Item>
														{getFieldDecorator(
															"firstName",
															{
																rules: [
																	{
																		required: this
																			.state
																			.fields_required,
																		message:
																			"Enter your first name!",
																	},
																],
															}
														)(
															<Input
																className="form-control"
																type="text"
																placeholder="First Name"
															/>
														)}
													</Form.Item>
												</div>
											</div>
											<div className="col-sm-6">
												<div className="form-group">
													<Form.Item>
														{getFieldDecorator(
															"lastName",
															{
																rules: [
																	{
																		required: this
																			.state
																			.fields_required,
																		message:
																			"Enter your last name!",
																	},
																],
															}
														)(
															<Input
																className="form-control"
																type="text"
																placeholder="Last Name"
															/>
														)}
													</Form.Item>
												</div>
											</div>
										</div>
										<div className="form-group">
											<Form.Item>
												{getFieldDecorator("address", {
													rules: [
														{
															required: this.state
																.fields_required,
															message:
																"Enter an address!",
														},
													],
												})(
													<Input
														className="form-control"
														type="text"
														placeholder="Address"
													/>
												)}
											</Form.Item>
										</div>
										<div className="form-group">
											<Form.Item>
												{getFieldDecorator(
													"apartment",
													{
														rules: [
															{
																required: this
																	.state
																	.fields_required,
																message:
																	"Enter an Apartment!",
															},
														],
													}
												)(
													<Input
														className="form-control"
														type="text"
														placeholder="Apartment, suite, etc. (optional)"
													/>
												)}
											</Form.Item>
										</div>
										<div className="row">
											<div className="col-sm-6">
												<div className="form-group">
													<Form.Item>
														{getFieldDecorator(
															"pincode",
															{
																rules: [
																	{
																		required: this
																			.state
																			.fields_required,
																		message:
																			"Enter a postal code!",
																	},
																],
															}
														)(
															<Input
																className="form-control"
																type="postalCode"
																placeholder="Postal Code"
																maxLength="6"
																onChange={this.pinCodeApi.bind(
																	this,
																	"pancard"
																)}
															/>
														)}
													</Form.Item>
												</div>
											</div>
										</div>
										<div className="row">
											<div className="col-sm-6">
												<div className="form-group">
													<Form.Item>
														{getFieldDecorator(
															"city",
															{
																rules: [
																	{
																		required: this
																			.state
																			.fields_required,
																		message:
																			"Enter a city!",
																	},
																],
															}
														)(
															<Input
																className="form-control"
																type="city"
																placeholder="City"
															/>
														)}
													</Form.Item>
												</div>
											</div>
											<div className="col-sm-6">
												<div className="form-group">
													<Form.Item>
														{getFieldDecorator(
															"state",
															{
																rules: [
																	{
																		required: this
																			.state
																			.fields_required,
																		message:
																			"Enter a state!",
																	},
																],
															}
														)(
															<Input
																className="form-control"
																type="state"
																placeholder="state"
															/>
														)}
													</Form.Item>
												</div>
											</div>
										</div>
									</div>
									<div>
										<div>
											<div className="form-group">
												<div className="ps-block__footer">
													<button
														className="ps-btn  ps-btn--sm" style={{float:"right"}}
														onClick={this.handleIsAddressTypeChange.bind(
															this
														)}
													>
														{this.state.address_present ? (
															<p>Add new address</p>
														) : (
																<p>select address</p>
															)}
													</button>
												</div>
											</div>
										</div>
										<div style={{ margin: "2%" }}>
											<span style={{ color: "red" }}>
												<strong>
													Important: Shipping and Billing
													Address will be same
										</strong>
											</span>
											<br />

											{this.state.is_delivery_available ? null : (
												<span style={{ color: "red" }}>
													<strong>Note:</strong> Could not be
													delivered on this pincode
										</span>
											)}
										</div>
										<div
											className="ps-form__submit"
											style={{ marginTop: "2%" }}
										>
											<Link href="/">
												<a style={{ paddingRight: "10px" }}>
													<i className="icon-arrow-left mr-2" ></i>
													Return to shopping
										</a>
											</Link>
											{/* {this.state.is_delivery_available ? (
										<div className="ps-block__footer">
											<br />
											<button
												className="ps-btn"
												type="submit"
											>
												Continue to shipping
											</button>
										</div>
									) : (
											<div className="ps-block__footer">
												<button
													className="ps-btn"
													type="submit"
													style={a}
													disabled="true"
												>
													Continue to shipping
											</button>
											</div>
										)} */}
										</div>
									</div>
								</div>
							) : null}




							{this.state.address_present === true ? (
								<div
									className="ps-form--checkout"
									style={{ width: "60%", paddingLeft: "150px" }}
								>
									<div>
										<div className="col-lg-12">
											<div className="ps-section__content">
												<div className="ps-page__left">
													<div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 ps-section__content">
														<div className="">
															<h3>Address</h3>
														</div>
														<TableAddress
															address_data={
																this.state
																	.address_data
															}
														/>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div>
										<div>
											<div className="form-group">
												<div className="ps-block__footer">
													<button
														className="ps-btn  ps-btn--sm" style={{float:"right"}}
														onClick={this.handleIsAddressTypeChange.bind(
															this
														)}
													>
														{this.state.address_present ? (
															<p>Add new address</p>
														) : (
																<p>select address</p>
															)}
													</button>
												</div>
											</div>
										</div>
										<div style={{ margin: "2%" }}>
											<span style={{ color: "red" }}>
												<strong>
													Important: Shipping and Billing
													Address will be same
										</strong>
											</span>
											<br />

											{this.state.is_delivery_available ? null : (
												<span style={{ color: "red" }}>
													<strong>Note:</strong> Could not be
													delivered on this pincode
										</span>
											)}
										</div>
										<div
											className="ps-form__submit"
											style={{ marginTop: "2%" }}
										>
											<Link href="/">
												<a style={{ paddingRight: "10px" }}>
													<i className="icon-arrow-left mr-2" ></i>
													Return to shopping
										</a>
											</Link>
											{/* {this.state.is_delivery_available ? (
										<div className="ps-block__footer">
											<br />
											<button
												className="ps-btn"
												type="submit"
											>
												Continue to shipping
											</button>
										</div>
									) : (
											<div className="ps-block__footer">
												<button
													className="ps-btn"
													type="submit"
													style={a}
													disabled="true"
												>
													Continue to shipping
											</button>
											</div>
										)} */}
										</div>
									</div>
								</div>
							) : null}


						</div>
					</div>
				</Form>
			</div>
		);
	}
}

const WrapForm = Form.create()(FormCheckoutInformation);

const mapStateToProps = (state) => {
	return {
		setting: state.setting,
		cart: state.cart,
		auth: state.auth,
		Product: state.product
	};
};

export default connect(mapStateToProps)(WrapForm);
