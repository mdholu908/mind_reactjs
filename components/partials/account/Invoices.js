import React, { PureComponent } from "react";
import AccountMenuSidebar from "./modules/AccountMenuSidebar";
import TableInvoices from "./modules/TableInvoices";

class Invoices extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      data: [],
    };
  }

  render() {
    const accountLinks = [
      {
        text: "Invoices",
        url: "/account/invoices",
        icon: "icon-papers",
        active: true,
      },
    ];
    return (
      <section className="ps-my-account ps-page--account">
        <div className="container">
          <div className="row">
            <div className="col-lg-8">
              <div className="ps-page__content">
                <div className="ps-section--account-setting">
                  <div className="ps-section__header">
                    <h3>Invoices</h3>
                  </div>
                  <div
                    style={{ overflowX: "auto" }}
                    className="ps-section__content"
                  >
                    <TableInvoices />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Invoices;
