import React, { PureComponent } from "react";
import { connect } from "react-redux";
import OrderCancelPopup from "../../shared/OrderCancelPopup";
import OrderReturnPopup from "../../shared/OrderReturnPopup";
import OrderExchangePopup from "../../shared/OrderExchangePopup";
import { Pagination } from "antd";

import Loader from "../../elements/Loader";
import fetch from "isomorphic-fetch";
import { BaseURL } from "../../../public/static/data/baseURL.json";
class Creditrepay extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      isfetched: false,
      history: [],
      paytmdata: [],
      alldata: [],
      currentPage: 1,

      amount: 0.0,
      return_status: true,
      credit_amount: 10,
      token_data: null,
      newdata: null,
      returndata: null,
      exchangedata: null,
      order_id_send: null,
      return_msg: null,
    };
    // this.setState({token_data:this.props.auth.auth_data.token})
  }

  handleChange = (e) => {
    const re = /^[0-9\b]+$/;
    //   if (e.target.value === '' || re.test(e.target.value)) {
    this.setState({ credit_amount: e.target.value });
    //   }
    //   else{
    //       this.setState({credit_amount:120})
    //   }
  };
  handlemsg() {
    this.setState({ return_msg: "Option not valid" });
  }
  handlecancel = (e, oid) => {
    e.preventDefault();
    let Token = "Token" + " " + this.props.auth.auth_data.token;

    const req_data1 = {
      method: "POST",
      headers: {
        // 'Accept':'application/json',
        "Content-Type": "application/json",
        authorization: Token,
      },
      body: JSON.stringify({
        order_id: oid,
      }),
    };
    fetch(`${BaseURL}ecomapi/order-cancel`, req_data1)
      // .then((res) => res.json())
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else if (res.status === 401) {
          console.log("SOMETHING WENT WRONG")
          // this.setState({ requestFailed: true })
        }
      })
      .then((result) => {
        this.setState({ newdata: result, order_id_send: oid }, () => {});
      });
  };

  handlereturn = (e, oid) => {
    e.preventDefault();
    let Token = "Token" + " " + this.props.auth.auth_data.token;
    const req_data2 = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        authorization: Token,
      },
      body: JSON.stringify({
        order_id: oid,
      }),
    };
    fetch(`${BaseURL}ecomapi/order-return`, req_data2)
      // .then((res) => res.json())
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else if (res.status === 401) {
          console.log("SOMETHING WENT WRONG")
          // this.setState({ requestFailed: true })
        }
      })
      .then((result) => {
        this.setState({ returndata: result, order_id_send: oid });
      });
  };

  handleexchange = (e, oid) => {
    e.preventDefault();
    // let Token = "Token" + " " + this.props.auth.auth_data.token;
    const req_data3 = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        // authorization: Token,
      },
      body: JSON.stringify({
        order_id: oid,
      }),
    };
    fetch(`${BaseURL}ecomapi/get_order_variant/`, req_data3)
      // .then((res) => res.json())
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else if (res.status === 401) {
          console.log("SOMETHING WENT WRONG")
          // this.setState({ requestFailed: true })
        }
      })
      .then((result) => {
        this.setState({ exchangedata: result, order_id_send: oid });
      });
  };

  resetData() {
    this.setState({
      newdata: null,
      returndata: null,
      exchangedata: null,
    });
  }

  orderdata(data) {
    let Token = "Token" + " " + data.token;
    const req_data = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        authorization: Token,
      },
    };
    fetch(`${BaseURL}ecomapi/user-order-history-api`, req_data)
      // .then((res) => res.json())
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else if (res.status === 401) {
          console.log("SOMETHING WENT WRONG")
          // this.setState({ requestFailed: true })
        }
      })
      .then((result) => {
        this.setState({
          isLoaded: true,
          alldata: result.data,
          isfetched: true,
          totalPages: Math.ceil(result.data.length / 3),
          currentPage: 1,
          history: result.data.slice(0, 3),
        });
      })
      .catch((error) =>
        this.setState({
          isLoaded: true,
          error: error,
        })
      );
  }
  onPageChange = (page) => {
    let start = 3 * (page - 1);
    let end = 3 * page;
    this.setState({
      currentPage: page,
      history: this.state.alldata.slice(start, end),
    });
  };

  render() {
    const { history } = this.state;

    return (
      <div>
        {this.state.newdata !== null ? (
          <div>
            <OrderCancelPopup
              newdata={this.state.newdata}
              order_id={this.state.order_id_send}
              key={this.state.newdata.status}
            />
            {setTimeout(() => {
              {
                this.resetData();
              }
            }, 10000)}
          </div>
        ) : null}
        {this.state.returndata !== null ? (
          <div>
            <OrderReturnPopup
              returndata={this.state.returndata}
              order_id={this.state.order_id_send}
              key={this.state.returndata.status}
            />
            {setTimeout(() => {
              {
                this.resetData();
              }
            }, 10000)}
          </div>
        ) : null}
        {this.state.exchangedata !== null ? (
          <div>
            <OrderExchangePopup
              exchangedata={this.state.exchangedata}
              order_id={this.state.order_id_send}
              key={this.state.exchangedata.status}
            />
            {setTimeout(() => {
              {
                this.resetData();
              }
            }, 10000)}
          </div>
        ) : null}
        {this.props.auth.auth_data !== null ? (
          <div>
            {this.state.isfetched === false
              ? this.orderdata({ token: this.props.auth.auth_data.token })
              : null}
            {this.state.isLoaded ? (
              <div>
                {this.state.history.length !== 0 ? (
                  <div className="container">
                    <aside className="widget widget_footer widget_contact-us">
                      <div className="ps-section--shopping ps-shopping-cart">
                        <div className="ps-section__header">
                          <h1>Order History</h1>
                        </div>

                        <div className="ps-section__content">
                          {history.map((order_data) => (
                            <div
                              key={order_data.order_details_id}
                              className="ps-page--repay"
                            >
                              {order_data.orders.length !== 0 ? (
                                <div>
                                  <h3 style={{ color: "#353535" }}>
                                    OrderID : {order_data.order_text}
                                  </h3>
                                  <h4 style={{ color: "#606161" }}>
                                    Order Date : {order_data.date}
                                  </h4>

                                  <div className="table-responsive">
                                    <table className="table .ps-table--vendor-status ">
                                      <thead>
                                        <tr></tr>
                                      </thead>
                                      <tbody>
                                        {order_data.orders.map((prod) => (
                                          <tr key={prod.order_id}>
                                            <td>
                                              <a style={{ color: "black" }}>
                                                <img
                                                  style={{
                                                    maxWidth: "100px",
                                                    maxHeight: "100px",
                                                  }}
                                                  src={prod.thumbnail}
                                                  alt="Creditkart"
                                                />
                                              </a>
                                            </td>

                                            {prod.status === "placed" ? (
                                              <td className="widget_content">
                                                <a>{prod.title}</a>
                                                <p style={{ color: "black" }}>
                                                  {prod.unique_order_id}
                                                  <br />
                                                  <strong
                                                    style={{ color: "black" }}
                                                  >
                                                    {prod.status}
                                                  </strong>
                                                  <br />
                                                  <button
                                                    className=" ps-btn"
                                                    onClick={(e) =>
                                                      this.handlecancel(
                                                        e,
                                                        prod.order_id
                                                      )
                                                    }
                                                  >
                                                    cancel
                                                  </button>
                                                </p>
                                              </td>
                                            ) : (
                                              <td className="widget_content">
                                                <strong
                                                  style={{ color: "black" }}
                                                >
                                                  {prod.title}
                                                </strong>
                                                <p style={{ color: "black" }}>
                                                  {prod.unique_order_id}
                                                  <br />
                                                  <strong
                                                    style={{ color: "black" }}
                                                  >
                                                    {prod.status}
                                                  </strong>
                                                  <br />

                                                  {prod.return_status ? (
                                                    <div>
                                                      <button
                                                        className=" ps-btn1"
                                                        onClick={(e) =>
                                                          this.handlereturn(
                                                            e,
                                                            prod.order_id
                                                          )
                                                        }
                                                      >
                                                        return
                                                      </button>
                                                    </div>
                                                  ) : (
                                                    <div>
                                                      {prod.status ===
                                                      "delivered" ? (
                                                        <a
                                                          style={{
                                                            color: "red",
                                                          }}
                                                        >
                                                          This item is no longer
                                                          eligible for return.{" "}
                                                        </a>
                                                      ) : (
                                                        <div>
                                                          {prod.status !==
                                                            "cancelled" &&
                                                          prod.status !==
                                                            "return_request" &&
                                                          prod.status !==
                                                            "initiated" &&
                                                          prod.status !==
                                                            "return_confirm" ? (
                                                            <a>
                                                              Order shipped:
                                                              Order cannot be
                                                              cancelled.
                                                            </a>
                                                          ) : null}
                                                        </div>
                                                      )}
                                                    </div>
                                                  )}
                                                </p>
                                              </td>
                                            )}
                                          </tr>
                                        ))}
                                      </tbody>
                                    </table>
                                  </div>

                                  <div className="form-group submit"></div>
                                </div>
                              ) : null}
                            </div>
                          ))}
                        </div>
                      </div>
                    </aside>
                    <Pagination
                      defaultCurrent={1}
                      total={this.state.totalPages * 10}
                      onChange={this.onPageChange}
                    />
                  </div>
                ) : (
                  // </div>
                  <div className="ps-page--404">
                    <div className="container">
                      <div className="ps-section__content">
                        <figure>
                          <img
                            src="/static/img/404.jpg"
                            alt="CreditKart Error"
                          />
                        </figure>
                      </div>
                    </div>
                  </div>
                )}
              </div>
            ) : (
              <Loader />
            )}
          </div>
        ) : (
          <div className="ps-page--404">
            <div className="container">
              <div className="ps-section__content">
                <figure>
                  <img
                    src="/static/img/errors/loggingerror.png"
                    alt="CreditKart Error"
                  />
                </figure>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return { produt: state.product, auth: state.auth };
};

export default connect(mapStateToProps)(Creditrepay);
