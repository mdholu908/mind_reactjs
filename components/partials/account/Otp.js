/* Component name: Otp.js this page gets called after user clicks 
   on Register Button from Registeration Page. 
*/

import React, { PureComponent } from "react";
import Link from "next/link";
import Router from "next/router";
import { login, saveUser } from "../../../store/auth/action";
import { Form, Input, notification } from "antd";
import { connect } from "react-redux";
import { BaseURL } from "../../../public/static/data/baseURL.json";


class Otp extends PureComponent {
  constructor(props) {
    super(props);


    this.state = {
      error: null,
      isLoaded: false,
      is_profile_filled: false,
      data: null,
      entered_otp: "",
      pin_length: "",
      entered_pin: "",
      new_result: null,
    };
  }

  //------------------------------------------------------------------

  // this method gets called after page is rendered.

  componentDidMount() {
    this.handleParseUrl();

  }

  //-------------------------------------------------------------------

  handleParseUrl() {
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
      params = {},
      match;
    while ((match = regex.exec(window.location.href))) {
      params[match[1]] = match[2];
    }
    if (params.id) {
      this.validateToken(params.id);
    }
  }

  //-------------------------------------------------------

  // this method gets called on change of Otp field

  changeValueHandler(e) {
    let tstate = this.state
    tstate.entered_otp = e.target.value;
    this.setState({ tstate });

  }

  //-------------------------------------------------------

  // this method gets called on change of Pin field

  changePinHandler(e) {
    let tstate = this.state
    tstate.entered_pin = e.target.value;
    this.setState({ tstate });

  }

  //--------------------------------------------------------

  handleFeatureWillUpdate(e) {
    e.preventDefault();
    notification.open({
      message: "Opp! Something went wrong.",
      description: "This feature has been updated later!",
      duration: 500,
    });
  }

  //---------------------------------------------------------

  validateUser(data) {

    const login_url = `${BaseURL}api/confirm-otp-set-pin/`;

    const req_data = {
      mobile_number: this.props.auth.new_mobno.mobile_number,
      otp: this.state.entered_otp,
      pin: this.state.entered_pin
    }

    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(req_data),
    };

    fetch(login_url, requestOptions)
      // .then((response) => response.json())
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else if (res.status === 401) {
          console.log("SOMETHING WENT WRONG")
          // this.setState({ requestFailed: true })
        }
      })
      .then((result) => {
        this.setState({
          data: result,
          is_profile_filled: result.is_profile_filled,
        });
        this.props.form.validateFields((err, values) => {
          if (!err) {
            let status_code = result.status;

            if (status_code == "5" || status_code == 5) {

              this.props.saveUser({
                username: data.username,
                token: result.token,
              });


            }
          }
        });

      })
      .catch((error) =>
        this.setState({
          isLoaded: true,
          error: error,
        })
      );
  }

  //-------------------------------------------------------

  // this method gets called when user Clicks on submit button and redirects the user to the Login Page

  handleLoginSubmit = () => {
    let fData = this.props.form.getFieldsValue();
    this.validateUser(fData);

    Router.push("/account/login");

  };

  //---------------------------------------------------------


  render() {

    const { getFieldDecorator } = this.props.form;
    return (
      <div className="ps-my-account">
        <div className="container">
          {
            <Form className="ps-form--account">
              <ul className="ps-tab-list">
                <li className="active">
                  <Link href="/account/login">
                    <a>Otp</a>
                  </Link>
                </li>
                {/* <li>
                  <Link href="/account/register">
                    <a>Register</a>
                  </Link>
                </li> */}
              </ul>
              <div className="ps-tab active" id="sign-in">
                <div className="ps-form__content">
                  <h5>Otp Your Account</h5>
                  <div className="form-group">
                    <Form.Item>
                      {getFieldDecorator("username", {
                        rules: [
                          {
                            required: true,
                            message: "Please input your username!",
                          },
                        ],
                      })(
                        <input
                          className="form-control"
                          type="text"
                          placeholder="Mobile Number"
                          defaultValue={this.props.auth.new_mobno.mobile_number}
                        ></input>

                      )}
                    </Form.Item>
                  </div>
                  <div className="form-group form-forgot">
                    <Form.Item>
                      {getFieldDecorator("otp", {
                        rules: [
                          {
                            required: true,
                            message: "Please input your otp!",
                          },
                        ],
                      })(
                        <Input
                          className="form-control"
                          type="text"
                          placeholder="Enter Otp"
                          onChange={this.changeValueHandler.bind(this)}
                        />
                      )}
                    </Form.Item>
                  </div>
                  <div className="form-group form-forgot">
                    <Form.Item>
                      {getFieldDecorator("pin", {
                        rules: [
                          {
                            required: true,
                            message: "Please input your Set pin!",
                          },
                        ],
                      })(
                        <Input
                          className="form-control"
                          type="password"
                          minLength="4"
                          placeholder="Set Pin"
                          onChange={this.changePinHandler.bind(this)}

                        />
                      )}
                    </Form.Item>
                  </div>

                  <div className="form-group submit">
                    <button
                      type="submit"
                      onClick={this.handleLoginSubmit.bind(this)}
                      className="ps-btn ps-btn--fullwidth"
                    >
                      Submit
                    </button>
                  </div>
                </div>
                <br></br>
              </div>
            </Form>
          }
        </div>
      </div>
    );
  }
}

const WrapFormOtp = Form.create()(Otp);

// connects store to Component
// Redux Store --> Component
const mapStateToProps = (state) => {

  return {
    cart: state.cart,
    auth: state.auth,
    product: state.product,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    login: () => dispatch(login()),
    saveUser: (saveData) => dispatch(saveUser(saveData)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(WrapFormOtp);