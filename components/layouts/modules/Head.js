import React, { PureComponent } from "react";
import Head from "next/head";

class StyleSheets extends PureComponent {
	constructor(props) {
		super(props);

		this.state = {
			isLoaded: false,
			error: null,
			data: [],
		};
	}

	componentDidMount() {
		const req_data = {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				product_id: window.location.pathname,
			}),
		};

		fetch(`http://15.206.113.44:8004/ecomapi/product_meta/`, req_data)
			// .then((res) => res.json())
			.then((res) => {
				if (res.ok) {
					return res.json();
				} else if (res.status === 401) {
					console.log("SOMETHING WENT WRONG")
					// this.setState({ requestFailed: true })
				}
			})
			.then((result) => {
				// alert(JSON.stringify(result));
				if (result.data) {
					if (result.data.name && result.data.name != null) {

						this.setState({
							isLoaded: true,
							data: result.data.name,
						});
						console.log("----in ", this.state);

					}
				}

				console.log("----", this.state);
			});
	}

	render() {

		const title_data = {
			name: "title",
			content: this.state.data
		};

		const disc_data = {
			name: "description",
			content: `${this.state.data}`
		};

		return (
			<div>
				<Head>
					<title>Mind {this.state.data}</title>
					{/* <link rel="shortcut icon" href="/static/img/creditkart.png" /> */}
					<link rel="shortcut icon" href="/static/img/logo/logo.jpeg" />
					<link rel="icon" href="/static/img/logo/logo.jpeg" sizes="50x50" />
					<link rel="icon" href="/static/img/logo/logo.jpeg" sizes="192x192" />
					{/* <link
      rel="apple-touch-icon-precomposed"
      href="/static/img/creditkart.png"
    /> */}
					<link
						rel="apple-touch-icon-precomposed"
						href="/static/img/logo/logo.jpeg"
					/>
					<meta httpEquiv="X-UA-Compatible" content="IE=edge" />
					<meta name="viewport" content="width=device-width, initial-scale=1.0" />
					<meta name="format-detection" content="telephone=no" />
					<meta name="apple-mobile-web-app-capable" content="yes" />
					<meta name="author" content="nouthemes" />
					<meta name="keywords" content="mind" />
					<meta name="description" content="mind" />

					<link
						href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700&amp;amp;subset=latin-ext"
						rel="stylesheet"
					/>
					<link
						rel="preload"
						href="/static/fonts/Linearicons/Font/demo-files/demo.css"
						as="style"
						onLoad="this.rel='stylesheet'"
					/>

					<link
						rel="preload"
						href="/static/fonts/font-awesome/css/font-awesome.min.css"
						as="style"
						onLoad="this.rel='stylesheet'"
					/>

					<link
						rel="stylesheet"
						type="text/css"
						href="/static/css/bootstrap.min.css"
					/>
					<link rel="stylesheet" type="text/css" href="/static/css/slick.min.css" />
				</Head>

			</div>
		);
	}
}

export default StyleSheets;
