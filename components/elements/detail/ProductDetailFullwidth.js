import React, { PureComponent } from "react";
import Router from "next/router";
import { Tabs, Modal, Button, Form, Input, Collapse } from "antd";
import Rating from "../../elements/Rating";

import { connect } from "react-redux";
import ThumbnailDefault from "./modules/thumbnail/ThumbnailDefault";
import InformationDefault from "./modules/information/InformationDefault";
import DefaultDescription from "./modules/description/DefaultDescription";
import BreadCrumb from "../BreadCrumb";
import { getProductsById } from "../../../store/product/action";
import { BaseURL } from "../../../public/static/data/baseURL.json";

import StarRatingComponent from 'react-star-rating-component';


class ProductDetailFullwidth extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      colour: "colour",
      size: "size",

      modalOpen: false,
      visible: false,
      setVisible: false,
      confirmLoading: false,
      setConfirmLoading: false,
      review_resp: [],

      review_arr: [],
      value: "",
      rating: 0,
      isOpen: false

    };


  }

  onStarClick(nextValue, prevValue, name) {
    this.setState({ rating: nextValue });
  }

  ratingChanged = (newRating) => {
    alert(newRating);
    this.setState({ rating: newRating })

  };

  addReviews(e) {
    let tstate = this.state
    tstate.value = e.target.value
    this.setState({ tstate });
  }

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  hideModal = () => {
    this.setState({
      visible: false,
    });
  };

  handleOk = () => {
    setModalText('The modal will be closed after two seconds');
    setConfirmLoading(true);
    setTimeout(() => {
      setVisible(false);
      setConfirmLoading(false);
    }, 2000);
  }

  handleCancel = () => {
    setVisible(false);
  }

  saveReviews() {

    let Token = "Token" + " " + this.props.auth.auth_data.token;

    const req_data1 = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        authorization: Token
      },
      body: JSON.stringify({
        rating: this.state.rating,
        review: this.state.value,
        product_id: this.props.productId
      }),


    };

    fetch(
      `${BaseURL}ecomapi/add_product_rating/`,
      req_data1
    )
      // .then((res) => res.json())

      .then((res) => {
        if (res.ok) {
          return res.json();
        } else if (res.status === 401) {
          console.log("SOMETHING WENT WRONG")
          // this.setState({ requestFailed: true })
        }
      })
      .then((result) => {
        this.setState({
          review_resp: result,
          visible: false
        })
      });
  }


  componentDidMount() {
    const { productId } = this.props;
    const { size } = this.state;

    let Token = "Token" + " " + this.props.auth.auth_data.token;

    const data = {
      product_id: productId,
      colour: this.state.colour,
      size: size,
      unit_combination:"unit_combination"
    };
    // this.props.dispatch(getProductsById(productId));
    this.props.dispatch(getProductsById(data));

    const req_data1 = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        authorization: Token
      },
      body: JSON.stringify({
        product_id: this.props.productId
      }),


    };

    fetch(
      `${BaseURL}ecomapi/product_reviews/`,
      req_data1
    )
      // .then((res) => res.json())
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else if (res.status === 401) {
          console.log("SOMETHING WENT WRONG")
          // this.setState({ requestFailed: true })
        }
      })
      .then((result) => {
        this.setState({
          review_resp: result,
          visible: false,
          review_arr: result.review_list
        })
      });

      Router.events.on('routeChangeComplete', () => {
        window.scroll({
          top: 0,
          left: 0,
          behavior: 'smooth'
        });
      });
  }

  fetchReviews = () => {

    const { productId } = this.props;
    const { size } = this.state;

    let Token = "Token" + " " + this.props.auth.auth_data.token;


    const data = {
      product_id: productId,
      colour: this.state.colour,
      size: size,
      unit_combination:"unit_combination"
    };
    // this.props.dispatch(getProductsById(productId));
    this.props.dispatch(getProductsById(data));

    const req_data1 = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        authorization: Token
      },
      body: JSON.stringify({
        product_id: this.props.productId
      }),


    };

    fetch(
      `${BaseURL}ecomapi/product_reviews/`,
      req_data1
    )
      // .then((res) => res.json())
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else if (res.status === 401) {
          console.log("SOMETHING WENT WRONG")
          // this.setState({ requestFailed: true })
        }
      })
      .then((result) => {
        this.setState({
          review_resp: result,
          visible: false,
          review_arr: result.review_list
        })
      });

  }

  executeOnClick(isExpanded) {
    console.log(isExpanded);
  }

  getRenderedItems() {

    const MAX_ITEMS = 3;

    if (this.state.isOpen) {
      return this.state.review_arr;
    }
    return this.state.review_arr.slice(0, MAX_ITEMS);
  }

  toggle = () => {
    this.setState({ isOpen: !this.state.isOpen });
  }

  render() {


    const { productId } = this.props;
    const { singleProduct } = this.props.Product;
    const { rating } = this.state;


    if (singleProduct) {

      return (
        <div className="ps-product--detail ps-product--fullwidth">
          {singleProduct ? (
            <div>
              <BreadCrumb
                breacrumb={[
                  {
                    text: "Home",
                    url: "/",
                  },
                  {
                    text: singleProduct.categories[0].name,
                  },
                  {
                    text: singleProduct.sub_category,
                  },
                ]}
              />
              <div className="ps-product__header">
                <ThumbnailDefault product={singleProduct} />
                <InformationDefault
                  key={singleProduct.id}
                  product={singleProduct}
                />
              </div>
            </div>
          ) : (
              ""
            )}
          <DefaultDescription productId={productId} product={singleProduct} />
          <div>
            <h3 style={{ fontWeight: "bolder" }}>Reviews ({this.props.Product.singleProduct.review_length})</h3>
            <a style={{ color: "blue", fontWeight: "bolder" }} onClick={this.showModal}>Write a Review</a>
          </div>
          <Modal
            title="Write a Review"
            visible={this.state.visible}
            okText="Submit"
            onOk={
              this.saveReviews.bind(this)
            }
            onCancel={this.hideModal}
            afterClose={this.fetchReviews}
            destroyOnClose={true}
          >
            <p style={{ color: "blue", fontWeight: "bolder" }}>{singleProduct.title}</p>
            <Form>
              <StarRatingComponent
                className="ps-rating"
                name="rate1"
                starCount={5}
                value={rating}
                onStarClick={this.onStarClick.bind(this)}
                size={24}
              />

              <Form.Item
                name="title"
                label="Tell us your experience"
                rules={[{ required: true, message: 'Please input the title of collection!' }]}>
                <Input onChange={(e) => this.addReviews(e)}></Input>
              </Form.Item>
            </Form>
          </Modal>
          <br></br>
          <br></br>

          {this.props.Product.singleProduct.review_list ? (
            <div>

              {this.getRenderedItems().map((item)=>(
                 <div id="navcontainer">
                 <ul id="navlist">
                   <li className="icon-user" id="active"></li>
                   <li style={{ fontWeight: "bolder" }}>{item.user}</li>
                   <li> <Rating rating={item.rating}></Rating></li>
                   <br></br>
                   <li style={{ paddingLeft: "40px" }}>{item.review}</li>

                 </ul>
               </div>
              ))}

              <h4><a onClick={this.toggle}> {this.state.isOpen ? 'See Few Reviews' : 'See All Reviews'}</a></h4>

              
            </div>) : ""}


        </div>
      );
    }

    else {
      return (
        <div className="ps-product--detail ps-product--fullwidth">
          {singleProduct ? (
            <div>
              <BreadCrumb
                breacrumb={[
                  {
                    text: "Home",
                    url: "/",
                  },
                  {
                    text: singleProduct.categories[0].name,
                  },
                  {
                    text: singleProduct.sub_category,
                  },
                ]}
              />
              <div className="ps-product__header">
                <ThumbnailDefault product={singleProduct} />
                <InformationDefault
                  key={singleProduct.id}
                  product={singleProduct}
                />
              </div>
            </div>
          ) : (
              ""
            )}
          <DefaultDescription productId={productId} product={singleProduct} />
          <div>
            <h3 style={{ fontWeight: "bolder" }}>Reviews</h3>
            <a style={{ color: "blue", fontWeight: "bolder" }} onClick={this.showModal}>Write a Review</a>
          </div>
          <Modal
            title="Write a Review"
            visible={this.state.visible}
            okText="Submit"
            onOk={
              this.saveReviews.bind(this)
            }
            onCancel={this.hideModal}
          >
            <p style={{ color: "blue", fontWeight: "bolder" }}></p>
            <Form>
              <StarRatingComponent
                className="ps-rating"
                name="rate1"
                starCount={5}
                value={rating}
                onStarClick={this.onStarClick.bind(this)}
              />

              <Form.Item
                name="title"
                label="Tell us your experience"
                rules={[{ required: true, message: 'Please input the title of collection!' }]}>
                <Input
                  onChange={(e) => this.addReviews(e)}></Input>
              </Form.Item>
            </Form>
          </Modal>
          <br></br>
          <br></br>

          {this.state.review_resp.review_list ? (
            <div>
              {this.state.review_resp.review_list.map((item) => (
                <ul>
                  <li className="h3">
                    User Name: {item.user}
                  </li>
                  <li>
                    User Review :{item.review}
                  </li>
                  <li>
                    <Rating rating={item.rating}></Rating>
                  </li>
                </ul>
              ))}
            </div>
          ) : ""}


        </div>
      );
    }


  }
}
const mapStateToProps = (state) => {
  return {
    cart: state.cart,
    auth: state.auth,
    setting: state.setting,
    Product: state.product,
  };
};

export default connect(mapStateToProps)(ProductDetailFullwidth);
