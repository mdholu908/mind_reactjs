import React, { PureComponent } from "react";
import { connect } from "react-redux";
import Link from "next/link";
import Rating from "../../../Rating";
import { addItem } from "../../../../../store/cart/action";
import { buyNow } from "../../../../../store/cart/action";
import { addItemToCompare } from "../../../../../store/compare/action";
import { addItemToWishlist } from "../../../../../store/wishlist/action";
import { getProductsById } from "../../../../../store/product/action";
import { notification } from "antd";
import { BaseURL } from "../../../../../public/static/data/baseURL.json";
import Router from "next/router";

import { FacebookShareButton, TwitterShareButton, LinkedinShareButton, WhatsappShareButton } from 'react-share';
import { FacebookIcon, TwitterIcon, WhatsappIcon, LinkedinIcon } from 'react-share';

import {
	orderPlacedSuccessCart,
	orderPlacedSuccessByNow,
} from "../../../../../store/cart/action";


class InformationDefault extends PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			quantity: 1,
			error: null,
			isLoaded: false,
			colour: "Colour",
			size: "Size",
			productcolour: [],
			productsize: [],
			productdata: [],
			bgColor: "#fcb800",
			firstprdcolor: "",
			firstproductsize: "",
			category: true,
			new_product_price: "",
			combination: "",
			unit_combination:""
		};
	}

	handleAddItemToBuynow = (e) => {
		const new_product_price1 = this.state.quantity * this.props.product.price
		e.preventDefault();

		const { colour } = this.state;
		const { size } = this.state;
		const { productdata } = this.state;

		if (colour !== "Colour" && size !== "Size") {
			if (this.props.auth.isLoggedIn === false) {

				//------------------------------------------------------------------


				if (this.props.product.category == "Grocery") {
					if (new_product_price1 < 500) {

						notification["warning"]({
							message: "Info",
							description: "Please Select Minimum Order of 500",
							duration: 5,
						});


					}


					else {

						notification["warning"]({
							message: "Info",
							description: "You are not Logged In, Kindly Login  ",
							duration: 2,
						});

						const req_data1 = {
							method: "POST",
							headers: {
								"Content-Type": "application/json",
							},
							body: JSON.stringify({
								product_id: this.props.product.id,
								colour: colour,
								size: size,
							}),


						};
						fetch(
							`${BaseURL}ecomapi/product_details_info`,
							req_data1
						)
							// .then((res) => res.json())
							.then((res) => {
								if (res.ok) {
									return res.json();
								} else if (res.status === 401) {
									console.log("SOMETHING WENT WRONG")
									// this.setState({ requestFailed: true })
								}
							})

							.then((result) => {

								this.setState(
									{
										productdata: result.output,
									},
									() => {
										let tempProduct = this.state
											.productdata;
										tempProduct.quantity = this.state.quantity;
										Object.assign(tempProduct, { "color": colour, "size": size })

										this.props.dispatch(
											buyNow(this.state.productdata)
										);
										this.setState({
											colour: "Colour",
											size: "Size",
										});
									}
								);

								Router.push(`/account/login`);
							});
					}
				}
				//---------------------------------------------------------------

				else {

					notification["warning"]({
						message: "Info",
						description: "You are not Logged In, Kindly Login  ",
						duration: 2,
					});

					const req_data1 = {
						method: "POST",
						headers: {
							"Content-Type": "application/json",
						},
						body: JSON.stringify({
							product_id: this.props.product.id,
							colour: colour,
							size: size,
						}),


					};
					fetch(
						`${BaseURL}ecomapi/product_details_info`,
						req_data1
					)
						// .then((res) => res.json())
						.then((res) => {
							if (res.ok) {
								return res.json();
							} else if (res.status === 401) {
								console.log("SOMETHING WENT WRONG")
								// this.setState({ requestFailed: true })
							}
						})

						.then((result) => {

							this.setState(
								{
									productdata: result.output,
								},
								() => {
									let tempProduct = this.state
										.productdata;
									tempProduct.quantity = this.state.quantity;
									Object.assign(tempProduct, { "color": colour, "size": size })

									this.props.dispatch(
										buyNow(this.state.productdata)
									);
									this.setState({
										colour: "Colour",
										size: "Size",
									});
								}
							);

							Router.push(`/account/login`);
						});
				}

				//---------------------------------------------------------------


			}
			else {
				if (this.props.auth.auth_data !== null) {

					if (this.props.product.category == "Grocery") {
						if (new_product_price1 < 500) {

							notification["warning"]({
								message: "Info",
								description: "Please Select Minimum Order Of 500  ",
								duration: 5,
							});


						}


						else {

							if (
								this.state.quantity <= this.props.product.product_stock
							) {
								const req_data1 = {
									method: "POST",
									headers: {
										"Content-Type": "application/json",
									},
									body: JSON.stringify({
										product_id: this.props.product.id,
										colour: colour,
										size: size,
									}),
								};
								fetch(
									`${BaseURL}ecomapi/product_details_info`,
									req_data1
								)
									// .then((res) => res.json())
									.then((res) => {
										if (res.ok) {
											return res.json();
										} else if (res.status === 401) {
											console.log("SOMETHING WENT WRONG")
											// this.setState({ requestFailed: true })
										}
									})
									.then((result) => {

										this.setState(
											{
												productdata: result.output,
											},
											() => {
												let tempProduct = this.state
													.productdata;
												tempProduct.quantity = this.state.quantity;
												// Object.assign(tempProduct, { "color": colour, "size": size })
												this.props.dispatch(
													buyNow(this.state.productdata)
												);
												this.setState({
													// colour: "Colour",
													// size: "Size",
												});
											}
										);

										Router.push(`/account/checkout`);
									});
							}

							else {
								notification["warning"]({
									message: "Warning",
									description: "Selected quantity is too large ! ",
									duration: 2,
								});
							}
						}
					}

					//------------------------------------------------------------------------------

					else {

						if (
							this.state.quantity <= this.props.product.product_stock
						) {
							const req_data1 = {
								method: "POST",
								headers: {
									"Content-Type": "application/json",
								},
								body: JSON.stringify({
									product_id: this.props.product.id,
									colour: colour,
									size: size,
								}),
							};
							fetch(
								`${BaseURL}ecomapi/product_details_info`,
								req_data1
							)
								// .then((res) => res.json())
								.then((res) => {
									if (res.ok) {
										return res.json();
									} else if (res.status === 401) {
										console.log("SOMETHING WENT WRONG")
										// this.setState({ requestFailed: true })
									}
								})
								.then((result) => {

									this.setState(
										{
											productdata: result.output,
										},
										() => {
											let tempProduct = this.state
												.productdata;
											tempProduct.quantity = this.state.quantity;
											Object.assign(tempProduct, { "color": colour, "size": size })
											this.props.dispatch(
												buyNow(this.state.productdata)
											);
											this.setState({
												colour: "Colour",
												size: "Size",
											});
										}
									);

									Router.push(`/account/checkout`);
								});
						}

						else {
							notification["warning"]({
								message: "Warning",
								description: "Selected quantity is too large ! ",
								duration: 2,
							});
						}
					}
					//------------------------------------------------------------------------------
				} else {
					notification["warning"]({
						message: "Info",
						description: "You are not Logged In, Kindly Login ",
						duration: 2,
					});
					Router.push(`/account/login`);
				}

			}
		} else {
			notification["info"]({
				message: "Info",
				description: "You must select Colour and Size!",
				duration: 2,
			});
		}
	};

	handleAddItemToCart = (e) => {
		this.props.cart.buynow.length = 0;

		this.props.dispatch(orderPlacedSuccessByNow());

		e.preventDefault();
		const { colour } = this.state;
		const { size } = this.state;
		const { productdata } = this.state;

		if (colour !== "Colour" && size !== "Size") {
			if (this.props.auth.isLoggedIn === false) {
				notification["warning"]({
					message: "Info",
					description: "You are not Logged In, Kindly Login  ",
					duration: 2,
				});


				const req_data1 = {
					method: "POST",
					headers: {
						"Content-Type": "application/json",
					},
					body: JSON.stringify({
						product_id: this.props.product.id,
						colour: colour,
						size: size,
					}),


				};

				fetch(
					`${BaseURL}ecomapi/product_details_info`,
					req_data1
				)
					// .then((res) => res.json())
					.then((res) => {
						if (res.ok) {
							return res.json();
						} else if (res.status === 401) {
							console.log("SOMETHING WENT WRONG")
							// this.setState({ requestFailed: true })
						}
					})

					.then((result) => {


						this.setState(
							{
								productdata: result.output,
							},
							() => {
								let tempProduct = this.state
									.productdata;
								tempProduct.quantity = this.state.quantity;
								Object.assign(tempProduct, { "color": colour, "size": size })

								this.props.dispatch(
									addItem(this.state.productdata)
								);
								this.setState({
									colour: "Colour",
									size: "Size",
								});
							}
						);

						Router.push(`/account/login`);
					});

			} else {
				if (this.state.quantity <= this.props.product.product_stock) {
					const req_data1 = {
						method: "POST",
						headers: {
							"Content-Type": "application/json",
						},
						body: JSON.stringify({
							product_id: this.props.product.id,
							colour: colour,
							size: size,
						}),
					};
					fetch(`${BaseURL}ecomapi/product_details_info`, req_data1)
						// .then((res) => res.json())
						.then((res) => {
							if (res.ok) {
								return res.json();
							} else if (res.status === 401) {
								console.log("SOMETHING WENT WRONG")
								// this.setState({ requestFailed: true })
							}
						})
						.then((result) => {

							this.setState(
								{
									productdata: result.output,
								},
								() => {
									let tempProduct = this.state.productdata;
									tempProduct.quantity = this.state.quantity;

									this.props.dispatch(
										addItem(this.state.productdata)
									);
								}
							);
							this.setState({
								colour: "Colour",
								size: "Size",
							});
						});
				} else {
					notification["warning"]({
						message: "Warning",
						description: "Selected quantity is too large ! ",
						duration: 2,
					});
				}

			}
		} else {
			notification["info"]({
				message: "Info",
				description: "You must select Colour and Size!",
				duration: 2,
			});
		}
	};

	handleAddItemToCompare = (e) => {
		e.preventDefault();
		const { product } = this.props;
		this.props.dispatch(addItemToCompare(product));
	};

	handleAddItemToWishlist = (e) => {
		e.preventDefault();
		const { product } = this.props;
		this.props.dispatch(addItemToWishlist(product));
	};

	handleIncreaseItemQty = (e) => {
		e.preventDefault();
		this.setState({
			quantity: this.state.quantity + 1,
			new_product_price: (this.state.quantity + 1) * this.props.product.price
		});
	};

	handleDecreaseItemQty = (e) => {
		e.preventDefault();
		if (this.state.quantity > 1) {
			this.setState({
				quantity: this.state.quantity - 1,
				new_product_price: (this.state.quantity - 1) * this.props.product.price

			});
		}
	};

	getColor() {
		const req_data2 = {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				product_id: this.props.product.id,
			}),
		};
		fetch(`${BaseURL}ecomapi/colours_for_product`, req_data2)
			// .then((res) => res.json())
			.then((res) => {
				if (res.ok) {
					return res.json();
				} else if (res.status === 401) {
					console.log("SOMETHING WENT WRONG")
					// this.setState({ requestFailed: true })
				}
			})
			.then((result) => {
				if (result.data && result.data.colour) {
					const default_color = result.data.colour[0].colour;

					if (default_color) {
						this.setState({
							isLoaded: true,
							// firstprdcolor: result.data.colour[0].colour,
							colour: this.props.Product.singleProduct.specifications.colour,
							productcolour: result.data.colour

						});

						this.getSize();
					} else {
						notification.open({
							message: "Opp! Something went wrong.",
							// description: "This feature has been updated later!",
							duration: 5,
						});
					}
				}
			})
			.catch((error) =>
				this.setState({
					isLoaded: true,
					error: error,
				})
			);
	}
	handleUpdateProductDetails(size) {
		this.setState({ size: size }, () => {
			const updatedata = {
				product_id: this.props.product.id,
				colour: this.state.colour,
				size: this.state.size,
				unit_combination: "unit_combination"
			};

			this.props.dispatch(getProductsById(updatedata));
		});
	}

	getSize() {
		const req_data1 = {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				product_id: this.props.product.id,
				colour: this.state.colour,
			}),
		};
		fetch(`${BaseURL}ecomapi/size_for_product`, req_data1)
			// .then((res) => res.json())
			.then((res) => {
				if (res.ok) {
					return res.json();
				} else if (res.status === 401) {
					console.log("SOMETHING WENT WRONG")
					// this.setState({ requestFailed: true })
				}
			})
			.then((result) => {
				console.log("sizes "+JSON.stringify(result));
				if (result.data && result.data.size) {
					this.setState({
						isLoaded: true,
						// firstproductsize: result.data.colour[0].size,
						size: this.props.Product.singleProduct.specifications.size,
						productsize: result.data.size
					});
					this.handleUpdateProductDetails(result.data.size[0].size);
				} else {

					notification.open({
						message: "Opp! Something went wrong.",
						duration: 5,
					});
				}
			})
			.catch((error) => {
				this.setState({
					isLoaded: true,
					error: error,
				});
			});
	}

	getDefaultValue = () =>{
		let value= this.props.product.d_unit_combinations[0]
		this.setState({
			unit_combination:value,
			colour:this.props.product.specifications.colour,
			size:this.props.product.specifications.size
		})

	}

	componentDidMount() {

		if (this.props.product.category === "Grocery") {

			this.getDefaultValue()

		}
		else {
			this.getColor();
		}


	}
	handleColourchange = (e, color) => {
		e.target.blur();
		e.target.parentNode.blur();
		e.preventDefault();
		this.setState({ colour: color, bgColor: color }, () => {
			// if (this.state.colour != "Colour") {
			const req_data2 = {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify({
					product_id: this.props.product.id,
					colour: this.state.colour,
				}),
			};
			fetch(`${BaseURL}ecomapi/size_for_product`, req_data2)
				// .then((res) => res.json())
				.then((res) => {
					if (res.ok) {
						return res.json();
					} else if (res.status === 401) {
						console.log("SOMETHING WENT WRONG")
						// this.setState({ requestFailed: true })
					}
				})
				.then((result) => {
					// console.log("In handleColourchange "+JSON.stringify(result))
					this.setState({
						isLoaded: true,
						productsize: result.data.size,
					});
				})
				.catch((error) =>
					this.setState({
						isLoaded: true,
						error: error,
					})
				);
			// }
		});
	};

	//--------------------------------------------------

	handleCombinationChange = (e, combination) => {


		this.setState({ unit_combination: combination }, () => {
			const updatedata = {
				product_id: this.props.product.id,
				colour: this.state.colour,
				size: this.state.size,
				unit_combination: combination.split(" ").join("%")
			};

			this.props.dispatch(getProductsById(updatedata));
		});



	}

	//--------------------------------------------------

	handleSizechange = (e, size) => {
		e.preventDefault();
		this.handleUpdateProductDetails(size);
	};

	componentWillMount() {
		const req_data = {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				product_id: this.props.product.id,
			}),
		};
		fetch(`${BaseURL}ecomapi/colours_for_product`, req_data)
			// .then((res) => res.json())
			.then((res) => {
				if (res.ok) {
					return res.json();
				} else if (res.status === 401) {
					console.log("SOMETHING WENT WRONG")
					// this.setState({ requestFailed: true })
				}
			})
			.then((result) => {
				this.setState({
					isLoaded: true,
					// productcolour: result.data.colour					
				});
			})
			.catch((error) =>
				this.setState({
					isLoaded: true,
					error: error,
				})
			);
	}

	render() {
		const url = `https://thecreditkart.com/product/${encodeURIComponent(this.props.product.id)}`;
		const { productcolour } = this.state;
		const { productsize } = this.state;
		const { colour } = this.state;
		const { size } = this.state;
		// const {combination} = this.state
		const { currency } = this.props.setting;
		const { product } = this.props;



		return (
			<div className="ps-product__info" style={{paddingBottom:"0px"}}>
				<h1>{product.title}</h1>
				<div className="ps-product__meta">

					<div className="ps-product__rating">
						<Rating rating={product.ratingCount} /> &nbsp;&nbsp;
						<p style={{ fontSize: "20px" }}>({product.ratingCount})</p>

					</div>
				</div>
				{product.mrp ? (
					<div>
						{product.mrp < product.price || product.discount === 0 ? (
							<p style={{ display: "inline", fontWeight: "700", fontSize: "20px" }}>Price {currency ? currency.symbol : "$"} {product.price}</p>

						) : (
								<h4 className="ps-product__price">
									{currency ? currency.symbol : "$"}
									{product.price}

									&nbsp;&nbsp;
							<strike style={{ color: "#757575" }}>
										{currency ? currency.symbol : "$"}
										{product.mrp}</strike>
									&nbsp;&nbsp;<p style={{ display: "inline", color: "#24a3b5", fontWeight: "500", fontSize: "18px" }}>{Math.round(product.discount)}% Off</p>

								</h4>
							)}</div>
				) : (<div>
					<p style={{ display: "inline", fontWeight: "900", fontSize: "20px" }}>Price {currency ? currency.symbol : "$"} {product.price}</p>
				</div>)}
				<div className="ps-product__desc">
					<p>
						Sold By:

								<strong style={{ color: "#0099CC" }}> {product.vendor}</strong>

					</p>
				</div>
				<div className="ps-product__desc">
					<p>
						{product.product_stock <= 0 ? (
							<div style={{ color: "red", fontWeight: "bold" }}>
								Out of Stock
							</div>
						) : (
								<div>
									In Stock:
											<strong style={{ color: "#0099CC" }}>
										{" "}
										{product.product_stock}{" "}
									</strong>
								</div>
							)}
					</p>
				</div>

				{product.category == "Grocery" ? (

					<div className="ps-dropdown">
						<a className="ps-btn">{this.state.unit_combination} </a>
						<ul className="ps-dropdown-menu">
							{product.d_unit_combinations.map((combination) => (
								<li style={{ display: "block" }}
									className="ps-btn">
									<a onClick={(e) =>
										this.handleCombinationChange(
											e,
											combination
										)
									}>
										{combination}
									</a>
								</li>

							))}


						</ul>



					</div>
				) :
					(
						<div>


							<div className="ps-dropdown">
								<a className="ps-btn">{colour.charAt(0).toUpperCase() + colour.slice(1).split("_").join(" ")}</a>
								<ul className="ps-dropdown-menu">
									{productcolour.map((product) => (
										<li
											key={product.colour}
											style={{ display: "block" }}
											className="ps-btn"
										>
											<a
												onClick={(e) =>
													this.handleColourchange(
														e,
														product.colour
													)
												}
											>
												{product.colour.charAt(0).toUpperCase() + product.colour.slice(1).split("_").join(" ")}
											</a>
										</li>
									))}
								</ul>
							</div>
							&nbsp;&nbsp;

				            <div className="ps-dropdown">
								<a className="ps-btn">{this.props.Product.singleProduct.specifications.size.charAt(0).toUpperCase() + this.props.Product.singleProduct.specifications.size.slice(1).split("_").join(" ")}</a>
								{colour !== "Colour" ? (
									<ul className="ps-dropdown-menu">
										{productsize.map((product) => (
											<li
												key={product.size}
												style={{ display: "block" }}
												className="ps-btn"
											>
												<a
													onClick={(e) =>
														this.handleSizechange(
															e,
															product.size
														)
													}
												>
													{product.size.charAt(0).toUpperCase() + product.size.slice(1).split("_").join(" ")}
												</a>
											</li>
										))}
									</ul>
								) : null}
							</div>
						</div>
					)
				}

				&nbsp;&nbsp;&nbsp;&nbsp;
				<div className="ps-product__shopping">
					<figure>
						<figcaption>Quantity</figcaption>
						<div className="form-group--number">
							<button
								className="up"
								onClick={this.handleIncreaseItemQty.bind(this)}
							>
								<i className="fa fa-plus"></i>
							</button>
							<button
								className="down"
								onClick={this.handleDecreaseItemQty.bind(this)}
							>
								<i className="fa fa-minus"></i>
							</button>
							<input
								className="form-control"
								type="text"
								placeholder={this.state.quantity}
								disabled
							/>
						</div>
					</figure>
					{product.product_stock <= 0 ? (
						<div className="btn_space" >
							<a
								className="btn_space ps-btn ps-btn"
								href="#"
								onClick={this.handleAddItemToCart.bind(this)}
								disabled
							>
								Add to cart
							</a>
							&nbsp;&nbsp;&nbsp;
							<a
								className="btn_space ps-btn"
								href="#"
								onClick={this.handleAddItemToBuynow.bind(this)}
								disabled
							>
								Buy Now
							</a>
						</div>
					) : (
							<div className="btn_space">
								<a
									className="btn_space ps-btn ps-btn"
									href="#"
									onClick={this.handleAddItemToCart.bind(this)}
								>
									Add to cart
							</a>
								&nbsp;&nbsp;&nbsp;
								<a
									className="btn_space ps-btn"
									href="#"
									onClick={this.handleAddItemToBuynow.bind(this)}
								>
									Buy Now
							</a>
							</div>
						)}
				</div>
				<div className="ps-product__sharing">

					<h4>Share</h4>
					<FacebookShareButton url={url} separator="" title={this.props.product.title}>
						<FacebookIcon size={42} round={true}></FacebookIcon>
					</FacebookShareButton>

					<TwitterShareButton url={url} separator="" title={this.props.product.title}>
						<TwitterIcon size={42} round={true}></TwitterIcon>
					</TwitterShareButton>

					<LinkedinShareButton url={url} separator="" title={this.props.product.title}>
						<LinkedinIcon size={42} round={true}></LinkedinIcon>
					</LinkedinShareButton>

					<WhatsappShareButton url={url} separator="" title={this.props.product.title} >
						<WhatsappIcon size={42} round={true}></WhatsappIcon>
					</WhatsappShareButton>

				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		cart: state.cart,
		auth: state.auth,
		setting: state.setting,
		Product: state.product,
	};
};

export default connect(mapStateToProps)(InformationDefault);
