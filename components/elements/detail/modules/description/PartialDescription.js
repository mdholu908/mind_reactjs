import React, { PureComponent } from "react";
import { BaseURL } from "../../../../../public/static/data/baseURL.json";
class PartialDescriptionData extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      productss: [],
    };
  }

  componentDidMount() {
    const { productId } = this.props;
    const req_data = {
      method: "POST",
      headers: {
        // 'Accept':'application/json',
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        product_id: { productId }.productId,
      }),
    };
    fetch(`${BaseURL}ecomapi/product_details_api`, req_data)
      .then((res) => res.json())
      .then((result) => {
        this.setState({
          isLoaded: true,
          productss: result.data.description,
        });
      })
      .catch((error) =>
        this.setState({
          isLoaded: true,
          error: error,
        })
      );
  }

  render() {
    const { productss } = this.state;

    return (
      <div className="ps-document">
        {productss.map((product) => (
          <div key={product.head}>
            <h5>{product.head}</h5>
            <p>{product.info}</p>
          </div>
        ))}
      </div>
    );
  }
}
export default PartialDescriptionData;
