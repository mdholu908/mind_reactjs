import React, { PureComponent } from "react";
import { Tabs } from "antd";
const { TabPane } = Tabs;
import PartialDescriptionData from "./PartialDescription";
import PartialSpecification from "./PartialSpecification";

class DefaultDescription extends PureComponent {
  constructor(props) {
    super(props);
  }
  render() {
    const { productId } = this.props;
    return (
      <div>
        <div>
          <Tabs defaultActiveKey="1">
            <TabPane tab="Description" key="1">
              <PartialDescriptionData productId={productId} />
            </TabPane>
            <TabPane tab="Specification" key="2">
              <PartialSpecification key={productId.id} productId={productId} />
            </TabPane>
          </Tabs>
        </div>
      </div>
    );
  }
}

export default DefaultDescription;
