import React, { PureComponent } from "react";
import { BaseURL } from "../../../../../public/static/data/baseURL.json";
class PartialSpecification extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      productss: [],
    };
  }

  componentDidMount() {
    const { productId } = this.props;
    const req_data = {
      method: "POST",
      headers: {
        // 'Accept':'application/json',
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        product_id: { productId }.productId,
      }),
    };
    fetch(`${BaseURL}ecomapi/product_details_api`, req_data)
      .then((res) => res.json())
      .then((result) => {
        this.setState({
          isLoaded: true,
          productss: result.data.specification,
        });
      })
      .catch((error) =>
        this.setState({
          isLoaded: true,
          error: error,
        })
      );
  }

  render() {
    const { productss } = this.state;

    return (
      <div className="table-responsive">
        <table className="table table-bordered ps-table ps-table--specification">
          <tbody>
            {/* <tr>
                    <td>Color</td>
                    <td>Black, Gray</td>
                </tr> */}
            {productss.map((product) => (
              <tr>
                <td>{product.head}</td>
                <td>{product.info}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}
export default PartialSpecification;

// const PartialSpecification = () => (
//     <div className="table-responsive">
//         <table className="table table-bordered ps-table ps-table--specification">
//             <tbody>
//                 <tr>
//                     <td>Color</td>
//                     <td>Black, Gray</td>
//                 </tr>
//                 <tr>
//                     <td>Style</td>
//                     <td>Ear Hook</td>
//                 </tr>
//                 <tr>
//                     <td>Wireless</td>
//                     <td>Yes</td>
//                 </tr>
//                 <tr>
//                     <td>Dimensions</td>
//                     <td>5.5 x 5.5 x 9.5 inches</td>
//                 </tr>
//                 <tr>
//                     <td>Weight</td>
//                     <td>6.61 pounds</td>
//                 </tr>
//                 <tr>
//                     <td>Battery Life</td>
//                     <td>20 hours</td>
//                 </tr>
//                 <tr>
//                     <td>Bluetooth</td>
//                     <td>Yes</td>
//                 </tr>
//             </tbody>
//         </table>
//     </div>
// );

// export default PartialSpecification;
