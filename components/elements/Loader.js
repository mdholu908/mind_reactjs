import React from "react";
import { Spin, Icon } from "antd";

class Loader extends React.PureComponent {
	render() {
		// const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

		const antIcon = (
			<Icon
				type="loading"
				style={{
					fontSize: 50,
					color: "#000",
					border: "1px solid #000",
					// position: 'fixed',
					top: 0,
					left: 0,
					width: "100%",
					// height: 100%,
					// z-index: 999999,
					alignItems: "center",
				}}
				spin
			/>
		);

		return (
			// Loader code
			// <div className="loaderParent">
			//   <div className="loader">
			//     <span></span>
			//   </div>
			// </div>

			// loader end

			// new loader
			// <Spin />
			<div></div>
			// <Spin indicator={antIcon} />
			//

			// <div className="icon-carttest" aria-hidden="true">
			//   <p>
			//     {" "}
			//     <img
			//       src="/static/img/creditkart1.png"
			//       width={300}
			//       height={300}
			//       alt="CreditKart"
			//     ></img>
			//   </p>

			// </div>
		);
	}
}

export default Loader;
