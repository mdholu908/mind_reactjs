import React from "react";
//const Rating = () => (
class Rating extends React.PureComponent {
  constructor(props) {
    super(props);
  }
  render() {
    var stars = [];
    var emptystar = [];
    for (var i = 0; i < this.props.rating; i++) {
      stars.push(
        <i key={i + "a"} className="fa fa-star">
          {" "}
        </i>
      );
    }
    for (var i = 5; i > this.props.rating; i--) {
      emptystar.push(
        <i key={i + "b"} className="fa fa-star-o">
          {" "}
        </i>
      );
    }
    return (
      <div style={{display:"inline"}}>
        <span className="ps-rating">
          {stars}
          {emptystar}
        </span>
      </div>
      /* <span className="ps-rating">
        <i className="fa fa-star"></i>
        <i className="fa fa-star"></i>
        <i className="fa fa-star"></i>
        <i className="fa fa-star"></i>
        <i className="fa fa-star-o"></i>
    </span>*/
    );
  }
}
export default Rating;
