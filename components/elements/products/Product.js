import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { addItem } from "../../../store/cart/action";
import { addItemToCompare } from "../../../store/compare/action";
import { addItemToWishlist } from "../../../store/wishlist/action";
import Link from "next/link";
import { Modal } from "antd";
import ProductDetailQuickView from "../detail/ProductDetailQuickView";
import Rating from "../Rating";
import "lazysizes";

class Product extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isQuickView: false,
    };
  }

  handleAddItemToCart = (e) => {
    e.preventDefault();
    const { product } = this.props;
    this.props.dispatch(addItem(product));
  };

  handleAddItemToCompare = (e) => {
    e.preventDefault();
    const { product } = this.props;
    this.props.dispatch(addItemToCompare(product));
  };

  handleAddItemToWishlist = (e) => {
    e.preventDefault();
    const { product } = this.props;
    this.props.dispatch(addItemToWishlist(product));
  };

  handleShowQuickView = (e) => {
    e.preventDefault();
    this.setState({ isQuickView: true });
  };

  handleHideQuickView = (e) => {
    e.preventDefault();
    this.setState({ isQuickView: false });
  };

  render() {
    const { product, currency } = this.props;
    // console.log("Product detailssss "+JSON.stringify(product))
    let productBadge = null;
    if (product.badge && product.badge !== null) {
      product.badge.map((badge) => {
        if (badge.type === "sale") {
          return (productBadge = (
            <div className="ps-product__badge">{badge.value}</div>
          ));
        } else if (badge.type === "outStock") {
          return (productBadge = (
            <div className="ps-product__badge out-stock">{badge.value}</div>
          ));
        } else {
          return (productBadge = (
            <div className="ps-product__badge hot">{badge.value}</div>
          ));
        }
      });
    }
    return (
      <div className="ps-product">
        <div className="ps-product__thumbnail">
          <Link href={`/product/${encodeURIComponent(product.id)}`}>
            <a>
              <img
                data-src={product.thumbnail}
                width={200}
                height={250}
                alt="CreditKart"
                class="lazyload"
              />
            </a>
          </Link>
          {product.badge ? productBadge : ""}
          <ul>
            {/* <li>
                            <a
                                href="#"
                                data-toggle="tooltip"
                                 data-placement="top"
                                title="Read More"
                                onClick={this.handleAddItemToCart.bind(this)}>
                                <i className="icon-bag2"></i>
                            </a>
                        </li>
                        <li>
                            <a
                                href="#"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Quick View"
                                onClick={this.handleShowQuickView.bind(this)}>
                                <i className="icon-eye"></i>
                            </a>
                        </li> */}
            {/* <li>
                            <a
                                href="#"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Add to wishlist"
                                onClick={this.handleAddItemToWishlist.bind(
                                    this
                                )}>
                                <i className="icon-heart"></i>
                            </a>
                        </li>
                        <li>
                            <a
                                href="#"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Compare"
                                onClick={this.handleAddItemToCompare.bind(
                                    this
                                )}>
                                <i className="icon-chart-bars"></i>
                            </a>
                        </li> */}
          </ul>
        </div>
        <div className="ps-product__container">
          {/* <Link href="/shop">
                        <a className="ps-product__vendor"></a>
                    </Link> */}
          <div className="ps-product__content pl-3">
            <Link href={`/product/${encodeURIComponent(product.id)}`}>
              <a className="ps-product__title">{product.title}</a>
            </Link>
            {/* <div className="ps-product__rating">
              <Rating rating={product.ratingCount} />
              Rating <span>{product.ratingCount}</span>
            </div> */}
            {product.mrp < product.price || product.discount ===0 ? (
              <p style={{ display: "inline", fontWeight: "700", fontSize: "14px" }}>Price {currency ? currency.symbol : "$"} {product.price}</p>
            ) : (
                <p className="ps-product__price">
                  <strike style={{ color: "#757575" }}>
                    MRP {currency ? currency.symbol : "$"}
                    {product.mrp}</strike>
                  &nbsp;&nbsp; <p style={{ display: "inline", fontWeight: "700", fontSize: "14px" }}>Price {currency ? currency.symbol : "$"} {product.price}</p>

                  <br />
                  <p style={{ color: '#24a3b5', fontSize: "14px" }}>{Math.round(product.discount)}% Off</p>

                </p>
              )}
          </div>
          {/* <div className="ps-product__content hover">
            <Link href="/product/[pid]" as={`/product/${product.id}`}>
              <a className="ps-product__title">{product.title}</a>
            </Link>
            {product.sale === true ? (
              <p className="ps-product__price sale">
                {currency ? currency.symbol : "$"}
                {product.price}{" "}
                <del className="ml-2">
                  {currency ? currency.symbol : "$"}
                  {product.salePrice}
                </del>
              </p>
            ) : (
              <p className="ps-product__price">
                {currency ? currency.symbol : "$"}
                {product.price}
              </p>
            )}
          </div> */}
        </div>
        <Modal
          title={product.title}
          centered
          footer={null}
          width={1024}
          onCancel={this.handleHideQuickView}
          visible={this.state.isQuickView}
        >
          <ProductDetailQuickView product={product} />
        </Modal>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return state.setting;
};
export default connect(mapStateToProps)(Product);
