import React, { PureComponent } from "react";
import Link from "next/link";
import { notification } from "antd";
// import Menu from "../../elements/menu/Menu";
import { connect } from "react-redux";
import menuData from "../../../public/static/data/menu";
import { Menu } from "antd";
import { BaseURL } from "../../../public/static/data/baseURL.json";


const { SubMenu } = Menu;



class NavigationDefault extends PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			error: null,
			isLoaded: false,
			items: [],
			openKeys: [],
		};
	}
	rootSubmenuKeys = ["sub1", "sub2", "sub4"];

	onOpenChange = (openKeys) => {
		const latestOpenKey = openKeys.find(
			(key) => this.state.openKeys.indexOf(key) === -1
		);
		if (this.rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
			this.setState({ openKeys });
		} else {
			this.setState({
				openKeys: latestOpenKey ? [latestOpenKey] : [],
			});
		}
	};

	handleFeatureWillUpdate(e) {
		e.preventDefault();
		notification.open({
			message: "Opp! Something went wrong.",
			description: "This feature has been updated later!",
			duration: 500,
		});
	}

	componentDidMount() {

		const req_data = {
			method: "GET",
			headers: {
				"Content-Type": "application/json",
			},
			// body: JSON.stringify({
			// 	text_view: "Home",
			// }),
		};

		fetch(`${BaseURL}ecomapi/return_category/`, req_data)
			.then((res) => res.json())
			.then((result) => {
				this.setState({
					isLoaded: true,
					items: result.category,
				});
			})
			.catch((error) =>
				this.setState({
					isLoaded: true,
					error: error,
				})
			);
	}

	render() {
		const { items } = this.state;

		return (
			<nav className="navigation" style={{ backgroundColor: '#000' }}>
				<div className="ps-container">
					<div className="navigation__left">
						<div className="menu--product-categories">
							
						</div>
					</div>

					<div className="navigation__right">
						{this.props.auth.isLoggedIn ? (
							<Menu
								data={menuData.menuPrimary.menu_login}
								className="menu"
							/>
						) : (
								<Menu
									data={menuData.menuPrimary.menu_1}
									className="menu"
								/>
							)}
						<ul className="navigation__extra">
						</ul>
					</div>
				</div>
			</nav>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		cart: state.cart,
		auth: state.auth,
	};
};
export default connect(mapStateToProps)(NavigationDefault);
