import React, { Component } from 'react';
import SearchHeader from '../headers/modules/SearchHeader';

class PanelSearch extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="ps-panel__wrapper">
                <div className="ps-panel__header">
                  < SearchHeader />  
                </div>
                <div className="navigation__content"></div>
            </div>
        );
    }
}

export default PanelSearch;
