import React, { Component } from "react";
import { connect } from "react-redux";

import { Menu } from "antd";
import { category } from "../../../public/static/data/category";
import Link from "next/link";

import { BaseURL } from "../../../public/static/data/baseURL.json";

const { SubMenu } = Menu;

class PanelMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      requestFailed:false,
      items: [],
      openKeys: [],
    };
  }

  rootSubmenuKeys = ["sub1", "sub2", "sub4"];

  state = {
    openKeys: [],
  };

  onOpenChange = (openKeys) => {
    const latestOpenKey = openKeys.find(
      (key) => this.state.openKeys.indexOf(key) === -1
    );
    if (this.rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      this.setState({ openKeys });
    } else {
      this.setState({
        openKeys: latestOpenKey ? [latestOpenKey] : [],
      });
    }
  };


  componentDidMount() {

    const req_data = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",

      },
      // body: JSON.stringify({
      // 	text_view: "Home",
      // }),
    };

    fetch(`${BaseURL}ecomapi/return_category/`, req_data)
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else if (res.status === 408) {
          console.log("SOMETHING WENT WRONG")
          this.setState({ requestFailed: true })
        }
      })
      .then((result) => {
        this.setState({
          isLoaded: true,
          items: result.category,
        });
      })
      .catch((error) =>
        this.setState({
          isLoaded: true,
          error: error,
        })
      );

  }

  render() {

    const { items } = this.state;

    return (
      <div className="ps-panel__wrapper">
        <div className="ps-panel__header">
          <h3>Categories</h3>
        </div>
        <div className="ps-panel__content">
          <Menu
            style={{ marginBottom: "10%" }}
            mode="inline"
            openKeys={this.state.openKeys}
            onOpenChange={this.onOpenChange}
          >
            {items.map((item) => {
              if (item.subMenu) {
                return (
                  <SubMenu
                    key={item.text}
                    title={
                      <Link href={item.url}>
                        <a>{item.text}</a>
                      </Link>
                    }
                  >
                    {item.subMenu.map((subItem) => (
                      <Menu.Item key={subItem.text}>
                        <Link href={subItem.url}>
                          <a>{subItem.text}</a>
                        </Link>
                      </Menu.Item>
                    ))}
                  </SubMenu>
                );
              } else if (item.megaContent) {
                return (
                  <SubMenu
                    key={item.text}
                    title={
                      // <Link href={item.url}>
                      <a style={{ color: "#31b9d8" }}>{item.text}</a>
                      // </Link>
                    }
                  >
                    {item.megaContent.map((megaItem) => (
                      <SubMenu
                        key={megaItem.heading}
                        title={<span>{megaItem.heading}</span>}
                      >
                        {megaItem.megaItems.map((megaSubItem) => (
                          <Menu.Item key={megaSubItem.text}>
                            <Link
                              href={`/shop/${encodeURIComponent(megaSubItem.url)}`}
                            // as={`/shop/${encodeURIComponent(megaSubItem.url)}`}
                            >
                              <strong>{megaSubItem.text}</strong>
                            </Link>
                          </Menu.Item>
                        ))}
                      </SubMenu>
                    ))}
                  </SubMenu>
                );
              } else {
                return (
                  <Menu.Item key={item.text}>
                    {item.type === "dynamic" ? (
                      <Link
                        href={`${item.url}/[pid]`}
                        as={`${item.url}/${item.endPoint}`}
                      >
                        l<a>{item.text}</a>
                      </Link>
                    ) : (
                        <Link href={item.url} as={item.alias}>
                          <a>{item.text}</a>
                        </Link>
                      )}
                  </Menu.Item>
                );
              }
            })}
          </Menu>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    setting: state.setting,
    auth: state.auth
  }
};

export default connect(mapStateToProps)(PanelMenu);
