import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Menu } from 'antd';
import { menuPrimary } from '../../../public/static/data/menu';
import Link from 'next/link';
import { logOut } from "../../../store/auth/action";

const { SubMenu } = Menu;

class PanelMenu extends Component {
    constructor(props) {
        super(props);
    }

    rootSubmenuKeys = ['sub1', 'sub2', 'sub4'];

    state = {
        openKeys: [],
    };

    onOpenChange = openKeys => {
        const latestOpenKey = openKeys.find(
            key => this.state.openKeys.indexOf(key) === -1,
        );
        if (this.rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
            this.setState({ openKeys });
        } else {
            this.setState({
                openKeys: latestOpenKey ? [latestOpenKey] : [],
            });
        }
    };

    handleLogout = (e) => {
        this.props.cart.buynow.length = 0;
        e.preventDefault();
        this.props.dispatch(logOut());
    };


    render() {
        return (
            <div className="ps-panel__wrapper">
                <div className="ps-panel__header">
                    <h3>Menu</h3>
                </div>
                <div className="ps-panel__content">
                    {this.props.auth.isLoggedIn ? (
                        <Menu
                            mode="inline"
                            openKeys={this.state.openKeys}
                            onOpenChange={this.onOpenChange}
                        >
                            {menuPrimary.mobile_menu_login.map(item => {
                                if (item.subMenu) {
                                    return (
                                        <SubMenu
                                            key={item.text}
                                            title={
                                                <Link href={item.url}>
                                                    <a>{item.text}</a>
                                                </Link>
                                            }>
                                            {item.subMenu.map(subItem => (
                                                <Menu.Item key={subItem.text}>
                                                    <Link href={subItem.url}>
                                                        <a>{subItem.text}</a>
                                                    </Link>
                                                </Menu.Item>
                                            ))}
                                        </SubMenu>
                                    );
                                } else if (item.megaContent) {
                                    return (
                                        <SubMenu
                                            key={item.text}
                                            title={
                                                <Link href={item.url}>
                                                    <a>{item.text}</a>
                                                </Link>
                                            }>
                                            {item.megaContent.map(megaItem => (
                                                <SubMenu
                                                    key={megaItem.heading}
                                                    title={<span>{megaItem.heading}</span>}>
                                                    {megaItem.megaItems.map(megaSubItem => (
                                                        <Menu.Item key={megaSubItem.text}>
                                                            <Link href={item.url}>
                                                                <a>{megaSubItem.text}</a>
                                                            </Link>
                                                        </Menu.Item>
                                                    ))}
                                                </SubMenu>
                                            ))}
                                        </SubMenu>
                                    );
                                } else {
                                    return (
                                        <Menu.Item key={item.text}>
                                            {item.type === 'dynamic' ? (
                                                <Link
                                                    href={`${item.url}/[pid]`}
                                                    as={`${item.url}/${item.endPoint}`}>
                                                    l<a>{item.text}</a>
                                                </Link>
                                            ) : (
                                                    <div style={{color:"#1890ff"}}>
                                                        {item.text === "" ? (
                                                            <a onClick={this.handleLogout.bind(this)}>
                                                                LogOut
                                                            </a>
                                                        ) : ""}
                                                        <Link href={item.url} as={item.alias}>
                                                            <a>{item.text}</a>

                                                        </Link>
                                                    </div>
                                                )}
                                        </Menu.Item>
                                    );
                                }
                            })}
                        </Menu>) :
                        <Menu
                            mode="inline"
                            openKeys={this.state.openKeys}
                            onOpenChange={this.onOpenChange}
                        >
                            {menuPrimary.menu_1.map(item => {
                                if (item.subMenu) {
                                    return (
                                        <SubMenu
                                            key={item.text}
                                            title={
                                                <Link href={item.url}>
                                                    <a>{item.text}</a>
                                                </Link>
                                            }>
                                            {item.subMenu.map(subItem => (
                                                <Menu.Item key={subItem.text}>
                                                    <Link href={subItem.url}>
                                                        <a>{subItem.text}</a>
                                                    </Link>
                                                </Menu.Item>
                                            ))}
                                        </SubMenu>
                                    );
                                } else if (item.megaContent) {
                                    return (
                                        <SubMenu
                                            key={item.text}
                                            title={
                                                <Link href={item.url}>
                                                    <a>{item.text}</a>
                                                </Link>
                                            }>
                                            {item.megaContent.map(megaItem => (
                                                <SubMenu
                                                    key={megaItem.heading}
                                                    title={<span>{megaItem.heading}</span>}>
                                                    {megaItem.megaItems.map(megaSubItem => (
                                                        <Menu.Item key={megaSubItem.text}>
                                                            <Link href={item.url}>
                                                                <a>{megaSubItem.text}</a>
                                                            </Link>
                                                        </Menu.Item>
                                                    ))}
                                                </SubMenu>
                                            ))}
                                        </SubMenu>
                                    );
                                } else {
                                    return (
                                        <Menu.Item key={item.text}>
                                            {item.type === 'dynamic' ? (
                                                <Link
                                                    href={`${item.url}/[pid]`}
                                                    as={`${item.url}/${item.endPoint}`}>
                                                    l<a>{item.text}</a>
                                                </Link>
                                            ) : (
                                                    <Link href={item.url} as={item.alias}>
                                                        <a>{item.text}</a>
                                                    </Link>
                                                )}
                                        </Menu.Item>
                                    );
                                }
                            })}
                        </Menu>
                    }
                </div>

            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        cart: state.cart,
        auth: state.auth,
        setting: state.setting,
        Product: state.product,
    };
};

export default connect(mapStateToProps)(PanelMenu);
