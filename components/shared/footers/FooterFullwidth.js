import React from "react";
import FooterWidgets from "./modules/FooterWidgets";
import FooterCopyright from "./modules/FooterCopyright";
import FooterCopyrightBlack from "./modules/FooterCopyrightBlack";


const FooterFullwidth = () => (
  <footer className="ps-footer">
    <div> 
    {/* className="ps-container"> */}
      <FooterWidgets />
      <FooterCopyrightBlack />
      <FooterCopyright />

    </div>
  </footer>
);

export default FooterFullwidth;
