import React, { PureComponent } from 'react';
import { diffieHellman } from 'crypto';

class FooterCopyright extends PureComponent{
    constructor(props){
        super(props);
        this.state = {
            fields: {},
            errors: {}
        }

    }
    

    render(){
     return(
    <div className="ps-footer__copyright">
       
        <div className=" col-md-12 col-sm-12">
            <div className="row">

                <div className=" col-md-6 col-sm-6" style={{ color: "white" }}>

        © 2020 mind. All Rights Reserved
                    
                </div>
                <div className=" col-md-6 col-sm-6">
              
            </div>
            </div>


        </div>
    </div>
);
}
}

export default FooterCopyright;
