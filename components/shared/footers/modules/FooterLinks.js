import React, { Component } from "react";
import Link from "next/link";

class FooterLinks extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: [],
    };
  }

  componentDidMount() {
    fetch("http://13.126.93.37:8004/loan-app-data/FooterCatLinks")
      // .then((res) => res.json())
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else if (res.status === 401) {
          console.log("SOMETHING WENT WRONG")
          // this.setState({ requestFailed: true })
        }
      })
      .then((result) => {
        this.setState({
          isLoaded: true,
          items: result.data,
        });
      })
      .catch((error) =>
        this.setState({
          isLoaded: true,
          error: error,
        })
      );
  }

  render() {
    const { items } = this.state;

    return (
      <div className="ps-footer__links">
        {items.map((item) => (
          <p>
            <strong>{item.category}:</strong>
            {item.subs.map((sub) => (
              <Link href="/shop/[cat]" as={`/shop/${item.category}`}>
                <a>{sub.text}</a>
              </Link>
            ))}
          </p>
        ))}
      </div>
    );
  }
}

export default FooterLinks;
