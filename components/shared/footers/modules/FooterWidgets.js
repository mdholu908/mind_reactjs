import React, { PureComponent } from "react";
import Link from "next/link";
import { connect } from "react-redux";

class FooterWidgets extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: [],
    };
  }

  render() {
    const { items } = this.state;
    return (
      <div className="ps-footer__widgets">
      
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    cart: state.cart,
    auth: state.auth,
  };
};
export default connect(mapStateToProps)(FooterWidgets);
