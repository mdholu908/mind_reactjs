import React, { PureComponent } from "react";
import Link from "next/link";
import MobileHeaderActions from "./modules/MobileHeaderActions";
import SearchHeader from "./modules/SearchHeader";

class HeaderMobile extends PureComponent {
	constructor({ props }) {
		super(props);
	}

	render() {
		return (
			<header
				className="header header--mobile"
				
			>
				<div className="header__top">
					<div className="header__left"></div>
					<div className="header__right">
						<ul className="navigation__extra">
						
							<li>
								
							</li>
						</ul>
					</div>
				</div>
				<div className="navigation--mobile">
					<div className="navigation__left">
						<Link href="/">
							<a className="ps-logo">
								
								<img
									src="/static/img/logo/logo.jpeg"
									alt="mind"
									width={150}
									height={50}
								/>
							</a>
						</Link>
					</div>
					<div className="navigation__right">
						<MobileHeaderActions />
					</div>
				</div>
				<div className="ps-search--mobile">
					<SearchHeader />
					
				</div>
			</header>
		);
	}
}

export default HeaderMobile;
