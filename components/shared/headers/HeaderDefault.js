import React, { PureComponent } from "react";
import Link from "next/link";
import NavigationDefault from "../navigation/NavigationDefault";
import HeaderActions from "./modules/HeaderActions";
import SearchHeader from "./modules/SearchHeader";
import "lazysizes";

class HeaderDefault extends PureComponent {
	constructor({ props }) {
		super(props);
	}

	

	render() {
		return (
			<header
				className="header header--1 bg --cover"
				data-sticky="true"
				id="headerSticky"
				style={
					{
						backgroundColor:"#F7F7F7"
					}
				}
			>
				<div className="header__top">
					<div className="ps-container">
						<div className="header__left">
							<Link href="/">
								<a className="ps-logo">
									<img
										data-src="/static/img/logo/logo.jpeg"
										alt="Creditkart"
										class="lazyload"
										width={150}
										height={70}
									/>
								</a>
							</Link>
							
						</div>
						<div className="header__center">
							<SearchHeader />
						</div>
						<div className="header__right">
							<HeaderActions />
						</div>
					</div>
				</div>
				<NavigationDefault />
			</header>
		);
	}
}

export default HeaderDefault;
