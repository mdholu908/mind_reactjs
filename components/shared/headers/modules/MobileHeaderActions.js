import React, { PureComponent } from "react";
import { connect } from "react-redux";
import MiniCart from "./MiniCart";
import AccountQuickLinks from "./AccountQuickLinks";

class MobileHeaderActions extends PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    const { auth } = this.props;
    return (
      <div className="header__actions">
        <MiniCart />
        {auth.isLoggedIn && Boolean(auth.isLoggedIn) === true
          ? //                    <AccountQuickLinks isLoggedIn={true} />
            null
          : //                    <AccountQuickLinks isLoggedIn={false} />
            null}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return state;
};

export default connect(mapStateToProps)(MobileHeaderActions);
