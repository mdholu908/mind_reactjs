import React, { PureComponent } from "react";
import Link from "next/link";
import Router from 'next/router'
import ProductResultList from "../../../elements/products/ProductSearchList";
import { BaseURL } from "../../../../public/static/data/baseURL.json";
class SearchHeader extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      searchPanel: false,
      searchProducts: [],
      // searchProducts: products,
      keyword: "",
    };
  }
  searchByProductName = (keyword, object) => {
    let matches = [];
    let regexp = new RegExp(keyword.toLowerCase(), "g");
    object.forEach((product) => {
      if (product.title.toLowerCase().match(regexp)) matches.push(product);
    });
    return matches;
  };
  handleSearch(e) {
    if (e.target.value.length > 2) {
      if (e.target.value !== "") {
        this.setState({
          searchPanel: true,
          keyword: e.target.value,
          //                searchProducts: this.searchByProductName(
          //                    e.target.value,
          //                    products
          //                ),
        });
        const url = `${BaseURL}ecomapi/search_suggestion_web`;
        const req_data = {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            search_keyword: e.target.value,
          }),
        };
        fetch(url, req_data)
          // .then((res) => res.json())
          .then((res) => {
            if (res.ok) {
              return res.json();
            } else if (res.status === 401) {
              console.log("SOMETHING WENT WRONG")
              // this.setState({ requestFailed: true })
            }
          })
          .then((res) => {
            this.setState({
              items: res.suggestions,
              searchPanel: true,
              //  keyword: e.target.value,
              searchProducts: res.suggestions,
            });
          });
      } else {
        this.setState({ searchPanel: false, searchProducts: [] });
      }
    }
    else {
      this.setState({ searchPanel: false, searchProducts: [] });
    }
  }
  handleSubmit(e) {
    e.preventDefault();
    location.replace(`/search?keyword=${this.state.keyword}`)
  }


  render() {
    const { searchPanel, searchProducts } = this.state;

    return (
      <form
        className="ps-form--quick-search"
        method="get"
        action="/"
        onSubmit={this.handleSubmit.bind(this)}

      >
        {/* <Select defaultValue="Categories" style={{ width: 180 }}>
                    {productCategories.map(option => (
                        <Option value={option.text} key={option.text}>
                            {option.text}
                        </Option>
                    ))}
                </Select> */}
        {/* <div className="ps-form__categories">
                    <select className="form-control">
                        {exampleCategories.map(option => (
                            <option value={option} key={option}>
                                {option}
                            </option>
                        ))}
                    </select>
                </div> */}
        <input
          className="form-control"
          type="text"
          placeholder="I'm shopping for..."
          onChange={this.handleSearch.bind(this)}
          style={{ border: "0.5px solid #cabebe" }}
        />
        <button onClick={this.handleSubmit.bind(this)}>Search</button>
        <div
          className={`ps-panel--search-result${
            this.state.searchPanel && this.state.searchPanel === true ? " active " : ""
            }`}
        >
          <div className="ps-panel__content">
            {searchProducts.length > 0 ? (
              searchProducts.map((product) => (
                // <ProductResultList
                // 	product={product}
                //   key={product.id}
                //   searchPanel={searchPanel}
                // />

                <div className="ps-product ps-product--wide ps-product--search-result">
                  <div className="ps-product__thumbnail">
                    <Link href={`/product/${encodeURIComponent(product.id)}`}>
                      <a>
                        <img src={product.thumbnail} alt="" />
                      </a>
                    </Link>
                  </div>
                  <div className="ps-product__content">
                    <Link href={`/product/${encodeURIComponent(product.id)}`}>
                      <a className="ps-product__title">{product.title}</a>
                    </Link>
                  </div>
                </div>

              ))
            ) : (
                <span>Not found! Try with another keyword.</span>
              )}
          </div>
          <div className="ps-panel__footer text-center">
            {/* <Link 
            href={`/search?keyword=${encodeURIComponent(this.state.keyword)}`}
            > */}
              <a onClick={this.handleSubmit.bind(this)}>See all results</a>
            {/* </Link> */}
          </div>
        </div>
      </form>
    );
  }
}


export default SearchHeader;
