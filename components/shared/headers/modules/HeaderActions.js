import React, { PureComponent } from "react";
import { connect } from "react-redux";
import MiniCart from "./MiniCart";
import AccountQuickLinks from "./AccountQuickLinks";

class HeaderActions extends PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    const { compare, wishlist, auth } = this.props;
    return (
      <div className="header__actions">
  
        <MiniCart />
        {auth.isLoggedIn && Boolean(auth.isLoggedIn) === true ? (
          <AccountQuickLinks isLoggedIn={true} />
        ) : (
          <AccountQuickLinks isLoggedIn={false} />
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return state;
};

export default connect(mapStateToProps)(HeaderActions);
