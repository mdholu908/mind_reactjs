import React from "react";
import HeaderDefault from "../components/shared/headers/HeaderDefault";
import FooterFullwidth from "../components/shared/footers/FooterFullwidth";
// import HomeAdsColumns from "../components/partials/homepage/home-default/HomeAdsColumns"
import HeaderMobile from "../components/shared/headers/HeaderMobile";
import NavigationList from "../components/shared/navigation/NavigationList";


import Login from "../components/partials/account/Login";

const Index = () => (
  <div className="site-content">
    <HeaderDefault />
    <HeaderMobile />
    <NavigationList />
    <main id="homepage-1">
      
      <Login/>
    </main>
    <FooterFullwidth />
  </div>
);
export default Index;
