import React from "react";

import FooterFullwidth from "../../components/shared/footers/FooterFullwidth";
import HeaderDefault from "../../components/shared/headers/HeaderDefault";
import Newletters from "../../components/partials/commons/Newletters";
import BreadCrumb from "../../components/elements/BreadCrumb";
import SearchResult from "../../components/partials/shop/SearchResult";
import HeaderMobile from "../../components/shared/headers/HeaderMobile";
import NavigationList from "../../components/shared/navigation/NavigationList";
import SpecificSearch from "./keyword";

const SearchResultsPage = () => {
	const breadCrumb = [
		{
			text: "Home",
			url: "/",
		},
		{
			text: "Search Result",
		},
	];

	return (
		<div className="site-content">
			<HeaderDefault />
			<HeaderMobile />
			<NavigationList />
			<BreadCrumb breacrumb={breadCrumb} />
			<div className="ps-page--shop" id="shop-sidebar">
				<div className="container">
					{/* <SearchResult /> */}
					<SpecificSearch />
				</div>
			</div>
            {/*
			<div className="ps-shopping__footer">
				<div className="ps-pagination">
					<ul className="pagination">
						<li className="active">
							<a href="#">1</a>
						</li>
						<li>
							<a href="#">2</a>
						</li>
						<li>
							<a href="#">3</a>
						</li>
						<li>
							<a href="#">
								Next Page
								<i className="icon-chevron-right"></i>
							</a>
						</li>
					</ul>
				</div>
			</div>
			*/}
			<FooterFullwidth />
		</div>
	);
};

export default SearchResultsPage;
