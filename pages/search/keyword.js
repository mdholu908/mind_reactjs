import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import ShopWidget from "../../components/partials/homepage/home-default/shopwidget";
import Product from "../../components/elements/products/Product";
import { BaseURL } from "../../public/static/data/baseURL.json";
class SpecificSearch extends Component {
  constructor(props) {
    super(props);
    const { query } = this.props.router;

    this.state = {
      error: null,
      isLoaded: false,
      items: [],
    };
  }

  componentDidMount() {
    const { query } = this.props.router;
    if (query != null && query != "") {
      const req_data = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          input: { query }.query.keyword,
        }),
      };

      fetch(`${BaseURL}ecomapi/search_product`, req_data)
        // .then((res) => res.json())
        .then((res) => {
          if (res.ok) {
            return res.json();
          } else if (res.status === 401) {
            console.log("SOMETHING WENT WRONG")
            // this.setState({ requestFailed: true })
          }
        })
        .then((result) => {
          this.setState({
            items: result.products,
          });
        })
        .catch((error) =>
          this.setState({
            isLoaded: true,
            error: error,
          })
        );
    }
  }

  render() {
    const { items } = this.state;
    // const a  = productss.home_1_electronic;

    return (
      // const HomeDefaultTopCategories = () => (
      <div className="ps-layout--shop">
        <ShopWidget />
        <div className="ps-layout__right">
          <div className="ps-shopping">
            <div className="ps-shopping__header">
              <p>
                <strong className="mr-2">
                  <a>Search Result</a>
                </strong>
              </p>
            </div>
            <div className="ps-shopping__content">
              <div className="ps-shopping-product">
                <div className="row">
                  {items.map((item) => (
                    <div
                      className="col-xl-2 col-lg-4 col-md-4 col-sm-6 col-6 "
                      key={item.id}
                    >
                      <Product product={item} key={item.id} />
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return state.product;
};
export default withRouter(connect(mapStateToProps)(SpecificSearch));
