import React, { PureComponent } from 'react';
import { Col, Row, Card } from 'antd'
import FooterFullwidth from '../../components/shared/footers/FooterFullwidth';
import HeaderDefault from '../../components/shared/headers/HeaderDefault';
import Newsletter from '../../components/partials/commons/Newletters';
import BreadCrumb from '../../components/elements/BreadCrumb';
import OurTeam from '../../components/partials/page/about-us/OurTeam';
import AboutAwards from '../../components/partials/page/about-us/AboutAwards';
import AboutProduct from '../../components/partials/page/about-us/ProductDetail';
import HeaderMobile from '../../components/shared/headers/HeaderMobile';
import NavigationList from '../../components/shared/navigation/NavigationList';

class Media extends PureComponent {

    constructor(props) {
        super(props);
    }

    render() {
        return (

            <div>
                <HeaderDefault />
                <HeaderMobile />
                <NavigationList />

                <div className="col-sm-12 text-center" style={{
                    alignItems: "center"
                }}>

                    <h2>Media</h2>
                </div>


                <div className="container">
                    <div className="col-sm-12">

                        <div className="row">

                            <div className="col-sm-4">
                                <div className="card" style={{ width: "98%", lineHeight: "175px" }}>
                                    <div className="card-body">
                                        <div className="row" style={{
                                            alignItems: "center",
                                        }}>
                                            <div className="col-sm-6"><h4 className="text-center" >10 Nov, 2020</h4></div>
                                            <div className="col-sm-6" style={{ marginRight: "0px" }}>
                                                <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/5f/Yahoo%21News_Logo.svg/1200px-Yahoo%21News_Logo.svg.png"
                                                    // "https://www.taazatadka.com/wp-content/uploads/2020/09/cropped-taazatadka-logo-2.png" 
                                                    style={{ width: "" }}></img>
                                            </div>
                                        </div>
                                        <div className="row">
                                            {/* <div className="container"> */}
                                            <p style={{ textAlign: "justify", fontSize: "1.5rem", padding: "0px 10% 0% 10%", fontFamily: "Times New Roman" }}> <a href="https://in.news.yahoo.com/creditkart-fin-com-giving-100-132259657.html"
                                            // "https://www.taazatadka.com/diwali-creditkart-cashback-hidden-charge-swapnil-madiyar-financial-inclusion/"
                                            >
                                                "CreditKart Fin-Com is giving 100 pc cashback on shopping this Diwali"
                                                {/* "This Diwali, shop anything on CreditKart to get 100% instant cashback" */}
                                            </a></p>
                                            {/* </div> */}
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div className="col-sm-4">
                                <div className="card" style={{ width: "98%", lineHeight: "151px" }}>
                                    <div className="card-body">
                                        <div className="row" style={{
                                            alignItems: "center",
                                        }}>
                                            <div className="col-sm-6"><h4 className="text-center" >11 Nov, 2020</h4></div>
                                            <div className="col-sm-6" style={{ marginRight: "0px" }}>
                                                <img src="https://www.selectcitywalk.com/wp-content/uploads/2020/01/Financial-Express.jpg" style={{ width: "" }}></img>
                                            </div>
                                        </div>
                                        <div className="row">
                                            {/* <div className="container"> */}
                                            <p style={{ textAlign: "justify", fontSize: "1.5rem", padding: "0px 10% 0% 10%", fontFamily: "Times New Roman" }}> <a href="https://www.financialexpress.com/industry/sme/this-2-month-old-e-commerce-startup-is-burning-rs-10-cr-on-a-never-seen-before-offer-to-get-customers/2126658/">
                                            2 month old e-commerce startup is burning Rs 10 cr on a ‘never-seen-before’ offer to get customers
                                                                                            </a></p>
                                            {/* </div> */}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-sm-4">
                                <div className="card" style={{ width: "98%", lineHeight: "175px" }}>
                                    <div className="card-body">
                                        <div className="row" style={{
                                            alignItems: "center",
                                        }}>
                                            <div className="col-sm-6"><h4 className="text-center" >10 Nov, 2020</h4></div>
                                            <div className="col-sm-6" style={{ marginRight: "0px" }}>
                                                <img src="https://www.adgully.com/img/800/201901/annotation-2019-01-08-142535.jpg" style={{ width: "" }}></img>
                                            </div>
                                        </div>
                                        <div className="row">
                                            {/* <div className="container"> */}
                                            <p style={{ textAlign: "justify", fontSize: "1.5rem", padding: "0px 10% 0% 10%", fontFamily: "Times New Roman" }}> <a href="https://www.latestly.com/agency-news/business-news-creditkart-fin-com-is-giving-100-pc-cashback-on-shopping-this-diwali-2138211.html">
                                                "CreditKart Fin-Com is giving 100% cashback on shopping this Diwali"                                    </a></p>
                                            {/* </div> */}
                                        </div>
                                    </div>
                                </div>
                            </div>


                            {/* <div className="col-sm-4">
                                <div className="card" style={{ width: "98%", lineHeight: "150px" }}>
                                    <div className="card-body">
                                        <div className="row" style={{
                                            alignItems: "center",
                                        }}>
                                            <div className="col-sm-6"><h4 className="text-center" >10 Nov, 2020</h4></div>
                                            <div className="col-sm-6" style={{ marginRight: "0px" }}>
                                                <img src="https://res-5.cloudinary.com/crunchbase-production/image/upload/c_lpad,h_256,w_256,f_auto,q_auto:eco/v1419581175/tjeneie5blg9754xocgi.png" style={{ width: "" }}></img>
                                            </div>
                                        </div>
                                        <div className="row">
                                            {/* <div className="container"> */}
                            {/* <p style={{ textAlign: "justify", fontSize: "1.5rem", padding: "0px 10% 0% 10%", fontFamily: "Times New Roman" }}> <a href="http://www.itnewsonline.com/PRNewswire/CreditKart-Fin-Com-is-giving-100Percent-cashback-on-shopping-this-Diwali/719220">
                                "CreditKart Fin-Com is giving 100% cashback on shopping this Diwali"                                    </a></p> */}
                            {/* </div> */}
                            {/* </div>
                                    </div>
                                </div>
                            </div>  */}


                        </div>
                    </div>
                    <br></br>
                    <div className="col-sm-12">

                        <div className="row">

                            <div className="col-sm-4">
                                <div className="card" style={{ width: "98%", lineHeight: "175px" }}>
                                    <div className="card-body">
                                        <div className="row" style={{
                                            alignItems: "center",
                                        }}>
                                            <div className="col-sm-6"><h4 className="text-center" >10 Nov, 2020</h4></div>
                                            <div className="col-sm-6" style={{ marginRight: "0px" }}>
                                                <img src="https://media.licdn.com/dms/image/C511BAQFsueX7td6F4Q/company-background_10000/0?e=2159024400&v=beta&t=KNCrlvh1GpqfFv-xIBztAyp3Zpzdh9tPcYXjUZdMkEk" style={{ width: "" }}></img>
                                            </div>
                                        </div>
                                        <div className="row">
                                            {/* <div className="container"> */}
                                            <p style={{ textAlign: "justify", fontSize: "1.5rem", padding: "0px 10% 0% 10%", fontFamily: "Times New Roman" }}> <a href="https://www.indianweb2.com/2020/11/creditkart-fin-com-is-giving-100.html">
                                                "CreditKart Fin-Com is giving 100% cashback on shopping this Diwali"            </a></p>
                                            {/* </div> */}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="card" style={{ width: "98%", lineHeight: "175px" }}>
                                    <div className="card-body">
                                        <div className="row" style={{
                                            alignItems: "center",
                                        }}>
                                            <div className="col-sm-6"><h4 className="text-center" >10 Nov, 2020</h4></div>
                                            <div className="col-sm-6" style={{ marginRight: "0px" }}>
                                                <img src="https://thefactnews.in/wp-content/uploads/2020/04/cropped-fact-news-2.png" style={{ width: "" }}></img>
                                            </div>
                                        </div>
                                        <div className="row">
                                            {/* <div className="container"> */}
                                            <p style={{ textAlign: "justify", fontSize: "1.5rem", padding: "0px 10% 0% 10%", fontFamily: "Times New Roman" }}> <a href="https://thefactnews.in/creditkart-fin-com-is-giving-100-pc-cashback-on-shopping-this-diwali/">
                                                "CreditKart Fin-Com is giving 100 pc cashback on shopping this Diwali"                                            </a></p>
                                            {/* </div> */}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="card" style={{ width: "98%", lineHeight: "175px" }}>
                                    <div className="card-body">
                                        <div className="row" style={{
                                            alignItems: "center",
                                        }}>
                                            <div className="col-sm-6"><h4 className="text-center" >10 Nov, 2020</h4></div>
                                            <div className="col-sm-6" style={{ marginRight: "0px" }}>
                                                <img src="https://www.devdiscourse.com/AdminFiles/Logo/devdiscourse_logo.svg" style={{ width: "" }}></img>
                                            </div>
                                        </div>
                                        <div className="row">
                                            {/* <div className="container"> */}
                                            <p style={{ textAlign: "justify", fontSize: "1.5rem", padding: "0px 10% 0% 10%", fontFamily: "Times New Roman" }}> <a href="https://www.devdiscourse.com/article/business/1303474-creditkart-fin-com-is-giving-100-pc-cashback-on-shopping-this-diwali">
                                                "CreditKart Fin-Com is giving 100 pc cashback on shopping this Diwali"            </a></p>
                                            {/* </div> */}
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>

                    <br></br>

                    <div className="col-sm-12">

                        <div className="row">

                            <div className="col-sm-4">
                                <div className="card" style={{ width: "98%", lineHeight: "150px" }}>
                                    <div className="card-body">
                                        <div className="row" style={{
                                            alignItems: "center",
                                        }}>
                                            <div className="col-sm-6"><h4 className="text-center" >03 Nov, 2020</h4></div>
                                            <div className="col-sm-6" style={{ marginRight: "0px" }}>
                                                <img src="https://www.taazatadka.com/wp-content/uploads/2020/09/cropped-taazatadka-logo-2.png"
                                                    // "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5f/Yahoo%21News_Logo.svg/1200px-Yahoo%21News_Logo.svg.png" 
                                                    style={{ width: "" }}></img>
                                            </div>
                                        </div>
                                        <div className="row">
                                            {/* <div className="container"> */}
                                            <p style={{ textAlign: "justify", fontSize: "1.5rem", padding: "0px 10% 0% 10%", fontFamily: "Times New Roman" }}>
                                                <a href="https://www.taazatadka.com/diwali-creditkart-cashback-hidden-charge-swapnil-madiyar-financial-inclusion/">
                                                    "This Diwali, shop anything on CreditKart to get 100% instant cashback" </a></p>
                                            {/* "CreditKart Fin-Com is giving 100 pc cashback on shopping this Diwali"                                    </a></p> */}
                                            {/* </div> */}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {/* <div className="col-sm-4">
                                <div className="card" style={{ width: "98%", lineHeight: "150px" }}>
                                    <div className="card-body">
                                        <div className="row" style={{
                                            alignItems: "center",
                                        }}>
                                            <div className="col-sm-6"><h4 className="text-center" >10 Nov, 2020</h4></div>
                                            <div className="col-sm-6" style={{ marginRight: "0px" }}>
                                                <img src="https://www.adgully.com/img/800/201901/annotation-2019-01-08-142535.jpg" style={{ width: "" }}></img>
                                            </div>
                                        </div>
                                        <div className="row">
                                            {/* <div className="container"> */}
                            {/* <p style={{ textAlign: "justify", fontSize: "1.5rem", padding: "0px 10% 0% 10%", fontFamily: "Times New Roman" }}> <a href="https://www.latestly.com/agency-news/business-news-creditkart-fin-com-is-giving-100-pc-cashback-on-shopping-this-diwali-2138211.html">
                                                "CreditKart Fin-Com is giving 100% cashback on shopping this Diwali"                                    </a></p> */}
                            {/* </div> */}
                            {/* </div>
                                    </div>
                                </div>
                            </div> */}

                            <div className="col-sm-4">
                                <div className="card" style={{ width: "98%", lineHeight: "175px" }}>
                                    <div className="card-body">
                                        <div className="row" style={{
                                            alignItems: "center",
                                        }}>
                                            <div className="col-sm-6"><h4 className="text-center" >10 Nov, 2020</h4></div>
                                            <div className="col-sm-6" style={{ marginRight: "0px" }}>
                                                <img src="https://res-5.cloudinary.com/crunchbase-production/image/upload/c_lpad,h_256,w_256,f_auto,q_auto:eco/v1419581175/tjeneie5blg9754xocgi.png" style={{ width: "" }}></img>
                                            </div>
                                        </div>
                                        <div className="row">
                                            {/* <div className="container"> */}
                                            <p style={{ textAlign: "justify", fontSize: "1.5rem", padding: "0px 10% 0% 10%", fontFamily: "Times New Roman" }}> <a href="http://www.itnewsonline.com/PRNewswire/CreditKart-Fin-Com-is-giving-100Percent-cashback-on-shopping-this-Diwali/719220">
                                                "CreditKart Fin-Com is giving 100% cashback on shopping this Diwali"                                    </a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div className="col-sm-4">
                                <div className="card" style={{ width: "98%", lineHeight: "175px" }}>
                                    <div className="card-body">
                                        <div className="row" style={{
                                            alignItems: "center",
                                        }}>
                                            <div className="col-sm-6"><h4 className="text-center" >11 Nov, 2020</h4></div>
                                            <div className="col-sm-6" style={{ marginRight: "0px" }}>
                                                <img src="https://static.bignewsnetwork.com/bnn2/images/big-news-network.png" style={{ width: "" }}></img>
                                            </div>
                                        </div>
                                        <div className="row">
                                            {/* <div className="container"> */}
                                            <p style={{ textAlign: "justify", fontSize: "1.5rem", padding: "0px 10% 0% 10%", fontFamily: "Times New Roman" }}> <a href="https://www.bignewsnetwork.com/news/266946126/creditkart-fin-com-is-giving-100-pc-cashback-on-shopping-this-diwali">
                                                CreditKart Fin-Com is giving 100 pc cashback on shopping this Diwali                                                </a></p>
                                            {/* </div> */}
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>

                </div>

                <FooterFullwidth />

            </div>



        );
    }
}

export default Media;
