import React from 'react';

import FooterFullwidth from '../../components/shared/footers/FooterFullwidth';
import HeaderDefault from '../../components/shared/headers/HeaderDefault';
import Newsletter from '../../components/partials/commons/Newletters';
import BreadCrumb from '../../components/elements/BreadCrumb';
import OurTeam from '../../components/partials/page/about-us/OurTeam';
import AboutAwards from '../../components/partials/page/about-us/AboutAwards';
import AboutProduct from '../../components/partials/page/about-us/ProductDetail';
import HeaderMobile from '../../components/shared/headers/HeaderMobile';
import NavigationList from '../../components/shared/navigation/NavigationList';

const AboutUsPage = () => {
    const breadCrumb = [
        {
            text: 'Home',
            url: '/',
        },
        {
            text: 'About Us',
        },
    ];
    return (
        <div className="site-content">
            <HeaderDefault />
            <HeaderMobile />
            <NavigationList />
            <div className="ps-page--single">
                {/* <img src="/static/img/bg/about-us.jpg" alt="" /> */}
                <img src="/static/img/bg/aboutusdemo.gif" alt="" />
               
                <BreadCrumb breacrumb={breadCrumb} />
                <AboutProduct />
                <OurTeam />

                {/* <AboutAwards /> */}
                
            </div>
            <FooterFullwidth />
        </div>
    );
};
export default AboutUsPage;
