import React, { useEffect } from 'react';
import Router, { useRouter } from 'next/router';

import FooterFullwidth from '../../components/shared/footers/FooterFullwidth';
import HeaderDefault from '../../components/shared/headers/HeaderDefault';
import FaqTopic from '../../components/partials/page/FaqTopic';
import HeaderMobile from '../../components/shared/headers/HeaderMobile';
import NavigationList from '../../components/shared/navigation/NavigationList';
import BreadCrumb from '../../components/elements/BreadCrumb';

const ProductDefaultPage = () => {

    const router = useRouter();
    const { faq } = router.query;
    const breadCrumb = [
        {
            text: 'Home',
            url: '/',
        },
        {
            text: "FAQ's",
            url: '/page/faqs'
        },
        {
            text: 'Questions',
        },
    ];
    
    // useEffect(() => {
    //     if (isNaN(faq)) {
    //         Router.push('/page/page-404');
    //     }
    // }
    // );

    return (
        <div className="site-content">
           
            <HeaderDefault />
            <HeaderMobile />
            <NavigationList /> 
            <BreadCrumb breacrumb={breadCrumb} layout="fullwidth" />
            <div className="ps-page--product">
            <div className="ps-container">
                    <div className="ps-page__container"></div>
                    <div className="ps-page__left">
                        </div>
                        <div className="ps-page__right">
                        <FaqTopic
                        key = {faq}
                         faq={ faq }/>
                        </div>
                    </div>
            </div>
            <FooterFullwidth />
        </div>
    );
};

export default ProductDefaultPage;
