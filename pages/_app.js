import App from "next/app";
import React from "react";
import { Provider } from "react-redux";
import withRedux from "next-redux-wrapper";
import withReduxSaga from "next-redux-saga";
import { persistStore } from "redux-persist";
import { PersistGate } from "redux-persist/integration/react";
import createStore from "../store/store";
import DefaultLayout from "../components/layouts/DefaultLayout.js";
import ReactGA from "react-ga";
import TagManager from "react-gtm-module";
import dynamic from "next/dynamic";
// import ReactPixel from 'react-facebook-pixel';
// import ReactPixel from '@zsajjad/react-facebook-pixel';


import "../scss/style.scss";
// const FbPixel = dynamic(() => import("./fb_pixel"), { ssr: false });

class MyApp extends App {
	constructor(props) {
		super(props);
		this.persistor = persistStore(props.store);

	}

	componentDidMount() {
		setTimeout(function () {
			document.getElementById("__next").classList.add("loaded");
		}, 100);

		this.setState({ open: true });
		const trackingId = "UA-182379791-1";
		ReactGA.initialize(trackingId);
		ReactGA.pageview(window.location.pathname + window.location.search);
		// ReactGA.pageview("/");

		const gtmId = "GTM-KR92Q3T";
		const tagManagerArgs = {
			gtmId: gtmId,
		};

		TagManager.initialize(tagManagerArgs);


		// // if (typeof window === "undefined") {
		// 	const ReactPixel = import  ( "react-facebook-pixel") ;

		// 	// const ReactPixel =  require('react-facebook-pixel');
		// 	// global.window = {};
		// 	const options = {
		// 		autoConfig: true, // set pixel's autoConfig
		// 		debug: false, // enable logs
		// 	};
		// 	// ReactPixel.init("832090304216503", options);
		// 	ReactPixel.default.init('832090304216503');
		// 	ReactPixel.pageView(); // For tracking page view
		// // }
	}

	render() {
		const { Component, pageProps, store } = this.props;

		// Starting animation
		const getLayout =
			Component.getLayout ||
			((page) => <DefaultLayout children={page} />);
		return getLayout(
			<Provider store={store}>
				<PersistGate
					loading={<Component {...pageProps} />}
					persistor={this.persistor}
				>
					{/* <FbPixel /> */}

					<Component {...pageProps} />
				</PersistGate>
			</Provider>
		);
	}
}

export default withRedux(createStore)(withReduxSaga(MyApp));
