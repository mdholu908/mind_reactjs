import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import { Form, Input, notification, Alert, Card } from "antd";
import HeaderDefault from "../../components/shared/headers/HeaderDefault";
import HeaderMobile from "../../components/shared/headers/HeaderMobile";
import NavigationList from "../../components/shared/navigation/NavigationList";
import FooterFullwidth from "../../components/shared/footers/FooterFullwidth";

class paytmpay extends Component {
    constructor(props) {
        super(props);
        // const { amt } = this.props.router;
        this.state = {
            error: null,
            isLoaded: false,
            items: [],
        };
    }

    componentDidMount() {
        const script = document.createElement('script');
        script.src = 'https://checkout.razorpay.com/v1/checkout.js';
        script.async = true;
        document.body.appendChild(script);
    }

    handleSubmit = (e) => {

        var options = {
            "key": this.props.product.paydata.data.key_id, // Enter the Key ID generated from the Dashboard
            "amount": this.props.product.paydata.data.amount, // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
            "currency": this.props.product.paydata.data.currency,
            "name": "CreditKart",
            "description": this.props.product.paydata.data.dec,
            "image": "https://canvasjs.s3.ap-south-1.amazonaws.com/150X150jpeg.jpg",
            "order_id": this.props.product.paydata.data.order_id, //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
            "callback_url": this.props.product.paydata.data.callbackurl,
            "prefill": {
                "name": this.props.product.paydata.data.name,
                "email": this.props.product.paydata.data.email_id,
                "contact": this.props.product.paydata.data.mobile_no
            },
            "notes": {
                "address": "Razorpay Corporate Office"
            },
            "theme": {
                "color": "#3399cc"
            }
        };

        let rzp = new window.Razorpay(options);

        rzp.open();

        e.preventDefault();
       
    };

    render() {

        const cardStyle = {
            width: "50%",
            background: `linear-gradient(to right,  ${"#00FECA"} 0%,${"#2291C9"} 100%)`,
            color: "white",
            borderRadius: 20,
        };

        const white = {
            color: "white",
        };

        return (

            
            <div className="site-content">
                <HeaderDefault />
                <HeaderMobile />
                <NavigationList />
                <br /><br /><br /><br /><br /><br /><br /><br />
                <center>
                    <div className="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                     <h3>Click To Pay</h3>
                        <a onClick={this.handleSubmit}>

                            <Card style={cardStyle}>
                                <p style={white}>order id : {this.props.product.paydata.data.order_id_ck}</p>
                                <p style={white}>Amount : {this.props.product.paydata.data.amount}</p>


                            </Card>
                        </a>
                    </div>
                </center>
                <br /><br /><br /><br /><br /><br /><br /><br />
                <FooterFullwidth />
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        cart: state.cart,
        auth: state.auth,
        product: state.product,
    };
};
export default withRouter(connect(mapStateToProps)(paytmpay));
