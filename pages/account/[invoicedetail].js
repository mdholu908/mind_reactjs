import React from "react";
import FooterFullwidth from "../../components/shared/footers/FooterFullwidth";
import HeaderDefault from "../../components/shared/headers/HeaderDefault";
import BreadCrumb from "../../components/elements/BreadCrumb";
import HeaderMobile from "../../components/shared/headers/HeaderMobile";
import NavigationList from "../../components/shared/navigation/NavigationList";
import { useRouter } from "next/router";
import Loader from "../../components/elements/Loader";
import dynamic from "next/dynamic";

const InvoiceDetailPage = () => {
  const breadCrumb = [
    {
      text: "Home",
      url: "/",
    },
    {
      text: "Invoice Detail",
    },
  ];

  const router = useRouter();
  const { invoicedetail } = router.query;

  const InvoiceDetail = dynamic(
    () => import("../../components/partials/account/InvoiceDetail"),
    {
      loading: () => <Loader />,
    }
  );

  return (
    <div className="site-content">
      <HeaderDefault />
      <HeaderMobile />
      <NavigationList />
      <div className="ps-page--my-account">
        <BreadCrumb breacrumb={breadCrumb} />
        <InvoiceDetail record={invoicedetail} />
      </div>
      {/* <Newsletters layout="container" /> */}
      <FooterFullwidth />
    </div>
  );
};

export default InvoiceDetailPage;
