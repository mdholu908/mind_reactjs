import React, { PureComponent } from "react";
import FooterFullwidth from "../../components/shared/footers/FooterFullwidth";
import HeaderDefault from "../../components/shared/headers/HeaderDefault";
import { saveNewOrderId } from "../../store/auth/action";;
import BreadCrumb from "../../components/elements/BreadCrumb";
import HeaderMobile from "../../components/shared/headers/HeaderMobile";
import NavigationList from "../../components/shared/navigation/NavigationList";
import { connect } from "react-redux";
import { BaseURL } from "../../public/static/data/baseURL.json";
import dynamic from "next/dynamic";
import Loader from "../../components/elements/Loader";
import Link from "next/link";
import Router from "next/router";
import { Table, Tag, Space } from 'antd';



class Repayment extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      isfetched: false,
      creditpay: [],
      paytmdata: [],
      amount: 0.0,
      credit_amount: 10,
      token_data: null,
      totalPages: 0,
      currentPage: 1,
      creditPayCurrent: [],
      checked: false
    };

    const breadCrumb = [
      {
        text: "Home",
        url: "/",
      },

      {
        text: "Repayment",
      }
    ]

  }


  creditdata(data) {
    let Token = "Token" + " " + data.token;
    const req_data = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        authorization: Token,
      },
    };
    fetch(`${BaseURL}ecom/credit_repayment_info/`, req_data)
      // .then((res) => res.json())
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else if (res.status === 401) {
          console.log("SOMETHING WENT WRONG")
          // this.setState({ requestFailed: true })
        }
      })
      .then((result) => {
        this.setState({
          isLoaded: true,
          creditpay: result.data,
          isfetched: true,
          totalPages: Math.ceil(result.data.length / 3),
          currentPage: 1,
          creditPayCurrent: result.data.slice(0, 3),
        });
      })
      .catch((error) =>
        this.setState({
          isLoaded: true,
          error: error,
        })
      );
  }

  getPaymentDetails(orderid) {

    this.props.dispatch(saveNewOrderId(orderid))


    Router.push({ pathname: '/account/creditrepay' });


  }

  render() {
    const { creditpay, creditPayCurrent } = this.state;
   
    const data = [];
    for (let i = 0; i < this.state.creditpay.length; i++) {
      data.push({
        key: i,
        ID: <a onClick={() => this.getPaymentDetails(this.state.creditpay[i].order_id)}>{this.state.creditpay[i].login_id}</a>,
        Title: <a onClick={() => this.getPaymentDetails(this.state.creditpay[i].order_id)}>{this.state.creditpay[i].order_id}</a>,
        Date: <a onClick={() => this.getPaymentDetails(this.state.creditpay[i].order_id)}>{this.state.creditpay[i].order_date}</a>,
      });
    }

    const columns = [
      {
        title: 'ID',
        dataIndex: 'ID',
        key: 'ID',
      },

      {
        title: 'Title',
        dataIndex: 'Title',
        key: 'Title',
      },

      {
        title: 'Date',
        dataIndex: 'Date',
        key: 'Date',
      },
    ]


    const { items } = this.state;

    return (


      <div className="">
        <HeaderDefault />
        <HeaderMobile />
        <NavigationList />
        <section className="ps-my-account ps-page--account">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="ps-page__content">
                  <div className="ps-section--account-setting">
                    <div className="ps-section__header">
                      <h2>Repayment</h2>
                    </div>
                    <div  className="ps-section__content">
                      {this.props.auth.auth_data.token != null ? (
                        <div>
                          {this.state.isfetched == false ? this.creditdata({ token: this.props.auth.auth_data.token })
                            : null}
                          <div>

                            {creditpay.length !== 0 ? (
                              <div  className="ps-section__content">

                                <Table style={{ cursor: "pointer" }} columns={columns} dataSource={data} ></Table>

                              </div>
                            ) : (<div></div>)}
                          </div>
                        </div>
                      ) : (<div> </div>)}



                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <FooterFullwidth />

      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    produt: state.product,
    auth: state.auth,
  };
};
export default connect(mapStateToProps)(Repayment);
