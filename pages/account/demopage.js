import React, { useState } from 'react';
import { Table, Radio, Divider } from 'antd';
import demopagefortest from './demopagefortest';

class DemoPage extends React.Component {

    constructor(props) {
        super(props)

        this.state = {

            columns: [
                {
                    title: 'Name',
                    dataIndex: 'name',
                    render: (text) => <a>{text}</a>,
                },
                {
                    title: 'Age',
                    dataIndex: 'age',
                },
                {
                    title: 'Address',
                    dataIndex: 'address',
                },
            ],
            data: [
                {
                    key: '1',
                    name: 'John Brown',
                    age: 32,
                    address: 'New York No. 1 Lake Park',
                },
                {
                    key: '2',
                    name: 'Jim Green',
                    age: 42,
                    address: 'London No. 1 Lake Park',
                },
                {
                    key: '3',
                    name: 'Joe Black',
                    age: 32,
                    address: 'Sidney No. 1 Lake Park',
                },
                {
                    key: '4',
                    name: 'Disabled User',
                    age: 99,
                    address: 'Sidney No. 1 Lake Park',
                },
            ]
        }  // rowSelection object indicates the need for row selection


    }

    componentDidMount() {
        const script = document.createElement('script');
        script.src = 'https://checkout.razorpay.com/v1/checkout.js';
        script.async = true;
        document.body.appendChild(script);
    }

    razorPay() {
        // alert("in razorPay")

        var options = {
            "key": "YOUR_KEY_ID", // Enter the Key ID generated from the Dashboard
            "amount": "50000", // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
            "currency": "INR",
            "name": "Acme Corp",
            "description": "Test Transaction",
            "image": "https://example.com/your_logo",
            "order_id": "order_9A33XWu170gUtm", //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
            "callback_url": "https://eneqd3r9zrjok.x.pipedream.net/",
            "prefill": {
                "name": "Gaurav Kumar",
                "email": "gaurav.kumar@example.com",
                "contact": "9999999999"
            },
            "notes": {
                "address": "Razorpay Corporate Office"
            },
            "theme": {
                "color": "#3399cc"
            }
        };
        alert("opyions "+JSON.stringify(options) )
        var rzp1 = new Razorpay(options);
        rzp1.open();

    }


    render() {
        return (
            <div className="togglepanel">

                <div>
                    <div>
                        <p>Browse Plans for {this.state.operator}</p>
                        <div style={{ display: "flex", boxSizing: "border-box" }}>
                            <div className="popularstyleclass">
                                <div className="popular">Popular</div>
                            </div>

                            <div style={{ width: "80%" }}>
                                <div className="columnclass">
                                    <div className="innertitle">Circle</div>

                                    <div className="innertitle">Validity</div>
                                    <div className="innertitle1">Description</div>
                                    <div className="innertitle2">Price</div>
                                </div>
                                <div className="columnclass">

                                    <div className="innertitle">
                                        <a
                                            onClick={this.razorPay}>Value 1
                                        </a>
                                    </div>

                                    <div className="innertitle"><a
                                        onClick={this.razorPay}>Value 2
                                        </a></div>
                                    <div className="innertitle1"><a
                                        onClick={this.razorPay}>Value 3
                                        </a></div>
                                    <div className="innertitle2"><a
                                        onClick={this.razorPay}>Value 4
                                        </a></div>

                                </div>


                            </div>

                        </div>
                    </div>
                </div>
            </div>
        );
    };
}

export default DemoPage;