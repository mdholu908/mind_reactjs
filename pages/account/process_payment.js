import React from 'react';
import Newsletters from '../../components/partials/commons/Newletters';
import FooterFullwidth from '../../components/shared/footers/FooterFullwidth';
import HeaderDefault from '../../components/shared/headers/HeaderDefault';
import BreadCrumb from '../../components/elements/BreadCrumb';
import ProcessPayment from '../../components/partials/payment/ProcessPayment';
import HeaderMobile from '../../components/shared/headers/HeaderMobile';
import NavigationList from '../../components/shared/navigation/NavigationList';

const PaymentPage = () => {
    const breadCrumb = [
        {
            text: 'Home',
            url: '/',
        },
        {
            text: 'Shopping Cart',
            
        },
        {
            text: 'Checkout Information',
            
        },
        {
            text: 'Payment',
        },
    ];
    return (
        <div className="site-content">
            <HeaderDefault />
            <HeaderMobile />
            <NavigationList />
            <div className="ps-page--simple">
                <BreadCrumb breacrumb={breadCrumb} />
                <ProcessPayment />
            </div>
            <FooterFullwidth />
        </div>
    );
};

export default PaymentPage;
