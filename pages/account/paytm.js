import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import { Form, Input, notification, Alert } from "antd";

class paytmpay extends Component {
  constructor(props) {
    super(props);
    // const { amt } = this.props.router;
    this.state = {
      error: null,
      isLoaded: false,
      items: [],
    };
  }

  handleSubmit = (e) => {
    e.preventDefault();
    {
      document.paytm.submit();
    }
  };

  render() {
    const { items } = this.state;
    // const { paydata } = this.props;
    const { paydata } = this.props;
    // alert("Paytm render "+JSON.stringify(this.props.product.paydata));
    console.log("Check MID " + JSON.stringify(this.props.product.paydata))
    console.log(JSON.stringify(this.props.product.paydata.orderId))
    // alert("test alert")

    //alert("oid  "+JSON.stringify(this.props.product.paydata.order_id))

    // const a  = productss.home_1_electronic;

    return (
      // const HomeDefaultTopCategories = () => (

      <html>
        <head>
          <title>Merchant Check Out Page</title>
        </head>
        <body>
          <center>
            <h4>Please do not refresh this page...</h4>
          </center>
          <Form
            method="post"
            action="https://securegw.paytm.in/order/process"
            name="paytm"
            className="ps-form--account"
            onSubmit={this.handleSubmit}
          >
            <table>
              <tbody>
                <Input type="hidden" name="MID" value={this.props.product.paydata.mid} />
                <Input type="hidden" name="WEBSITE" value={this.props.product.paydata.website} />
                <Input type="hidden" name="ORDER_ID" value={this.props.product.paydata.orderId} />
                <Input type="hidden" name="CUST_ID" value={this.props.product.paydata.cust_id} />
                <Input
                  type="hidden"
                  name="INDUSTRY_TYPE_ID"
                  value={this.props.product.paydata.industryType}
                />
                <Input
                  type="hidden"
                  name="CHANNEL_ID"
                  value={this.props.product.paydata.channel}
                />
                {/* <Input  name="TXN_AMOUNT" value={this.props.product.paydata.amount} /> */}
                <Input type="hidden" name="TXN_AMOUNT" value={this.props.product.paydata.amount} />

                <Input
                  type="hidden"
                  name="CALLBACK_URL"
                  value={this.props.product.paydata.callback}
                />
                <Input
                  type="hidden"
                  name="CHECKSUMHASH"
                  value={this.props.product.paydata.checksum}
                />
              </tbody>
            </table>
            {setTimeout(() => {
              <script type="text/javascript">
                {document.paytm.submit()};
              </script>;
            }, 2000)}

            {/* <div className="form-group submit">
                                    <button
                                        type="submit"
                                        className="ps-btn ps-btn--fullwidth">
                                        Pay
                                    </button>
                                </div> */}
          </Form>
        </body>
      </html>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    cart: state.cart,
    auth: state.auth,
    product: state.product,
  };
};
export default withRouter(connect(mapStateToProps)(paytmpay));
// export default paytmpay
