import React from "react";

import Newsletters from "../../components/partials/commons/Newletters";
import FooterFullwidth from "../../components/shared/footers/FooterFullwidth";
import HeaderDefault from "../../components/shared/headers/HeaderDefault";
import BreadCrumb from "../../components/elements/BreadCrumb";
import Invoices from "../../components/partials/account/Invoices";
import HeaderMobile from "../../components/shared/headers/HeaderMobile";
import NavigationList from "../../components/shared/navigation/NavigationList";

const MyAccountPage = () => {
  const breadCrumb = [
    {
      text: "Home",
      url: "/",
    },
    {
      text: "Invoices",
    },
  ];
  return (
    <div className="site-content">
      <HeaderDefault />
      <HeaderMobile />
      <NavigationList />
      <div className="ps-page--my-account">
        <BreadCrumb breacrumb={breadCrumb} />
        <Invoices />
      </div>
      <FooterFullwidth />
    </div>
  );
};

export default MyAccountPage;
