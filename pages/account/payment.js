import React from 'react';
import Newsletters from '../../components/partials/commons/Newletters';
import FooterFullwidth from '../../components/shared/footers/FooterFullwidth';
import HeaderDefault from '../../components/shared/headers/HeaderDefault';
import BreadCrumb from '../../components/elements/BreadCrumb';
import Payment from '../../components/partials/account/Payment';
import Wallet from '../../components/partials/account/Wallet';
import HeaderMobile from '../../components/shared/headers/HeaderMobile';
import NavigationList from '../../components/shared/navigation/NavigationList';

const PaymentPage = () => {
    const breadCrumb = [
        {
            text: 'Home',
            url: '/',
        },
        {
            text: 'Shopping Cart',
            url:'/account/shopping-cart',
            
        },
        {
            text: 'Checkout Information',
            url:'/account/checkout'

            
        },
        {
            text: 'Pay Options',
            url:'/account/payoptions'
        },
        {
            text: 'Payment',
        },
       
    ];
    return (
        <div className="site-content">
            <HeaderDefault />
            <HeaderMobile />
            <NavigationList />
            <div className="ps-page--simple">
                <BreadCrumb breacrumb={breadCrumb} />
                <Payment />
            </div>
            <FooterFullwidth />
        </div>
    );
};

export default PaymentPage;
