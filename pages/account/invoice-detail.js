import React from "react";

import Newsletters from "../../components/partials/commons/Newletters";
import FooterFullwidth from "../../components/shared/footers/FooterFullwidth";
import HeaderDefault from "../../components/shared/headers/HeaderDefault";
import BreadCrumb from "../../components/elements/BreadCrumb";
import InvoiceDetail from "../../components/partials/account/InvoiceDetail";
import HeaderMobile from "../../components/shared/headers/HeaderMobile";
import NavigationList from "../../components/shared/navigation/NavigationList";
import Router, { useRouter } from "next/router";

const InvoiceDetailPage = () => {
  const breadCrumb = [
    {
      text: "Home",
      url: "/",
    },
    {
      text: "Invoice Detail",
    },
  ];

  const router = useRouter();
  const { invoicedetail } = router.query;

  return (
    <div className="site-content">
      <HeaderDefault />
      <HeaderMobile />
      <NavigationList />
      <div className="ps-page--my-account">
        <BreadCrumb breacrumb={breadCrumb} />
        <InvoiceDetail record={invoicedetail} />
      </div>
      {/*             <Newsletters layout="container" /> */}
      <FooterFullwidth />
    </div>
  );
};

export default InvoiceDetailPage;
