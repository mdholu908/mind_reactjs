import React from 'react';
import Newsletters from '../../components/partials/commons/Newletters';
import FooterFullwidth from '../../components/shared/footers/FooterFullwidth';
import HeaderDefault from '../../components/shared/headers/HeaderDefault';
import BreadCrumb from '../../components/elements/BreadCrumb';
import Otp from '../../components/partials/account/Otp';
import Wallet from '../../components/partials/account/Wallet';
import HeaderMobile from '../../components/shared/headers/HeaderMobile';
import NavigationList from '../../components/shared/navigation/NavigationList';

const OtpPage = () => {
    const breadCrumb = [
        {
            text: 'Home',
            url: '/',
        },
        {
            text: 'Otp',
        },
       
    ];
    return (
        <div className="site-content">
            <HeaderDefault />
            <HeaderMobile />
            <NavigationList />
            <div className="ps-page--simple">
                <BreadCrumb breacrumb={breadCrumb} />
                <Otp />
            </div>
            <FooterFullwidth />
        </div>
    );
};

export default OtpPage;