import React from "react";

import Newsletters from "../../components/partials/commons/Newletters";
import FooterFullwidth from "../../components/shared/footers/FooterFullwidth";
import HeaderDefault from "../../components/shared/headers/HeaderDefault";
import BreadCrumb from "../../components/elements/BreadCrumb";

import UserInfoCheck from "../../components/partials/account/userinfodata";
import HeaderMobile from "../../components/shared/headers/HeaderMobile";
import NavigationList from "../../components/shared/navigation/NavigationList";

const UserInformationPage = () => {
  const breadCrumb = [
    {
      text: "Home",
      url: "/",
    },
    {
      text: "User Information",
    },
  ];

  return (
    <div className="site-content">
      <HeaderDefault />
      <HeaderMobile />
      <NavigationList />
      <div className="ps-page--my-account">
        <BreadCrumb breacrumb={breadCrumb} />
        <UserInfoCheck />
      </div>
      <FooterFullwidth />
    </div>
  );
};

export default UserInformationPage;
