export const actionTypes = {
	LOGIN_REQUEST: "LOGIN_REQUEST",
	LOGIN_SUCCESS: "LOGIN_SUCCESS",
	LOGOUT: "LOGOUT",
	LOGOUT_SUCCESS: "LOGOUT_SUCCESS",
	CHECK_AUTHORIZATION: "CHECK_AUTHORIZATION",
	SAVE_USER: "SAVE_USER",
	SAVE_SHIPPING_ADDRESS: "SAVE_SHIPPING_ADDRESS",
	GET_SHIPPING_ADDRESS: "GET_SHIPPING_ADDRESS",
	SERVICE_AVAILABLE: "SERVICE_AVAILABLE",
	SAVE_NEW_ORDER: "SAVE_NEW_ORDER",
	SAVE_NEW_ORDERID: "SAVE_NEW_ORDERID",
	SAVE_NEW_RESULT: "SAVE_NEW_RESULT",
	SAVE_MOB_NUM: "SAVE_MOB_NUM",
	RETURN_ORDER_ID: "RETURN_ORDER_ID",
	EXCHANGE_DETAILS: "EXCHANGE_DETAILS",
	ORDER_PLACED_SUCCESSFULL: "ORDER_PLACED_SUCCESSFULL",
};

export function login() {
	return { type: actionTypes.LOGIN_REQUEST };
}

export function loginSuccess() {
	return { type: actionTypes.LOGIN_SUCCESS };
}

export function logOut() {
	return { type: actionTypes.LOGOUT };
}

export function logOutSuccess() {
	return { type: actionTypes.LOGOUT_SUCCESS };
}

export function saveUser(data) {
	return {
		type: actionTypes.SAVE_USER,
		data,
	};
}

export function saveOrder(data) {
	return {
		type: actionTypes.SAVE_NEW_ORDER,
		data,
	};
}

/* */

export function saveNewOrderId(data) {
	return {
		type: actionTypes.SAVE_NEW_ORDERID,
		data,
	};
}

/* */
export function saveResult(data) {
	return {
		type: actionTypes.SAVE_NEW_RESULT,
		data,
	};
}

/* */

export function saveMobNo(data) {
	return {
		type: actionTypes.SAVE_MOB_NUM,
		data,
	};
}

export function saveShippingAddress(data) {
	return {
		type: actionTypes.SAVE_SHIPPING_ADDRESS,
		data,
	};
}

export function getShippingAddress(data) {
	return {
		type: actionTypes.GET_SHIPPING_ADDRESS,
		data,
	};
}

export function serviceAvailable(data) {
	return {
		type: actionTypes.SERVICE_AVAILABLE,
		data,
	};
}

export function getReturnOid(data) {
	return {
		type: actionTypes.RETURN_ORDER_ID,
		data,
	};
}

export function getExchangeDetails(data) {
	return {
		type: actionTypes.EXCHANGE_DETAILS,
		data,
	};
}

export function orderPlacedSuccess(data) {
	return {
		type: actionTypes.ORDER_PLACED_SUCCESSFULL,
		data,
	};
}
