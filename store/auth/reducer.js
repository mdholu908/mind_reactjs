import { actionTypes } from "./action";

export const initState = {
	isLoggedIn: false,
	// auth_data: null,
	auth_data: {
		username: null,
		token: null,
	},
	shipping_address_id: null,
	shipping_address_selected: null,
	is_service_available: false,
	new_orderid: null,
	new_orderedid: null,
	new_mobno: null,
	new_orderresult: null,
	RETURN_orderid: {},
	exchange_details: {},
};

function reducer(state = initState, action) {
	switch (action.type) {
		case actionTypes.LOGIN_SUCCESS:
			return {
				...state,
				...{ isLoggedIn: true },
			};

		case actionTypes.LOGOUT_SUCCESS:
			return {
				...state,
				...{ isLoggedIn: false },
				...{
					auth_data: {
						username: null,
						token: null,
					},
				},
				...{ pay_options: null },
				...{ shipping_address_id: null },
				...{ shipping_address_selected: null },
				...{ new_orderid: null },
				...{ new_orderedid: null },
				...{ new_orderresult: null },
				...{ RETURN_orderid: {} },
				...{ exchange_details: {} },
				...{ new_mobno: null },
				...{ is_service_available: false },
			};

		case actionTypes.SAVE_USER:
			return {
				...state,
				...{ auth_data: action.data },
			};

		case actionTypes.SAVE_NEW_ORDER:
			return {
				...state,
				...{ new_orderid: action.data },
			};

		case actionTypes.SAVE_NEW_ORDERID:
			return {
				...state,
				...{ new_orderedid: action.data },
			};

		/* */

		case actionTypes.SAVE_NEW_RESULT:
			return {
				...state,
				...{ new_orderresult: action.data },
			};

		/* */
		case actionTypes.SAVE_MOB_NUM:
			return {
				...state,
				...{ new_mobno: action.data },
			};

		case actionTypes.RETURN_ORDER_ID:
			return {
				...state,
				...{ RETURN_orderid: action.data },
			};
		case actionTypes.EXCHANGE_DETAILS:
			return {
				...state,
				...{ exchange_details: action.data },
			};

		case actionTypes.SAVE_SHIPPING_ADDRESS:
			return {
				...state,
				...{ shipping_address_id: action.data },
			};

		case actionTypes.SERVICE_AVAILABLE:
			return {
				...state,
				...{ is_service_available: action.data },
			};

		case actionTypes.GET_SHIPPING_ADDRESS:
			return {
				...state,
				...{ shipping_address_selected: action.data },
			};

		case actionTypes.SAVE_PAY_OPTIONS:
			return {
				...state,
				...{ pay_options: action.data },
			};

		case actionTypes.ORDER_PLACED_SUCCESSFULL:
			return {
				...state,
				...{ pay_options: null },
				...{ shipping_address_id: null },
				...{ shipping_address_selected: null },
				...{ new_orderid: null },
				...{ new_orderedid: null },
				...{ new_orderresult: null },
				...{ RETURN_orderid: {} },
				...{ exchange_details: {} },
				...{ new_mobno: null },
				...{ is_service_available: false },
			};

		default:
			return state;
	}
}

export default reducer;
