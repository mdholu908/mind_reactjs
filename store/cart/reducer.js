import { actionTypes } from "./action";
import { generateShowHourMinuteSecond } from "antd/lib/time-picker";

export const initCart = {
	cartItems: [],
	amount: 0,
	cartTotal: 0,
	buynow: [],
	isBuyNow: false,
	// codamt: 0,
	paymode: 0
};

function buynowfunc(product) {
	let arr = [];
	arr.push(product);
	return arr;
}

// function storeStringValue(codamt){
// 	let value = codamt
// 	return value
// }


function storeStringValuePay(paymode){
	let values = paymode
	return values
}


function reducer(state = initCart, action) {
	switch (action.type) {
		case actionTypes.GET_CART_SUCCESS:
			return {
				...state,
			};

		case actionTypes.CLEAR_CART:
			return {
				...state,
				// ...{ cartItems: action.payload.cartItems },
				// ...{ amount: action.payload.amount },
				// ...{ cartTotal: action.payload.cartTotal },
			};

		case actionTypes.UPDATE_CART_SUCCESS:
			return {
				...state,
				...{ cartItems: action.payload.cartItems },
				...{ amount: action.payload.amount },
				...{ cartTotal: action.payload.cartTotal },
			};

		case actionTypes.CLEAR_CART_SUCCESS:
			return {
				...state,
				...{ cartItems: action.payload.cartItems },
				...{ amount: action.payload.amount },
				...{ cartTotal: action.payload.cartTotal },
			};

		case actionTypes.BUY_NOW:
			return {
				...state,
				buynow: buynowfunc(action.product),
			};
		//----------------------------------------------------
		// case actionTypes.COD_AMT:
		// 	return {
		// 		...state,
		// 		codamt: storeStringValuePay(action.codamt),
		// 	};


		case actionTypes.PAY_MODE:
			return {
				...state,
				paymode: storeStringValuePay(action.paymode),
			};

		//----------------------------------------------------


		case actionTypes.GET_CART_ERROR:
			return {
				...state,
				...{ error: action.error },
			};

		case actionTypes.UPDATE_CART_ERROR:
			return {
				...state,
				...{ error: action.error },
			};

		case actionTypes.ORDER_SUCCESS_CART:
			// clear all the data saved in the cart
			return {
				...state,
				...{ cartItems: [] },
				...{ amount: 0 },
				...{ cartTotal: 0 },
				...{ buynow: [] },
				...{ isBuyNow: false },
			};

		case actionTypes.ORDER_SUCCESS_BUY_NOW:
			// clear all the data saved in the buynow
			return {
				...state,
				...{ buynow: [] },
				...{ isBuyNow: false },
			};

		default:
			return state;
	}
}

export default reducer;
