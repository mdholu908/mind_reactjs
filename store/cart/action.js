export const actionTypes = {
	GET_CART: "GET_CART",
	GET_CART_SUCCESS: "GET_CART_SUCCESS",
	GET_CART_ERROR: "GET_CART_ERROR",

	GET_CART_TOTAL_QUANTITY: "GET_CART_TOTAL_QUANTITY",
	GET_CART_TOTAL_QUANTITY_SUCCESS: "GET_CART_TOTAL_QUANTITY_SUCCESS",

	ADD_ITEM: "ADD_ITEM",
	BUY_NOW: "BUY_NOW",

	//-----------------

	// COD_AMT:"COD_AMT",

	PAY_MODE:"PAY_MODE",

	//-----------------
	REMOVE_ITEM: "REMOVE_ITEM",

	CLEAR_CART: "CLEAR_CART",
	CLEAR_CART_SUCCESS: "CLEAR_CART_SUCCESS",
	CLEAR_CART_ERROR: "CLEAR_CART_ERROR",

	INCREASE_QTY: "INCREASE_QTY",
	INCREASE_QTY_SUCCESS: "INCREASE_QTY_SUCCESS",
	INCREASE_QTY_ERROR: "INCREASE_QTY_ERROR",

	DECREASE_QTY: "DECREASE_QTY",
	UPDATE_CART: "UPDATE_CART",

	UPDATE_CART_SUCCESS: "UPDATE_CART_SUCCESS",
	UPDATE_CART_ERROR: "UPDATE_CART_ERROR",

	ORDER_SUCCESS_CART: "ORDER_SUCCESS_CART",
	ORDER_SUCCESS_BUY_NOW: "ORDER_SUCCESS_BUY_NOW",
};

export function getCart() {
	return { type: actionTypes.GET_CART };
}

export function clearCart() {
	return { type: actionTypes.CLEAR_CART };
}

export function getCartSuccess() {
	return {
		type: actionTypes.GET_CART_SUCCESS,
	};
}

export function getCartError(error) {
	return {
		type: actionTypes.GET_CART_ERROR,
		error,
	};
}

export function buyNow(product) {
	return {
		type: actionTypes.BUY_NOW,
		product,
	};
}

//------------------------------------

// export function codAmount(codamt) {
// 	return {
// 		type: actionTypes.COD_AMT,
// 		codamt,
// 	};
// }


export function payMode(paymode) {
	return {
		type: actionTypes.PAY_MODE,
		paymode,
	};
}
//------------------------------------

export function addItem(product) {
	return { type: actionTypes.ADD_ITEM, product };
}

export function removeItem(product) {
	return { type: actionTypes.REMOVE_ITEM, product };
}

export function increaseItemQty(product) {
	return { type: actionTypes.INCREASE_QTY, product };
}

export function decreaseItemQty(product) {
	return { type: actionTypes.DECREASE_QTY, product };
}

export function updateCartSuccess(payload) {
	return {
		type: actionTypes.UPDATE_CART_SUCCESS,
		payload,
	};
}

export function updateCartError(payload) {
	return {
		type: actionTypes.UPDATE_CART_ERROR,
		payload,
	};
}

export function orderPlacedSuccessCart(data) {
	return {
		type: actionTypes.ORDER_SUCCESS_CART,
		data,
	};
}

export function orderPlacedSuccessByNow(data) {
	return {
		type: actionTypes.ORDER_SUCCESS_BUY_NOW,
		data,
	};
}
